#define PCL_NO_PRECOMPILE
#include <pcl/point_traits.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>

#include <covis/covis.h>
#include "covislib/pcl_edge_detection/organized_edge_detection.h"

using namespace covis::core;
using namespace covis::feature;
using namespace covis::visu;

// Point and feature type
typedef pcl::PointXYZRGBA PointT;

pcl::PointCloud<covis::core::CategorisedEdge>::Ptr shortEdge;

using namespace covis;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;

enum {EDGELABEL_NAN_BOUNDARY=1, EDGELABEL_OCCLUDING=2, EDGELABEL_OCCLUDED=4, EDGELABEL_HIGH_CURVATURE=8, EDGELABEL_RGB_CANNY=16};

float depth = 0.02f;
int max_search = 50; int canny_low = 30; int canny_hight = 70; // 50 100 20,60    50, 30, 70


boost::shared_ptr<pcl::visualization::PCLVisualizer> normalsVis (
        pcl::PointCloud<covis::core::CategorisedEdge>::ConstPtr cloud, pcl::PointCloud<covis::core::CategorisedEdge>::ConstPtr normals)
                {
    // --------------------------------------------------------
    // -----Open 3D viewer and add point cloud and normals-----
    // --------------------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBField<covis::core::CategorisedEdge> rgb(cloud);
    viewer->addPointCloud<covis::core::CategorisedEdge> (cloud, rgb, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    viewer->addPointCloudNormals<covis::core::CategorisedEdge, covis::core::CategorisedEdge> (cloud, normals, 1, 0.01, "normals");
    //viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    return (viewer);
                }


int main(int argc, const char** argv) {
    core::ProgramOptions po;
    po.addPositional("pcd", "point cloud");
    po.addFlag('d', "edge_direction", "show edge direction");
    // Parse
    if(!po.parse(argc, argv))
        return 1;

    bool edge_direction = po.getFlag("edge_direction");

    pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>());
    COVIS_ASSERT(pcl::io::loadPCDFile<PointT>(po.getValue("pcd"), *cloud) == 0);
    COVIS_ASSERT(!cloud->empty());
    {
        ScopedTimer t("cloud");
        feature::PCLEdgeExtraction<PointT> pclee =  feature::PCLEdgeExtraction<PointT>(depth, max_search, canny_low,
                canny_hight, EDGELABEL_OCCLUDING | EDGELABEL_HIGH_CURVATURE | EDGELABEL_RGB_CANNY, false);
        std::vector<pcl::PointIndices> label_indices;
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge;
        pcl::PointIndices::Ptr edge_indices(new pcl::PointIndices);
        pclee.extractPCLEdges(cloud, label_indices, edge, shortEdge, edge_indices);
       

        pcl::console::print_value("fcloud: %d \t short edges: %d\n", label_indices.size(), shortEdge->size());


               pcl::io::savePCDFile("edge_normals.pcd", *shortEdge);

        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr cloud_normals (new pcl::PointCloud<covis::core::CategorisedEdge>());
        cloud_normals->width = shortEdge->width;
        cloud_normals->height = shortEdge->height;
        cloud_normals->points.resize(cloud_normals->height * cloud_normals->width);
        for (size_t e1 = 0; e1 < shortEdge->size(); e1++) {
            (*cloud_normals)[e1].x = (*shortEdge)[e1].x;
            (*cloud_normals)[e1].y = (*shortEdge)[e1].y;
            (*cloud_normals)[e1].z = (*shortEdge)[e1].z;
            (*cloud_normals)[e1].r = (*shortEdge)[e1].r;
            (*cloud_normals)[e1].g = (*shortEdge)[e1].g;
            (*cloud_normals)[e1].b = (*shortEdge)[e1].b;
            (*cloud_normals)[e1].normal_x = 0;
            (*cloud_normals)[e1].normal_y = 0;
            (*cloud_normals)[e1].normal_z = 0;
        }



        for(size_t i = 0; i < shortEdge->size(); i++) {
            //get the current point
            const covis::core::CategorisedEdge& pi = (*shortEdge)[i];
            Eigen::Vector3f _point, _normal, g1;
            _point[0] = pi.dx;
            _point[1] = pi.dy;
            _point[2] = 0;

            _normal[0] =  pi.normal_x;
            _normal[1] =  pi.normal_y;
            _normal[2] =  pi.normal_z;

            if (!edge_direction) g1 = _normal.cross(_point.cross(_normal));
            else g1 = _normal.cross(_point);
            g1.normalize();

            (*cloud_normals)[i].normal_x = g1[0];
            (*cloud_normals)[i].normal_y = g1[1];
            (*cloud_normals)[i].normal_z = g1[2];

        }
        pcl::io::savePCDFile("edge_direction.pcd", *cloud_normals);

        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
        viewer = normalsVis(cloud_normals, cloud_normals);
        while (!viewer->wasStopped ())
        {
            viewer->spinOnce (100);
            boost::this_thread::sleep (boost::posix_time::microseconds (100000));
        }
    }
    return 0;
}

