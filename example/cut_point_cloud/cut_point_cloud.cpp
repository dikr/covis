//taken from PCL
#include <covis/covis.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/PCLPointCloud2.h>


#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/project_inliers.h>

using namespace covis;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;


#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

typedef pcl::PointXYZRGBA PointT;


int
main (int argc, const char** argv)
{

    // Setup program options
    core::ProgramOptions po;
    po.addPositional("pcdfile", "pcd file");
    po.addOption("zmin", 0, "minimum z distance");
    po.addOption("zmax", 100, "maximum z distance");
    po.addOption("xmin", 0, "minimum x distance");
    po.addOption("xmax", 40000, "maximum x distance");
    po.addOption("ymin", 0, "minimum y distance");
    po.addOption("ymax", 40000, "maximum y distance");

    po.addOption("x_min", 0, "x_min coordinates");
    po.addOption("y_min", 0, "y_min coordinate");
    po.addOption("x_max", 640, "x coordinates");
    po.addOption("y_max", 480, "y coordinate");
    po.addOption("n", "new.pcd", "name of pcd file");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    // Load PCD file
    std::string filename = po.getValue("pcdfile");
    TicToc tt;  tt.tic ();
    print_highlight ("Loading "); print_value ("%s ", filename.c_str());
    pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>);
    pcl::io::loadPCDFile(filename, *cloud);
    print_info ("[done, "); print_value ("%g", tt.toc ()); print_info (" ms : ");
    print_value ("%d", cloud->width * cloud->height);
    print_info (" points]\n");
    if (cloud->isOrganized()){ print_info("PointCloud ");  print_value("is "); print_info("organized\n");}
    else { print_info("PointCloud ");  print_value("is not "); print_info("organized\n");}
    print_info ("Available dimensions: "); print_value ("%s\n", pcl::getFieldsList(*cloud).c_str ());


    //cut point cloud
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr out (new pcl::PointCloud<pcl::PointXYZRGBA>);
    out->width = cloud->width;
    out->height = cloud->height;
    out->points.resize(out->width * out->height);

    int removedPoints = 0;

    unsigned int x_min =  po.getValue<unsigned int>("x_min");
    unsigned int x_max =  po.getValue<unsigned int>("x_max");
    unsigned int y_min =  po.getValue<unsigned int>("y_min");
    unsigned int y_max =  po.getValue<unsigned int>("y_max");

    pcl::PointXYZRGBA ref_point = (*cloud)(140, 230);

    //remove some noise
    for (unsigned int row = 0; row < cloud->width; row++)
        for (unsigned int col = 0; col < cloud->height; col++){
            if (row > x_min && row < x_max &&
                    col > y_min && col < y_max) {
                (*out)(row, col).r = (*cloud)(row, col).r;
                (*out)(row, col).g = (*cloud)(row, col).g;
                (*out)(row, col).b = (*cloud)(row, col).b;
                (*out)(row, col).x = (*cloud)(row, col).x;
                (*out)(row, col).y = (*cloud)(row, col).y;
                (*out)(row, col).z = (*cloud)(row, col).z;
            } else {
                (*out)(row, col).r = 0;
                (*out)(row, col).g = 0;
                (*out)(row, col).b = 0;
                (*out)(row, col).x = 0;
                (*out)(row, col).y = 0;
                (*out)(row, col).z = 0;
                removedPoints++;
            }

        }
    pcl::io::savePCDFile(po.getValue("n"), *out);
    print_info("Amount of removed points: "); print_value ("%d \n", removedPoints);


    //add plane!
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<PointT> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setDistanceThreshold (0.01);

    seg.setInputCloud (out);
    seg.segment (*inliers, *coefficients);

    if (inliers->indices.size () == 0)
    {
        PCL_ERROR ("Could not estimate a planar model for the given dataset.");
        return (-1);
    }

    std::cerr << "Model coefficients: " << coefficients->values[0] << " "
            << coefficients->values[1] << " "
            << coefficients->values[2] << " "
            << coefficients->values[3] << std::endl;

    float a = coefficients->values[0], b = coefficients->values[1], c = coefficients->values[2], d = coefficients->values[3];
    // Generate the data
    for (size_t ii = 0; ii < 5000; ++ii)
    {
        out->points[ii].x = ii;
        out->points[ii].y = ii;
        out->points[ii].z = (d - a * out->points[ii].x - b * out->points[ii].y) / c;
    }

    pcl::io::savePCDFile("cloud_cut.pcd", *out);

    return (0);
}
