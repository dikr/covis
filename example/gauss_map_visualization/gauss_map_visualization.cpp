// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/shot_lrf_omp.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/point_picking_event.h>

// Loaded point cloud with normals
typedef pcl::PointNormal PointNT;
pcl::PointCloud<PointNT>::Ptr cloud;

// Point picker callback, shows a histogram of chosen feature
void callback(const pcl::visualization::PointPickingEvent& ppe, void*);

// Globals
detect::PointSearch<PointNT> search;
float grad;
visu::Visu3D vis;

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("pcdfile", "point cloud file");
    po.addOption("radius-normal", 'n', 0.01, "normal estimation radius (set to <= 0 for disabled)");
    po.addOption("radius-gauss-map", 'g', 0.025, "local Gauss map radius");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;
    
    const float nrad = po.getValue<float>("radius-normal");
    grad = po.getValue<float>("radius-gauss-map");
    
    // Load point cloud
    cloud.reset(new pcl::PointCloud<PointNT>);
    COVIS_ASSERT(pcl::io::loadPCDFile<PointNT>(po.getValue("pcdfile"), *cloud) == 0);
    std::vector<int> dummy;
    pcl::removeNaNFromPointCloud(*cloud, *cloud, dummy);
    COVIS_ASSERT(!cloud->empty());
    
    if(nrad > 0.0) {
        // Compute normals
        core::ScopedTimer t("Normals");
        pcl::NormalEstimationOMP<PointNT, PointNT> ne;
        ne.setInputCloud(cloud);
        ne.setRadiusSearch(nrad);
        ne.compute(*cloud);
    }
    
    search.setTarget(cloud);
    
    // Setup a point cloud visualizer
    vis.addNormals<PointNT>(cloud, 5);
    vis.addPointCloud<PointNT>(cloud);
    vis.visu.registerPointPickingCallback(callback);
    vis.show();
    
    return 0;
}

void callback(const pcl::visualization::PointPickingEvent& ppe, void*) {
    const int idx = ppe.getPointIndex();
    if(idx >= 0 && size_t(idx) < cloud->size()) {
        COVIS_MSG("Picked point " << idx << ": " << cloud->points[idx]);
        pcl::RGB rgb;
        pcl::visualization::getRandomColors(rgb);
        
        // Show clicked point as a sphere
        vis.visu.addSphere(cloud->points[idx], grad,
                double(rgb.r) / 255.0, double(rgb.g) / 255.0, double(rgb.b) / 255.0,
                "sphere_" + core::stringify(idx));
        vis.visu.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5,
                "sphere_" + core::stringify(idx));
        
        // Extract points within local neighborhood
        const core::Correspondence c = search.radius(cloud->points[idx], grad);
        COVIS_MSG("Got " << c.size() << " points within local neighborhood");
        pcl::PointCloud<PointNT>::Ptr n(new pcl::PointCloud<PointNT>);
        *n = core::extract(*cloud, c.match);
        
        // Show Gauss map
        visu::Visu3D vgauss("Local Gauss map for point " + core::stringify(idx));
        vgauss.setShowOrigo(true);
        vgauss.addGaussMap<PointNT>(n, rgb);
        vgauss.show();
    }
}
