//taken from PCL
#include <covis/covis.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/PCLPointCloud2.h>

#include "covislib/pcl_edge_detection/organized_edge_detection.h"


using namespace covis;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;

typedef pcl::PointCloud<pcl::PointXYZRGBA> Cloud;
typedef Cloud::Ptr CloudPtr;
typedef Cloud::ConstPtr CloudConstPtr;


pcl::visualization::PCLVisualizer viewer ("3D Edge Viewer");
pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud;

void keyboard_callback (const pcl::visualization::KeyboardEvent& event, void*){

    double opacity;

    if (event.keyUp()){

        switch (event.getKeyCode()){
            case '1':
                viewer.getPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, opacity, "nan boundary edges");
                viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 1.0-opacity, "nan boundary edges");
                break;
            case '2':
                viewer.getPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, opacity, "occluding edges");
                viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 1.0-opacity, "occluding edges");
                break;
            case '3':
                viewer.getPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, opacity, "occluded edges");
                viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 1.0-opacity, "occluded edges");
                break;
            case '4':
                viewer.getPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, opacity, "high curvature edges");
                viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 1.0-opacity, "high curvature edges");
                break;
            case '5':
                viewer.getPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, opacity, "rgb edges");
                viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 1.0-opacity, "rgb edges");
                break;
            case '9':
                viewer.getPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, opacity, "normals");
                viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 1.0-opacity, "normals");
                break;
            case '6':
                viewer.removeAllShapes(0);
                break;
            case '7':
                viewer.removeAllPointClouds(0);
                break;
            case '8':
                pcl::PointXYZRGBA p = (*cloud)[182016];
                viewer.addSphere<pcl::PointXYZRGBA>(p, 0.03, 1,0,1,"a", 0);
                viewer.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, "a");
//                pcl::PointXYZRGBA p2 = (*cloud)[3920];
//                viewer.addSphere<pcl::PointXYZRGBA>(p2, 0.03, 0.5, 0.5, 0.5, "b", 0);
//                viewer.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, "b");
//                pcl::PointXYZRGBA p3 = (*cloud)[11243];
//                viewer.addSphere<pcl::PointXYZRGBA>(p3, 0.03, 0, 0.5, 0.5, "c", 0);
//                viewer.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, "c");
                break;
        }
    }
}

int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("pcdfile", "pcd file");
    po.addOption("l", 30 , "canny low threshold");
    po.addOption("h", 70 , "canny high threshold");

    po.addOption("s", "edges.pcd" , "save edges");
    po.addFlag('t', "template", "it is cut template");



    // Parse
    if(!po.parse(argc, argv))
        return 1;

    const bool it_is_template = po.getFlag("template");
    const float low = po.getValue<float>("l");
    const float high = po.getValue<float>("h");
    // Load PCD file
    std::string filename = po.getValue("pcdfile");
    TicToc tt;  tt.tic ();
    print_highlight ("Loading "); print_value ("%s ", filename.c_str());
    cloud.reset(new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::io::loadPCDFile(filename, *cloud);
    print_info ("[done, "); print_value ("%g", tt.toc ()); print_info (" ms : ");
    print_value ("%d", cloud->width * cloud->height);
    print_info (" points]\n");
    if (cloud->isOrganized()){ print_info("PointCloud ");  print_value("is "); print_info("organized\n");}
    else { print_info("PointCloud ");  print_value("is not "); print_info("organized\n");}
    print_info ("Available dimensions: "); print_value ("%s\n", pcl::getFieldsList(*cloud).c_str ());

    feature::PCLEdgeExtraction<pcl::PointXYZRGBA> pclee =  feature::PCLEdgeExtraction<pcl::PointXYZRGBA>(0.02f, 50, low, high, 1|2|4|8|16, it_is_template);
    std::vector<pcl::PointIndices> label_indices;
    pcl::PointIndices::Ptr edge_indices (new pcl::PointIndices);
    pcl::PointCloud<core::CategorisedEdge>::Ptr only_edge(new pcl::PointCloud<core::CategorisedEdge>);
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr gradient;
    int size = pclee.extractPCLEdges(cloud, label_indices, gradient, only_edge, edge_indices);
    std::cout << "total size: " << size <<               std::endl;
    // get separated edges into separated point clouds
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr occluding_edges (new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::copyPointCloud (*cloud, label_indices[1].indices, *occluding_edges);

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr occluded_edges (new pcl::PointCloud<pcl::PointXYZRGBA>),
            nan_boundary_edges (new pcl::PointCloud<pcl::PointXYZRGBA>),
            high_curvature_edges (new pcl::PointCloud<pcl::PointXYZRGBA>),
            rgb_edges (new pcl::PointCloud<pcl::PointXYZRGBA>);

    pcl::copyPointCloud (*cloud, label_indices[0].indices, *nan_boundary_edges);
    pcl::copyPointCloud (*cloud, label_indices[2].indices, *occluded_edges);
    pcl::copyPointCloud (*cloud, label_indices[3].indices, *high_curvature_edges);
    pcl::copyPointCloud (*cloud, label_indices[4].indices, *rgb_edges);


    ///////////////////////////////////////////
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr cloud_normals (new pcl::PointCloud<covis::core::CategorisedEdge>());
    cloud_normals->width = only_edge->width;
    cloud_normals->height = only_edge->height;
    cloud_normals->points.resize(cloud_normals->height * cloud_normals->width);
    for (size_t e1 = 0; e1 < only_edge->size(); e1++) {
        (*cloud_normals)[e1].x = (*only_edge)[e1].x;
        (*cloud_normals)[e1].y = (*only_edge)[e1].y;
        (*cloud_normals)[e1].z = (*only_edge)[e1].z;
        (*cloud_normals)[e1].r = (*only_edge)[e1].r;
        (*cloud_normals)[e1].g = (*only_edge)[e1].g;
        (*cloud_normals)[e1].b = (*only_edge)[e1].b;
        (*cloud_normals)[e1].normal_x = 0;
        (*cloud_normals)[e1].normal_y = 0;
        (*cloud_normals)[e1].normal_z = 0;
    }



    for(size_t i = 0; i < only_edge->size(); i++) {
        //get the current point
        const covis::core::CategorisedEdge& pi = (*only_edge)[i];
        Eigen::Vector3f _point, _normal, g1;
        _point[0] = pi.dx;
        _point[1] = pi.dy;
        _point[2] = 0;

        _normal[0] =  pi.normal_x;
        _normal[1] =  pi.normal_y;
        _normal[2] =  pi.normal_z;

        //g1 = _normal.cross(_point.cross(_normal));
        g1 = _normal.cross(_point);
        g1.normalize();

        (*cloud_normals)[i].normal_x = g1[0];
        (*cloud_normals)[i].normal_y = g1[1];
        (*cloud_normals)[i].normal_z = g1[2];

    }

//    viewer.addPointCloudNormals<covis::core::CategorisedEdge, covis::core::CategorisedEdge> (cloud_normals, cloud_normals, 1, 0.02, "normals");
//    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 0.0f, 0.0f, "normals");
//    /////////////////////////////////////////////////
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBA> rgb(cloud);
    viewer.addPointCloud <pcl::PointXYZRGBA>(cloud, rgb, "original point cloud", 0);

    //show edges
    const int point_size = 5;
    viewer.addPointCloud<pcl::PointXYZRGBA> (occluding_edges, "occluding edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, "occluding edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "occluding edges");

    viewer.addPointCloud<pcl::PointXYZRGBA> (nan_boundary_edges, "nan boundary edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, "nan boundary edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 0.0f, 1.0f, "nan boundary edges");


    viewer.addPointCloud<pcl::PointXYZRGBA> (occluded_edges, "occluded edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, "occluded edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0f, 0.0f, 0.0f, "occluded edges");

    viewer.addPointCloud<pcl::PointXYZRGBA> (high_curvature_edges, "high curvature edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, "high curvature edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0f, 1.0f, 0.0f, "high curvature edges");

    viewer.addPointCloud<pcl::PointXYZRGBA> (rgb_edges, "rgb edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, "rgb edges");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 1.0f, "rgb edges");

    print_info ("Occluding edges size: "); print_value("%d\n", occluding_edges->width * occluding_edges->height);
    print_info ("Nan boundary edges size: "); print_value("%d\n", nan_boundary_edges->width * nan_boundary_edges->height);
    print_info ("Occluded edges size: "); print_value("%d\n", occluded_edges->width * occluded_edges->height);
    print_info ("High curvature edges size: "); print_value("%d\n", high_curvature_edges->width * high_curvature_edges->height);
    print_info ("Canny(RGB) curvature edges size: "); print_value("%d\n", rgb_edges->width * rgb_edges->height);


    // Make gray point clouds
    //    for (size_t idx = 0; idx < cloud->points.size (); idx++)
    //    {
    //        uint8_t gray = uint8_t ((cloud->points[idx].r + cloud->points[idx].g + cloud->points[idx].b) / 3);
    //        cloud->points[idx].r = cloud->points[idx].g = cloud->points[idx].b = gray;
    //    }
    //    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBA> rgb(cloud);
    //        viewer.addPointCloud <pcl::PointXYZRGBA>(cloud, rgb, "original point cloud", 0);
    viewer.setSize (640, 480);
#if PCL_VERSION_COMPARE(>=,1,7,2)
    viewer.addCoordinateSystem (0.2f, "reference");
#else
    viewer.addCoordinateSystem (0.2f);
#endif
    viewer.registerKeyboardCallback(&keyboard_callback);

    viewer.setBackgroundColor (255, 255, 255);

    while (!viewer.wasStopped ())
    {
        viewer.spinOnce ();
        boost::this_thread::sleep (boost::posix_time::microseconds (100));
    }

    if (po.getValue("s") !="edges.pcd") pcl::io::savePCDFile(po.getValue("s"), *only_edge);
    return 0;
}
