#define PCL_NO_PRECOMPILE
#include <pcl/point_traits.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <pcl/visualization/point_cloud_color_handlers.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/icp.h>
#include <pcl/point_types.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <iterator>
#include <algorithm>

#include<map>

#include <pcl/filters/impl/radius_outlier_removal.hpp>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/search/search.h>
#include <pcl/search/impl/kdtree.hpp>
#include <pcl/search/kdtree.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/search/impl/organized.hpp>
#include <pcl/search/organized.h>
#include <pcl/filters/impl/filter.hpp>
#include <pcl/filters/filter.h>
#include <pcl/filters/impl/filter_indices.hpp>
#include <pcl/filters/filter_indices.h>
#include <pcl/impl/pcl_base.hpp>
#include <pcl/pcl_base.h>
#include <pcl/outofcore/outofcore.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/impl/shot_lrf_omp.hpp>
#include <pcl/features/impl/shot_omp.hpp>

#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <covis/covis.h>

using namespace covis;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;
using namespace std;

using namespace Eigen;

typedef Eigen::Matrix4f MyMatrix;

// Desired histogram dimension
#define HIST_DISTANCE_NORMAL 8*16
#define HIST_EDGE_UNCATEGORISED 40
#define HIST_EDGE_UNCATEGORISED2 80
#define HIST_1D_DISTANCE_ANGLE_SHOT 40 + 352

#define HIST_EDGE_CATEGORISED 15 * 10//15 * 10


// Point and feature type
typedef pcl::PointXYZRGBA PointT;

typedef pcl::Histogram<HIST_EDGE_CATEGORISED> FeatureEdgeCategorised;
typedef pcl::SHOT352 FeatureSHOT;
typedef pcl::SHOT1344 FeatureCSHOT;
typedef pcl::ReferenceFrame RFT;
typedef feature::ECSAD<covis::core::CategorisedEdge>::Histogram FeatureECSAD;

float depth = 0.02f;
int max_search = 50; int canny_low = 200; int canny_hight = 255; // 50 100 20,60    50, 30, 70

int corrSize = 5;
float radiusDistance = 0.05;
float radiusAngle = 0.03;

core::Correspondence::Vec best;
bool showCorr = false;
bool showGroundTruthAlignment = false;
int number_of_corr_to_show = 200;

ofstream file_object_size;
cv::Point2f shotMINMAX, dMINMAX, ecsadMINMAX;

enum {EDGELABEL_NAN_BOUNDARY=1, EDGELABEL_OCCLUDING=2, EDGELABEL_OCCLUDED=4, EDGELABEL_HIGH_CURVATURE=8, EDGELABEL_RGB_CANNY=16};


POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<HIST_EDGE_CATEGORISED>, (float[HIST_EDGE_CATEGORISED], histogram, histogram))


ofstream myfile, object1, object2, object3, object4, object5, temp;

const std::string directory_experiments = "/home/lilita/covis/covis-new/data/correspondence_test/feature_experiment/experiment/";
const std::string directory_scenes = "/home/lilita/covis/covis-new/data/correspondence_test/feature_experiment/scenes/";
const std::string directory_ground_truths = "/home/lilita/covis/covis-new/data/correspondence_test/feature_experiment/ground_truths/";

std::vector<std::string> objectsPath;

//XML Hacks
unsigned int indentionForXml = 0;

void printToFileCorrespondences(core::Correspondence::Vec cor, std::string name, pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge){
    ofstream f;
    f.open (name.c_str());
    for (size_t i = 0; i < cor.size(); i++){
        for (size_t j = 0; j < cor[i].match.size(); j++){
            covis::core::CategorisedEdge point = (*only_edge)[cor[i].match[j]];
            f << point.idx_original << "\n";

        }
    }
    f.close();
}

/**Parsing data to be used as Eigen Matrix**/
int fromVectoEigen (vector<string> & source, MyMatrix & target)
{   /**convert string to int and write it to the two dimensional array **/
    float array [16];

    //    for ( i= i-1 ; i >= 0 ; i-- )
    //    {
    string myString = source.back(); // leztzes Element von Vector als String
    stringstream ssin(myString);
    int j = 0;

    while (ssin.good() && j < 16)
    {
        ssin >> array[j];

        ++j;
    }

    target= Map<Matrix4f>(array);
    target.transposeInPlace();

    return 0 ;
}

int readFromFile (const char * path, vector <string> & mv)
{
    fstream file;
    string line;
    file.open(path);

    while (getline(file,line))
    {
        mv.push_back(line);
    }
    file.close();
    return 0;
}

std::vector<std::string> getPaths (const char* path, std::string name) {

    std::vector<std::string> objectsPath;
    DIR *dir;
    class dirent *ent;

    dir = opendir(path);
    std::string full_path;
    while ((ent = readdir(dir)) != NULL) {
        if(ent->d_type == DT_REG) {
            full_path = std::string(path);
            full_path.append(ent->d_name);
            objectsPath.push_back(full_path);
        }
    }
    pcl::console::print_value("amount of %s: %d\n", name.c_str(), objectsPath.size());

    return objectsPath;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
float computeAndPrintAmountOfCorrectCorr(const core::Correspondence::Vec corr, const std::vector<std::vector<int> > scene_correspondances, std::string object,
        bool save_separate_objects = false){
    int amount = 0;
    int size = 0;
    for (size_t i = 0; i < corr.size(); i++){

        int matching_point = corr[i].match[0];
        int source_point = corr[i].query;

        std::vector<int> ground_truth_points = scene_correspondances.at(source_point);

        for (size_t j = 0; j < ground_truth_points.size(); j++){

            if (ground_truth_points[j] == matching_point){
                amount++;
                break;
            }
        }
        if (ground_truth_points.size() > 0) size++;
    }
    float percent = (float)amount*100/(float)corr.size();
    float percentCorrect = (float)amount*100/(float)size;

    std::cout << "\t"  << percent << ", " << percentCorrect<< "\n";
    myfile << std::fixed << std::setprecision(3);
    myfile <<  "\t"  << percentCorrect;
    myfile.flush();

    return percentCorrect;
}
//*************************************************************************************************************************************************
pcl::PointCloud<FeatureECSAD>::Ptr getECSAD(pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge, pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge, float radius){
    feature::ECSAD<covis::core::CategorisedEdge> ecsad;
    ecsad.setRadius(radius);
    ecsad.setSurface(edge);
    pcl::PointCloud<FeatureECSAD>::Ptr ecsadHist = ecsad.compute(short_edge);
    return ecsadHist;
}

core::Correspondence::Vec compute_ECSAD(pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_Q_full, pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_Q,
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_T_full, pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_T,
        float radius, std::vector<std::vector<int> > scene_correspondances, std::string object, std::string scene, pcl::PointCloud<FeatureECSAD>::Ptr ftargetECSAD,
        float &resultECSAD){

    pcl::PointCloud<FeatureECSAD>::Ptr fquery = getECSAD(edge_Q_full, short_edge_Q, radius);

    core::Correspondence::Vec corr = *detect::computeKnnMatches<FeatureECSAD>(fquery, ftargetECSAD, 2, 2048);
    std::stringstream temp1;
    temp1 << "ECSAD_" << object << "_" << scene << ".txt";
    printToFileCorrespondences(corr, temp1.str().c_str(), short_edge_T);

    if (showCorr) {
        best = corr;
        core::sort(best);
        best.resize(number_of_corr_to_show);
        visu::showCorrespondences<covis::core::CategorisedEdge>(short_edge_Q, short_edge_T, best);
    }
    resultECSAD = computeAndPrintAmountOfCorrectCorr(corr, scene_correspondances, object, false);
    return corr;
}
//*************************************************************************************************************************************************
pcl::PointCloud<FeatureSHOT>::Ptr getSHOT(const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge, const pcl::PointIndices::Ptr edge_indices, float radius){

    pcl::SHOTEstimationOMP<covis::core::CategorisedEdge,covis::core::CategorisedEdge,FeatureSHOT,RFT> fest;
    fest.setRadiusSearch(radius);
    pcl::PointCloud<FeatureSHOT>::Ptr shotHist (new pcl::PointCloud<FeatureSHOT>);
    fest.setInputCloud(edge);
    fest.setIndices(edge_indices);
    fest.setInputNormals(edge);
    fest.compute(*shotHist);

    return shotHist;
}

core::Correspondence::Vec compute_SHOT(const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_Q_full, const pcl::PointIndices::Ptr edge_indices_Q,
        const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_T_full, const pcl::PointIndices::Ptr edge_indices_T, float radius,
        const std::vector<std::vector<int> > scene_correspondances, std::string object, std::string scene, pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_T,
        pcl::PointCloud<FeatureSHOT>::Ptr ftargetSHOT, float &resultSHOT){

    pcl::PointCloud<FeatureSHOT>::Ptr fquery = getSHOT(edge_Q_full, edge_indices_Q, radius);
    //make nan to 0
    for (size_t i = 0; i < fquery->size(); i++){
        for (size_t j = 0; j < 352; j++){
            if (!pcl_isfinite(fquery->points[i].descriptor[j])) {
                fquery->points[i].descriptor[j] = 0;
            }
        }
    }

    core::Correspondence::Vec corr;
    {
        covis::core::ScopedTimer t("core::Correspondence::Vec corr");
        pcl::console::print_warn("Amount of fquery points: %d\n", fquery->size());
        corr = *detect::computeKnnMatches<FeatureSHOT>(fquery, ftargetSHOT, 2, 2048);
        std::stringstream temp1;
        temp1 << "SHOT_" << object << "_" << scene << ".txt";
        printToFileCorrespondences(corr, temp1.str().c_str(), only_edge_T);
        pcl::console::print_warn("Amount of corr: %d\n", corr.size());

    }
    if (showCorr) {
        best = corr;
        core::sort(best);
        best.resize(number_of_corr_to_show);
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_Q(new pcl::PointCloud<covis::core::CategorisedEdge>);
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_T(new pcl::PointCloud<covis::core::CategorisedEdge>);
        pcl::copyPointCloud (*edge_Q_full, edge_indices_Q->indices, *short_edge_Q);
        pcl::copyPointCloud (*edge_T_full, edge_indices_T->indices, *short_edge_T);
        visu::showCorrespondences<covis::core::CategorisedEdge>(short_edge_Q, short_edge_T, best);
    }
    resultSHOT = computeAndPrintAmountOfCorrectCorr(corr, scene_correspondances, object, false);
    return corr;
}

//---------------------------------------------------------------------------------------------------------------------------------------------------
pcl::PointCloud<FeatureEdgeCategorised>::Ptr getEdgeDistanceAngleHistogram(const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr full_edge_cloud,
        const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_cloud, float radius){

    std::cout << "radius 1D: " << radius << std::endl;
    pcl::PointCloud<FeatureEdgeCategorised>::Ptr histograms (new pcl::PointCloud<FeatureEdgeCategorised>());
    feature::CategorizedEdgeDistanceAngleHistogram<covis::core::CategorisedEdge, HIST_EDGE_CATEGORISED> edah;
    edah.setEdgeCloud(*only_edge_cloud);
    edah.setRadius(radius);
    histograms = edah.compute(full_edge_cloud);

    return histograms;
}
//------------------------------------------------------------------------------------------------------------------
core::Correspondence::Vec compute_EdgeUncategorised(const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_Q,
        const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_Q, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_T,
        const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_T,
        std::vector<std::vector<int> > scene_correspondances, float radius, std::string object, std::string scene,
        pcl::PointCloud<FeatureEdgeCategorised>::Ptr ftarget1D, float result1D){

    pcl::PointCloud<FeatureEdgeCategorised>::Ptr fquery = getEdgeDistanceAngleHistogram(edge_Q, only_edge_Q, radius);
    core::Correspondence::Vec corr = *detect::computeKnnMatches<FeatureEdgeCategorised>(fquery, ftarget1D, 2, 2048); //2048
    std::stringstream temp1;
    temp1 << "1D_" << object << "_" << scene << ".txt";
    printToFileCorrespondences(corr, temp1.str().c_str(), only_edge_T);
    if (showCorr) {
        best = corr;
        core::sort(best);
        best.resize(number_of_corr_to_show);
        visu::showCorrespondences<covis::core::CategorisedEdge>(only_edge_Q, only_edge_T, best);
    }

    result1D = computeAndPrintAmountOfCorrectCorr(corr, scene_correspondances, object, false);
    return corr;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void getEdges(const pcl::PointCloud<PointT>::Ptr query, pcl::PointCloud<covis::core::CategorisedEdge>::Ptr &edge,
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr &short_edge, pcl::PointIndices::Ptr &edge_indices, bool itIsTemplate){


    feature::PCLEdgeExtraction<PointT> aa =  feature::PCLEdgeExtraction<PointT>(depth, max_search, canny_low,
            canny_hight, EDGELABEL_RGB_CANNY | EDGELABEL_OCCLUDING | EDGELABEL_OCCLUDED | EDGELABEL_HIGH_CURVATURE, itIsTemplate);
    //EDGELABEL_OCCLUDING | NOT EDGELABEL_OCCLUDED | EDGELABEL_HIGH_CURVATURE |
    pcl::console::print_value("Extracting for scene: EDGELABEL_RGB_CANNY | EDGELABEL_OCCLUDING | EDGELABEL_OCCLUDED | EDGELABEL_HIGH_CURVATURE \n");
    std::vector<pcl::PointIndices> label_indices;

    int edge_size = aa.extractPCLEdges(query, label_indices, edge, short_edge, edge_indices);
    pcl::console::print_warn("edge size : %d \n", edge_size);
}

//***************************************************************************************************************************************************
class BestCorrespondence{
    public:
        int key;
        int occurance;
        float distance;
        float distance_norm;
        float distance_ratio;
        std::vector<int> query;
        std::string feature;

        bool operator< (BestCorrespondence& o)
        {
            return occurance < o.occurance;
        }

};

bool sortByDistanceNorm(const BestCorrespondence &lhs, const BestCorrespondence &rhs)
{
    return lhs.distance_norm < rhs.distance_norm;
}

bool sortByDistanceRatio(const BestCorrespondence &lhs, const BestCorrespondence &rhs)
{
    return lhs.distance_ratio < rhs.distance_ratio;
}


cv::Point2f findMaxDistance(core::Correspondence::Vec cor){
    float dmax = -10000000;
    float dmin = 10000000;
    for(size_t i = 0; i < cor.size(); i++){
        if (cor[i].distance[0] > dmax) dmax = cor[i].distance[0];
        if (cor[i].distance[0] < dmin) dmin = cor[i].distance[0];
    }
    cv::Point2f point;
    point.x = dmin;
    point.y = dmax;
    return point;
}

void fillMatchList(std::map <int, BestCorrespondence>& match_list,  core::Correspondence::Vec corrSHOT,  core::Correspondence::Vec corr1D,
        core::Correspondence::Vec corrECSAD, int match_size, int i, cv::Point2f shot_min_max,
        cv::Point2f d_min_max, cv::Point2f ecsad_min_max){

    for(int g = 0; g < match_size; g++)
    {
        int neigh = g++;
        ++match_list[corrSHOT[i].match[g]].occurance;
        match_list[corrSHOT[i].match[g]].distance_ratio = corrSHOT[i].distance[g] / corrSHOT[i].distance[neigh];
        match_list[corrSHOT[i].match[g]].distance_norm = (corrSHOT[i].distance[g] - shot_min_max.x) / (shot_min_max.y - shot_min_max.x);
        match_list[corrSHOT[i].match[g]].key = corrSHOT[i].match[g];
        match_list[corrSHOT[i].match[g]].query.push_back(corrSHOT[i].query);
        match_list[corrSHOT[i].match[g]].feature = "shot";

        ++match_list[corr1D[i].match[g]].occurance;
        match_list[corr1D[i].match[g]].distance_ratio = corr1D[i].distance[g] / corr1D[i].distance[neigh];
        match_list[corr1D[i].match[g]].distance_norm = (corr1D[i].distance[g] - d_min_max.x) / (d_min_max.y - d_min_max.x);
        match_list[corr1D[i].match[g]].key = corr1D[i].match[g];
        match_list[corr1D[i].match[g]].query.push_back(corr1D[i].query);
        match_list[corr1D[i].match[g]].feature = "1d";

        ++match_list[corrECSAD[i].match[g]].occurance;
        match_list[corrECSAD[i].match[g]].distance_ratio = corrECSAD[i].distance[g] / corrECSAD[i].distance[neigh];
        match_list[corrECSAD[i].match[g]].distance_norm = (corrECSAD[i].distance[g] - ecsad_min_max.x) / (ecsad_min_max.y - ecsad_min_max.x);
        match_list[corrECSAD[i].match[g]].key = corrECSAD[i].match[g];
        match_list[corrECSAD[i].match[g]].query.push_back(corrECSAD[i].query);
        match_list[corrECSAD[i].match[g]].feature = "ecsad";
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void display(const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr target, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr final){
    pcl::visualization::PCLVisualizer viewer ("Alignment");
    viewer.addPointCloud<covis::core::CategorisedEdge> (target, "shortEdgeT");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 255, 255, 255, "shortEdgeT");

    viewer.addPointCloud<covis::core::CategorisedEdge> (final, "final");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 255, "final");

    while (!viewer.wasStopped ())
    {
        viewer.spinOnce ();
        boost::this_thread::sleep (boost::posix_time::microseconds (100));
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void computePoseAndersRansac(core::Correspondence::Vec corr, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_Q,
        const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_T, ofstream &file){

    core::Correspondence::VecPtr v(new core::Correspondence::Vec(corr));

    detect::Ransac<covis::core::CategorisedEdge> r;
    r.setSource(only_edge_Q);
    r.setTarget(only_edge_T);
    r.setCorrespondences(v);
    r.setIterations(50000);
    r.setInlierThreshold(0.01);
    r.setFullEvaluation(false);
    r.setInlierFraction(0);

    Eigen::Matrix4f m = r.estimate().pose;
    file << m(0) << " " << m(4) << " "<< m(8) << " " << m(12) << " "<< m(1) << " " << m(5) << " "<< m(9) << " " << m(13) << " "<< m(2) << " " << m(6) << " "<< m(10) << " " << m(14) << " "<< m(3) << " " << m(7) << " "<< m(11) << " " << m(15) << "\n";
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr final(new pcl::PointCloud<covis::core::CategorisedEdge>());
    pcl::transformPointCloud(*only_edge_Q, *final, m);
    //display(only_edge_T, final);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void computePose(core::Correspondence::Vec corr, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_Q, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_T, ofstream &file){
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr final (new pcl::PointCloud<covis::core::CategorisedEdge>());
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr aligned (new pcl::PointCloud<covis::core::CategorisedEdge>());

    pcl::registration::CorrespondenceRejectorSampleConsensus<covis::core::CategorisedEdge> crsc;
    pcl::CorrespondencesPtr pclc = core::convert(corr);
    pcl::CorrespondencesPtr corrOut(new pcl::Correspondences());
    crsc.setInputTarget(only_edge_T);
    crsc.setInputSource(only_edge_Q);
    crsc.setMaximumIterations(50000);
    crsc.setInlierThreshold(0.01);
    crsc.setSaveInliers(true);
    crsc.setRefineModel(true);
    crsc.getRemainingCorrespondences(*pclc, *corrOut);

    Eigen::Matrix4f transformation = crsc.getBestTransformation();
    std::vector<int> ransacIdx;
    crsc.getInliersIndices(ransacIdx);
    pcl::transformPointCloud(*only_edge_Q, *final, transformation);

    //ICP
    pcl::IterativeClosestPoint<covis::core::CategorisedEdge, covis::core::CategorisedEdge> icp;
    icp.setInputSource(final);
    icp.setInputTarget(only_edge_T);
    icp.setMaximumIterations(5000);
    icp.align(*aligned);

    Eigen::Matrix4f m = icp.getFinalTransformation()  * crsc.getBestTransformation();
    file << m(0) << " " << m(4) << " "<< m(8) << " " << m(12) << " "<< m(1) << " " << m(5) << " "<< m(9) << " " << m(13) << " "<< m(2) << " " << m(6) << " "<< m(10) << " " << m(14) << " "<< m(3) << " " << m(7) << " "<< m(11) << " " << m(15) << "\n";
    //  std::cout << m(0) << " " << m(4) << " "<< m(8) << " " << m(12) << " "<< m(1) << " " << m(5) << " "<< m(9) << " " << m(13) << " "<< m(2) << " " << m(6) << " "<< m(10) << " " << m(14) << " "<< m(3) << " " << m(7) << " "<< m(11) << " " << m(15) << "\n";
    //
    //        pcl::visualization::PCLVisualizer viewer ("Alignment");
    //        viewer.addPointCloud<covis::core::CategorisedEdge> (only_edge_T, "shortEdgeT");
    //        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 255, 255, 255, "shortEdgeT");
    //
    //        viewer.addPointCloud<covis::core::CategorisedEdge> (aligned, "aligned");
    //        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 255, 0, 0, "aligned");
    //
    //        viewer.addPointCloud<covis::core::CategorisedEdge> (final, "final");
    //        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 255, "final");
    //
    //        while (!viewer.wasStopped ())
    //        {
    //            viewer.spinOnce ();
    //            boost::this_thread::sleep (boost::posix_time::microseconds (100));
    //        }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void printToFileBothCorrespondences(ofstream &output, core::Correspondence corr,  const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_Q,
        const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_T, std::string type){
    output << (*only_edge_Q)[corr.query].idx_original << "\t" << (*only_edge_T)[corr.match[0]].idx_original << "\t";
    output << type << "\n";
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void compute1DDistanceAngleAndSHOTBestCorrespondances(const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_Q_full, const pcl::PointIndices::Ptr edge_indices_Q,
        const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_Q, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_T_full,
        const pcl::PointIndices::Ptr edge_indices_T, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr only_edge_T, float radiusSHOT,
        float radius1DDistanceAngle, const std::vector<std::vector<int> > scene_correspondances, std::string object, std::string scene,
        ofstream &file_SHOT, ofstream &file_1D, ofstream &file_ECSAD, ofstream &file_SELECTION_NORM, ofstream &file_SELECTION_RATIO,
        pcl::PointCloud<FeatureSHOT>::Ptr ftargetSHOT,
        pcl::PointCloud<FeatureEdgeCategorised>::Ptr ftarget1D, pcl::PointCloud<FeatureECSAD>::Ptr ftargetECSAD){

    float resultSHOT = 0.0;
    core::Correspondence::Vec corrSHOT = compute_SHOT(edge_Q_full, edge_indices_Q, edge_T_full, edge_indices_T, radiusSHOT, scene_correspondances, object, scene, only_edge_T, ftargetSHOT, resultSHOT);
    computePoseAndersRansac(corrSHOT, only_edge_Q, only_edge_T, file_SHOT);
    float result1D = 0.0;
    core::Correspondence::Vec corr1D = compute_EdgeUncategorised(edge_Q_full, only_edge_Q, edge_T_full, only_edge_T, scene_correspondances, radius1DDistanceAngle, object, scene, ftarget1D, result1D);
    computePoseAndersRansac(corr1D, only_edge_Q, only_edge_T, file_1D);
    float resultECSAD = 0.0;
    core::Correspondence::Vec corrECSAD = compute_ECSAD(edge_Q_full, only_edge_Q, edge_T_full, only_edge_T, radiusDistance, scene_correspondances, object, scene, ftargetECSAD, resultECSAD);
    computePoseAndersRansac(corrECSAD, only_edge_Q, only_edge_T, file_ECSAD);


//    cv::Point2f shot_min_max;// = findMaxDistance(corrSHOT);
//    cv::Point2f d_min_max;// = findMaxDistance(corr1D);
//    cv::Point2f ecsad_min_max;// = findMaxDistance(corrECSAD);
//
//    shot_min_max.x = 144927.5362;
//    shot_min_max.y = -144926.087;
//    d_min_max.x = 0.0002017956522;
//    d_min_max.y = 0.01897884058;
//    ecsad_min_max.x = 0.000001196511594;
//    ecsad_min_max.y = 0.0001878043478;
//
//
//    shotMINMAX.x += shot_min_max.x;
//    shotMINMAX.y += shot_min_max.y;
//    dMINMAX.x += d_min_max.x;
//    dMINMAX.y += d_min_max.y;
//    ecsadMINMAX.x += ecsad_min_max.x;
//    ecsadMINMAX.y += ecsad_min_max.y;
//
//    core::Correspondence::Vec corrBoth_norm;
//    core::Correspondence::Vec corrBoth_ratio;

    //        //-----------------------------------------
            core::Correspondence::Vec corr_concatinated;
            for (size_t j = 0; j < corrSHOT.size(); j++){
                corr_concatinated.push_back(corrSHOT[j]);
            }
            for (size_t j = 0; j < corr1D.size(); j++){
                corr_concatinated.push_back(corr1D[j]);
            }
            for (size_t j = 0; j < corrECSAD.size(); j++){
                corr_concatinated.push_back(corrECSAD[j]);
            }
            computePoseAndersRansac(corr_concatinated, only_edge_Q, only_edge_T, file_SELECTION_RATIO);
            computeAndPrintAmountOfCorrectCorr(corr_concatinated, scene_correspondances, object, false);
    //    -------------------------
    //
    //
//    corrBoth_norm.resize(corr1D.size());
//    corrBoth_ratio.resize(corr1D.size());
//
//
//    ofstream ratioFile, normFile;
//    std::stringstream tempRatio, tempNorm;
//    tempRatio << scene << "_" << object << "_ratioOutput.txt";
//    tempNorm << scene << "_" << object << "_normOutput.txt";
//    normFile.open(tempNorm.str().c_str());
//    ratioFile.open(tempRatio.str().c_str());

//    for(size_t i = 0; i < corrBoth_norm.size(); i++){
//        core::Correspondence c_norm, c_ratio;
//        c_norm.query = corr1D[i].query;
//        c_ratio.query = corr1D[i].query;
//        //
//        //    //            std::cout << "query: " << (*only_edge_Q)[corr1D[i].query].idx_original << "\n"<< std::endl;
//        //    //            std::cout << "corrSHOT:: " << corrSHOT[i].match[0] << "  " << (*only_edge_T)[corrSHOT[i].match[0]].idx_original << " " << corrSHOT[i].distance[0] << std::endl;
//        //    //            std::cout << "corr1D:: " << corr1D[i].match[0] << " " << (*only_edge_T)[corr1D[i].match[0]].idx_original << ": " << corr1D[i].distance[0] << std::endl;
//        //    //            std::cout << "corrECSAD:: " << corrECSAD[i].match[0] << "  "<< (*only_edge_T)[corrECSAD[i].match[0]].idx_original << ": " << corrECSAD[i].distance[0] << std::endl;
//        //    //
//        //    //            std::cout << "shot min: " << shot_min_max.x << ", max: " << shot_min_max.y << std::endl;
//        //    //            std::cout << "1d min: " << d_min_max.x << ", max: " << d_min_max.y << std::endl;
//        //    //            std::cout << "ecsad min: " << ecsad_min_max.x << ", max: " << ecsad_min_max.y << std::endl;
//        //    //
//        std::map <int, BestCorrespondence> match_list;
//        std::vector<BestCorrespondence> sorted_norm, sorted_ratio;
//
//        fillMatchList(match_list, corrSHOT, corr1D, corrECSAD, 1, i, shot_min_max, d_min_max, ecsad_min_max);
//
//        int the_most = -1;
//
//        for (std::map<int,BestCorrespondence>::iterator it=match_list.begin(); it!=match_list.end(); ++it) {
//            if (it->second.occurance >= the_most) {
//                the_most = it->second.occurance;
//                sorted_norm.push_back(it->second);
//                sorted_ratio.push_back(it->second);
//            }
//        }
//
//        std::sort(sorted_norm.begin(), sorted_norm.end(), sortByDistanceNorm);
//        std::sort(sorted_ratio.begin(), sorted_ratio.end(), sortByDistanceRatio);


        //            std::cout << "sorted size: " << sorted_norm.size() << std::endl;
        //
        //            for(unsigned int j = 0; j < sorted_norm.size(); j++)
        //            {
        //                std::cout << "j " << sorted_norm[j].key ;
        //                std::cout << " " << sorted_norm[j].distance_norm << " " << sorted_norm[j].occurance << "\n" ;
        //            }
        //
        //            std::cout << "wins: " <<sorted_norm[0].key << ", " << sorted_norm[0].distance_norm << std::endl;
        //            std::cout << "*******************************" << std::endl;
        //            for(unsigned int j = 0; j < sorted_ratio.size(); j++)
        //            {
        //                std::cout << "j " << sorted_ratio[j].key ;
        //                std::cout << " " << sorted_ratio[j].distance_ratio << " " << sorted_ratio[j].occurance << "\n" ;
        //            }
        //
        //            std::cout << "wins: " << (*only_edge_T)[sorted_ratio[0].key].idx_original << ", " << sorted_ratio[0].distance_ratio << std::endl;

//
//        c_norm.match.push_back(sorted_norm[0].key);
//        c_norm.distance.push_back(sorted_norm[0].distance_norm);
//
//        c_ratio.match.push_back(sorted_ratio[0].key);
//        c_ratio.distance.push_back(sorted_ratio[0].distance_norm);
//
//        printToFileBothCorrespondences(normFile, c_norm, only_edge_Q, only_edge_T, match_list[sorted_norm[0].key].feature);
//        printToFileBothCorrespondences(ratioFile, c_ratio, only_edge_Q, only_edge_T, match_list[sorted_ratio[0].key].feature);
//
//        corrBoth_norm[i] = c_norm;
//        corrBoth_ratio[i] = c_ratio;
        //        break;
//    }


//    if (showCorr) {
//        best = corrBoth_norm;
//        core::sort(best);
//        best.resize(number_of_corr_to_show);
//        visu::showCorrespondences<covis::core::CategorisedEdge>(only_edge_Q, only_edge_T, best);
//    }
//    float resultNorm = 0.0, resultRATIO = 0.0;
//    resultNorm = computeAndPrintAmountOfCorrectCorr(corrBoth_norm, scene_correspondances, object, false);
//    resultRATIO = computeAndPrintAmountOfCorrectCorr(corrBoth_ratio, scene_correspondances, object, false);
//    computePose(corrBoth_ratio, only_edge_Q, only_edge_T, file_SELECTION_RATIO);
//    computePose(corrBoth_norm, only_edge_Q, only_edge_T, file_SELECTION_NORM);
//    ratioFile.close();
//    normFile.close();

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void saveCannyParametersInFiles(std::string object){
    myfile << canny_low << "\t" << canny_hight;
    if (object == "bowl_2_1_1") object1 << canny_low << "\t" << canny_hight;
    else if (object == "cereal_box_1_2_107") object2 << canny_low << "\t" << canny_hight;
    else if (object == "cereal_box_2_2_200") object3 << canny_low << "\t" << canny_hight;
    else if (object == "flashlight_2_1_116") object4 << canny_low << "\t" << canny_hight;
    else if (object == "stapler_2_2_69") object5 << canny_low << "\t" << canny_hight;
}

std::vector<std::vector<int> > findSceneCorr( pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_Q,  pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_T,
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr final){
    typename pcl::search::Search<covis::core::CategorisedEdge>::Ptr s;
    s.reset(new pcl::search::KdTree<covis::core::CategorisedEdge>);
    s->setInputCloud(short_edge_T);
    std::vector<std::vector<int> > scene_correspondances;
    for (size_t p = 0; p < short_edge_Q->size(); p++){
        covis::core::CategorisedEdge point = (*final)[p];
        std::vector<int> idx;
        std::vector<float> distsq;
        s->radiusSearch(point, 0.01, idx, distsq);
        scene_correspondances.push_back(idx);

    }
    return scene_correspondances;
}

void computeFeatures(std::string featuresToCompute, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_Q, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_Q,
        const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_T, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_T, const std::vector<std::vector<int> > scene_correspondances,
        std::string object, std::string scene, ofstream &file_SHOT, ofstream &file_1D, ofstream &file_ECSAD, ofstream &file_SELECTION_NORM, ofstream &file_SELECTION_RATIO,
        const pcl::PointIndices::Ptr edge_indices_Q, const pcl::PointIndices::Ptr edge_indices_T,
        pcl::PointCloud<FeatureSHOT>::Ptr ftargetSHOT,
        pcl::PointCloud<FeatureEdgeCategorised>::Ptr ftarget1D, pcl::PointCloud<FeatureECSAD>::Ptr ftargetECSAD){

    if (featuresToCompute == "SELECTION" )
        compute1DDistanceAngleAndSHOTBestCorrespondances(edge_Q, edge_indices_Q, short_edge_Q, edge_T,
                edge_indices_T, short_edge_T, radiusDistance, radiusAngle, scene_correspondances,
                object, scene, file_SHOT, file_1D, file_ECSAD, file_SELECTION_NORM, file_SELECTION_RATIO,
                ftargetSHOT, ftarget1D, ftargetECSAD);


}

pcl::PointCloud<PointT>::Ptr loadQueryScene(std::string object_path){
    pcl::PointCloud<PointT>::Ptr something (new pcl::PointCloud<PointT>());
    COVIS_ASSERT(pcl::io::loadPCDFile<PointT>(object_path, *something) == 0);
    return something;
}

bool checkObjectEdgeSize(int size, std::string object){
    if (object == "magnet1")  if (size != 190) return false;
    if (object == "mirror1")  if (size != 4818) return false;
    if (object == "phone_yellow1")  if (size != 1349) return false;
    if (object == "wood1")  if (size != 8598) return false;
    if (object == "book1")  if (size != 11452) return false;

    if (object == "soda_can_6_1_27") if (size != 945) return false;
    if (object == "coffee_mug_5_1_47")  if (size != 753) return false;
    if (object == "cap_4_1_167")  if (size != 1249) return false;

    if (object == "green_ball")  if (size != 621) return false;
    if (object == "visitcard")  if (size != 1625) return false;
    if (object == "crest")  if (size != 2454) return false;
    if (object == "magasin")  if (size != 12591) return false;

    if (object == "stapler_00035")  if (size != 2404) return false;
    if (object == "cleaner_00000")  if (size != 1372) return false;
    if (object == "cleaner_00000")  if (size != 613) return false;
    if (object == "book_00000")  if (size != 2978) return false;
    if (object == "axle_00000")  if (size != 1245) return false;

    if (object == "cereal_box_4_1_145")  if (size != 8373) return false;
    if (object == "cap_4_1_170")  if (size != 1231) return false;
    if (object == "stapler_2_1_1")  if (size != 584) return false;
    if (object == "soda_can_1_1_141")  if (size != 935) return false;

    if (object == "kleenex_3_2_78")  if (size != 3082) return false;
    if (object == "cereal_box_1_1_198")  if (size != 6342) return false;
    if (object == "flashlight_3_1_33")  if (size != 780) return false;
    if (object == "flashlight_5_2_80")  if (size != 1182) return false;
    if (object == "bowl_4_2_36")  if (size != 979) return false;

    if (object == "bowl_4_2_72")  if (size != 916) return false;
    if (object == "cereal_box_1_2_149")  if (size != 6952) return false;
    if (object == "soda_can_3_2_50")  if (size != 1152) return false;
    if (object == "coffee_mug_1_1_145")  if (size != 1119) return false;

    if (object == "flashlight_2_1_117")  if (size != 702) return false;
    if (object == "coffee_mug_6_2_43")  if (size != 1151) return false;
    if (object == "bowl_3_1_1")  if (size != 1060) return false;
    if (object == "coffee_mug_5_1_122")  if (size != 875) return false;
    if (object == "stapler_2_2_73")  if (size != 531) return false;
    if (object == "kleenex_3_2_251")  if (size != 3092) return false;
    if (object == "cereal_box_2_2_200")  if (size != 5326) return false;
    if (object == "bowl_2_1_1")  if (size != 1461) return false;
    if (object == "cereal_box_1_2_113")  if (size != 6255) return false;

    if (object == "coffee_mug_5_1_74")  if (size != 783) return false;
    if (object == "cap_1_2_121")  if (size != 3919) return false;
    if (object == "soda_can_6_2_47")  if (size != 1217) return false;
    if (object == "soda_can_1_1_144")  if (size != 883) return false;
    if (object == "cereal_box_2_2_145")  if (size != 7110) return false;

    if (object == "stapler_2_1_53")  if (size != 254) return false;
    if (object == "soda_can_4_1_25")  if (size != 903) return false;
    if (object == "flashlight_1_1_17")  if (size != 1053) return false;
    if (object == "bowl_3_2_178")  if (size != 1227) return false;


    return true;
}


int run(int o, std::string object, std::string scene,
        ofstream &file_SHOT, ofstream &file_1D, ofstream &file_ECSAD, ofstream &file_SELECTION_NORM, ofstream &file_SELECTION_RATIO,
        std::string featuresToCompute, std::string object_path,
        const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_T, const pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_T,
        const pcl::PointIndices::Ptr edge_indices_T, MyMatrix transformation, pcl::PointCloud<FeatureSHOT>::Ptr ftargetSHOT,
        pcl::PointCloud<FeatureEdgeCategorised>::Ptr ftarget1D, pcl::PointCloud<FeatureECSAD>::Ptr ftargetECSAD){

    //-----------
    pcl::PointCloud<PointT>::Ptr query  = loadQueryScene(object_path);
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_Q (new pcl::PointCloud<covis::core::CategorisedEdge>());
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr short_edge_Q (new pcl::PointCloud<covis::core::CategorisedEdge>());
    pcl::PointIndices::Ptr edge_indices_Q (new pcl::PointIndices);


    getEdges(query, edge_Q, short_edge_Q, edge_indices_Q, true);

    /*if (!checkObjectEdgeSize(short_edge_Q->size(), object)){
        file_object_size << object << "- " << scene << std::endl;
        return 0;
    }*/

    if (showGroundTruthAlignment) covis::visu::showDetection<covis::core::CategorisedEdge>(short_edge_Q, short_edge_T, transformation);
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr final(new pcl::PointCloud<covis::core::CategorisedEdge>());
    pcl::transformPointCloud(*short_edge_Q, *final, transformation);

    const std::vector<std::vector<int> > scene_correspondances = findSceneCorr(short_edge_Q, short_edge_T, final);

    pcl::console::print_warn("%s\n", object.c_str());


    pcl::console::print_warn("object: %d  \n", o+1);
    computeFeatures(featuresToCompute, edge_Q, short_edge_Q, edge_T, short_edge_T, scene_correspondances, object, scene, file_SHOT, file_1D, file_ECSAD, file_SELECTION_NORM, file_SELECTION_RATIO, edge_indices_Q, edge_indices_T,
            ftargetSHOT, ftarget1D, ftargetECSAD);
    return 1;

}


std::string findShortName(std::string path){
    std::string object = path;
    const size_t ii = object.find_last_of("/");
    if(ii != std::string::npos)
    {
        object = (object.substr(ii+1).c_str());
    }

    object = object.substr(0, object.size()-4).c_str();
    return object;
}

int main(int argc, const char** argv) {
    core::ProgramOptions po;

    if(!po.parse(argc, argv))
        return 1;

    myfile.open ("results.txt");
    std::vector<std::string> experiments = getPaths(directory_experiments.c_str(), "experiments");
    canny_low = 30; canny_hight = 70;
    shotMINMAX.x = 0; shotMINMAX.y = 0;
    dMINMAX.x = 0; dMINMAX.y = 0;
    ecsadMINMAX.x = 0; ecsadMINMAX.y = 0;
    file_object_size.open("edge_size.txt");

    ofstream temp;
    temp.open("scenes_list.txt");
    std::string featuresToCompute = "SELECTION";

    myfile  << "object" << "\t" << "SHOT" << "\t1D"<< "\tECSAD" << "\tCombined"<<"\n";

    for (size_t o = 0; o < experiments.size(); o++){
        //process scene
        std::stringstream scene, scene_fileSHOT, scene_file1D, scene_fileECSAD, scene_fileSelection_norm, scene_fileSelection_ratio;
        scene << directory_scenes << findShortName(experiments[o]).c_str() << ".pcd";

        temp << findShortName(experiments[o]).c_str() << "\n";
        pcl::console::print_error("%d/%d %s\n", o+1, experiments.size(), findShortName(experiments[o]).c_str());

        //------------------------------------------------------------------------------------------------------------------
        scene_fileSHOT << findShortName(experiments[o]).c_str() << "_SHOT.txt";
        scene_file1D << findShortName(experiments[o]).c_str() << "_1D.txt";
        scene_fileECSAD << findShortName(experiments[o]).c_str() << "_ECSAD.txt";
        scene_fileSelection_norm << findShortName(experiments[o]).c_str() << "_SELECTION_NORM.txt";
        scene_fileSelection_ratio << findShortName(experiments[o]).c_str() << "_SELECTION_RATIO.txt";

        ofstream file_SHOT, file_1D, file_ECSAD, file_SELECTION_NORM, file_SELECTION_RATIO;

        file_SHOT.open(scene_fileSHOT.str().c_str());
        file_1D.open(scene_file1D.str().c_str());
        file_ECSAD.open(scene_fileECSAD.str().c_str());
        file_SELECTION_NORM.open(scene_fileSelection_norm.str().c_str());
        file_SELECTION_RATIO.open(scene_fileSelection_ratio.str().c_str());

        //-------------------------------------------------------------------------------------------------------------------------------
        pcl::PointCloud<PointT>::Ptr target = loadQueryScene(scene.str());
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge_T (new pcl::PointCloud<covis::core::CategorisedEdge>()), short_edge_T(new pcl::PointCloud<covis::core::CategorisedEdge>());
        pcl::PointIndices::Ptr edge_indices_T (new pcl::PointIndices);
        getEdges(target, edge_T, short_edge_T, edge_indices_T, false);
        //-------------------
        std::stringstream gound_truth_path;
        gound_truth_path << directory_ground_truths << findShortName(experiments[o]).c_str() << ".txt";
        vector <string>  gt;
        readFromFile (gound_truth_path.str().c_str(), gt);

        //for experiments, find objects
        vector <string>  mv;
        readFromFile (experiments[o].c_str(), mv);

        pcl::PointCloud<FeatureSHOT>::Ptr ftargetSHOT;
        pcl::PointCloud<FeatureEdgeCategorised>::Ptr ftarget1D;
        pcl::PointCloud<FeatureECSAD>::Ptr ftargetECSAD;
        ///////////////////////////////////////////////
	//precompute targets
        if (featuresToCompute == "SELECTION"){
            ftargetSHOT = getSHOT(edge_T, edge_indices_T, radiusDistance);
            for (size_t i = 0; i < ftargetSHOT->size(); i++){
                for (size_t j = 0; j < 352; j++){
                    if (!pcl_isfinite(ftargetSHOT->points[i].descriptor[j])) {
                        ftargetSHOT->points[i].descriptor[j] = 0;
                    }
                }
            }
            //------------------------------------------------------------------
            ftarget1D = getEdgeDistanceAngleHistogram(edge_T, short_edge_T, radiusAngle);
            ftargetECSAD = getECSAD(edge_T, short_edge_T, radiusDistance);
        } else if (po.getValue("features") == "1D"){
            ftarget1D = getEdgeDistanceAngleHistogram(edge_T, short_edge_T, radiusAngle);
        }
        //////////////////////////////////////////////

        int i = mv.size();
        for (i= i-1; i >=0; i--) {
            std::string object = findShortName(mv[i]);
            myfile << object;
            std::cout << object << std::endl;
            //find transformation
            MyMatrix transformation;
            fromVectoEigen(gt, transformation);

            run(o, object, findShortName(experiments[o]).c_str(), file_SHOT, file_1D, file_ECSAD, file_SELECTION_NORM, file_SELECTION_RATIO,
                    featuresToCompute, mv[i], edge_T, short_edge_T, edge_indices_T, transformation, ftargetSHOT, ftarget1D, ftargetECSAD);
            myfile << "\n";
            //remove
            mv.pop_back();
            gt.pop_back();
        }
    }
    file_object_size.close();
    myfile.close();

    pcl::console::print_warn("SHOT min: %f, max: %f\n", shotMINMAX.x, shotMINMAX.y );
    pcl::console::print_warn("1D min: %f, max: %f\n", dMINMAX.x, dMINMAX.y );
    pcl::console::print_warn("ECSAD min: %f, max: %f\n", ecsadMINMAX.x, ecsadMINMAX.y );

    ofstream minMaxFile;
    minMaxFile.open("MinMaxFile.txt");
    minMaxFile << "Shot min: " << shotMINMAX.x << ", max: " << shotMINMAX.y << std::endl;
    minMaxFile << "1D min: " << dMINMAX.x << ", max: " << dMINMAX.y << std::endl;
    minMaxFile << "ECSAD min: " << ecsadMINMAX.x << ", max: " << ecsadMINMAX.y << std::endl;
    minMaxFile.close();

    return 0;
}
