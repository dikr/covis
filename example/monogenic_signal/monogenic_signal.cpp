// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("imgfile", "image file");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;
    
    // Load image
    const cv::Mat img = cv::imread(po.getValue("imgfile"));
    COVIS_ASSERT(!img.empty());
    
    // Run monogenic signal filtering
    filter::MonogenicSignal ms;
    cv::Mat result = ms.filter(img);
    
    // Split into separate images
    std::vector<cv::Mat_<double> > mop;
    cv::split(result, mop);
    cv::Mat_<double> m = mop[0];
    cv::Mat_<double> o = mop[1];
    cv::Mat_<double> p = mop[2];
    
    // Normalize to [0,1] only for visualization purposes
    cv::normalize(m, m, 1, 0, CV_MINMAX);
    cv::normalize(o, o, 1, 0, CV_MINMAX);
    cv::normalize(p, p, 1, 0, CV_MINMAX);
    
    // Visualize in a montage
    cv::Mat all[] = {img, m, o, p};
    COVIS_MSG("Press any key to quit...");
    visu::showMontage("Input image, magnitude, orientation and phase", all, 4, 2);
    
    return 0;
}
