// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/registration/icp.h>

// OpenMP
#include <omp.h>

// Point and feature types
typedef pcl::PointXYZRGBNormal PointT;
//typedef feature::ECSAD<PointT>::Histogram FeatureT;
typedef feature::DistanceNormalHistogram<PointT,8,16>::Histogram FeatureT;

// Loaded point clouds and computed histogram features
pcl::PointCloud<PointT>::Ptr querySurf, targetSurf;
pcl::PointCloud<PointT>::Ptr query, target;
pcl::PointCloud<FeatureT>::Ptr fquery, ftarget;

// Downsample
void ds(pcl::PointCloud<PointT>::Ptr& cloud, float resolution);

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("pcd-query", "point cloud file for query point cloud");
    po.addPositional("pcd-target", "point cloud file for target point cloud");
    
    // Surfaces and normals
    po.addOption("resolution-surface", 'r', 0.001, "downsample point clouds to this resolution (<= 0 for disabled)");
    po.addOption("far", -1, "do not consider target points beyond this depth (<= 0 for disabled)");
    po.addOption("radius-normal", 'n', 2, "normal estimation radius in mr (<= 0 means two resolution units)");
    po.addFlag('o', "orient-query-normals", "ensure consistent normal orientation for the query model");
    
    // Features and matching
    po.addOption("resolution-query", 2, "resolution of query features in mr (<= 0 for two resolution units)");
    po.addOption("resolution-target", 5, "resolution of target features in mr (<= 0 for five resolution units)");
    po.addOption("radius-feature", 'f', 25, "feature estimation radius (<= 0 means 25 resolution units)");
    po.addOption("cutoff", 50, "use the <cutoff> % best L2 ratio correspondences for RANSAC");
    
    // Estimation
    po.addOption("iterations", 'i', 1000, "RANSAC iterations");
    po.addOption("inlier-threshold", 't', 5, "RANSAC inlier threshold (<= 0 for infinite)");
    po.addOption("inlier-fraction", 'a', 0.05, "RANSAC inlier fraction required for accepting a pose hypothesis");
    po.addFlag('e', "no-reestimate", "disable re-estimation of pose hypotheses using consensus set during RANSAC");
    po.addFlag('u', "full-evaluation", "enable full pose evaluation during RANSAC");
    po.addFlag('p', "prerejection", "enable prerejection during RANSAC");
    po.addOption("prerejection-similarity", 's', 0.9, "prerejection similarity threshold in [0,1]");
    po.addFlag('c', "no-occlusion-removal", "disable occlusion removal during pose hypothesis evaluation");
    
    // Refinement
    po.addFlag("refine", "apply pose refinement of the RANSAC result using ICP");
    po.addOption("icp-iterations", 100, "number of ICP iterations");
    
    // Misc.
    po.addFlag('v', "visualize", "show additional results");
    po.addFlag("experimental", "enter an experimental section for debugging purposes");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;
    po.print();
    
    // Load point clouds
    querySurf.reset(new pcl::PointCloud<PointT>);
    targetSurf.reset(new pcl::PointCloud<PointT>);
    COVIS_ASSERT(pcl::io::loadPCDFile<PointT>(po.getValue("pcd-query"), *querySurf) == 0);
    COVIS_ASSERT(pcl::io::loadPCDFile<PointT>(po.getValue("pcd-target"), *targetSurf) == 0);
    std::vector<int> dummy;
    pcl::removeNaNFromPointCloud(*querySurf, *querySurf, dummy);
    pcl::removeNaNFromPointCloud(*targetSurf, *targetSurf, dummy);
    
    // Surfaces and normals
    float resolution = po.getValue<float>("resolution-surface");
    const bool resolutionInput = (resolution > 0.0f);
    if(!resolutionInput)
        resolution = detect::computeResolution<PointT>(targetSurf);
    const float far = po.getValue<float>("far");
    const float nrad =
            po.getValue<float>("radius-normal") > 0.0 ?
                    po.getValue<float>("radius-normal") * resolution:
                    2 * resolution;
    const bool orientQueryNormals = po.getFlag("orient-query-normals");
    
    // Features and matching
    const float resQuery =
            po.getValue<float>("resolution-query") > 0.0 ? 
                    po.getValue<float>("resolution-query") * resolution :
                    2 * resolution;
    const float resTarget =
            po.getValue<float>("resolution-target") > 0.0 ? 
                    po.getValue<float>("resolution-target") * resolution :
                   5 * resolution;
    const float frad =
            po.getValue<float>("radius-feature") > 0.0 ?
                    po.getValue<float>("radius-feature") * resolution :
                    25 * resolution;
    const size_t cutoff = po.getValue<size_t>("cutoff");
    COVIS_ASSERT(cutoff > 0 && cutoff <= 100);
    
    // Estimation
    const size_t iterations = po.getValue<size_t>("iterations");
    const float inlierThreshold =
            (po.getValue<float>("inlier-threshold") > 0.0 ?
                    po.getValue<float>("inlier-threshold") * resolution : 
                    5 * resolution);
    const float inlierFraction = po.getValue<float>("inlier-fraction");
    const bool noReestimate = po.getFlag("no-reestimate");
    const bool fullEvaluation = po.getFlag("full-evaluation");
    const bool prerejection = po.getFlag("prerejection");
    const float prerejectionSimilarty = po.getValue<float>("prerejection-similarity");
    const bool noOcclusionRemoval = po.getFlag("no-occlusion-removal");
    
    // Refinement
    const bool refine = po.getFlag("refine");
    const size_t icpIterations = po.getValue<size_t>("icp-iterations");
    
    // Misc.
    const bool visualize = po.getFlag("visualize");
    const bool experimental = po.getFlag("experimental");
    
    // Remove far points
    if(far > 0.0f) {
        COVIS_MSG_INFO("Removing target points with a depth larger than " << far << "...");
        pcl::PassThrough<PointT> pt;
        pt.setInputCloud(targetSurf);
        pt.setFilterFieldName("z");
        pt.setFilterLimits(-far, far);
        pt.filter(*targetSurf);
    }
    
    // Downsample surfaces
    if(resolutionInput) {
        COVIS_MSG_INFO("Downsampling surfaces to a resolution of " << resolution << "...");
        ds(querySurf, resolution);
        ds(targetSurf, resolution);
    }
    
    // Check
    COVIS_ASSERT(!querySurf->empty());
    COVIS_ASSERT(!targetSurf->empty());
    
    /*
     * Compute surface normals
     */
    {
        core::ScopedTimer t("Surface normals with a radius of " + core::stringify(nrad));
        pcl::NormalEstimationOMP<PointT, PointT> ne;
        ne.setRadiusSearch(nrad);
        
        ne.setInputCloud(querySurf);
        ne.compute(*querySurf);
        if(orientQueryNormals) {
            COVIS_MSG_INFO("Pointing query normals outwards...");
            feature::computeCorrectedNormals(*querySurf);
        }
        
        ne.setInputCloud(targetSurf);
        ne.compute(*targetSurf);
    }
    
    // Generate feature points
    query.reset(new pcl::PointCloud<PointT>(*querySurf));
    target.reset(new pcl::PointCloud<PointT>(*targetSurf));
    
    if(resQuery > resolution) {
        COVIS_MSG_INFO("Creating query feature points with a resolution of " << resQuery << "...");
        ds(query, resQuery);
    }
    
    if(resTarget > resolution) {
        COVIS_MSG_INFO("Creating target feature points with a resolution of " << resTarget << "...");
        ds(target, resTarget);
    }
    
    // Check again
    COVIS_ASSERT(!query->empty());
    COVIS_ASSERT(!target->empty());
    
    /*
     * Compute features
     */
    {
        core::ScopedTimer t("Features with a radius of " + core::stringify(frad));
#pragma omp parallel sections
        {
//            fquery = feature::computeECSAD<PointT>(query, querySurf, frad);
//            ftarget = feature::computeECSAD<PointT>(target, targetSurf, frad);
            fquery = feature::computeDistanceNormalHistograms<PointT,8,16>(query, querySurf, frad);
#pragma omp section
            ftarget = feature::computeDistanceNormalHistograms<PointT,8,16>(target, targetSurf, frad);
        }
    }
    
    
    /*
     * Match features
     */
    core::Correspondence::VecPtr corr;
    {
        core::ScopedTimer t("Feature matching");
        corr = detect::computeRatioMatches<FeatureT>(fquery, ftarget);
    }
    
    /*
     * Show input L2 feature correspondences
     */
    if(visualize)
        visu::showCorrespondences<PointT>(query, target, *corr, 10,  visu::LINES, "Input L2 correspondences");
    
    /*
     * Sort correspondences and cutoff at <cutoff> %
     */
    core::sort(*corr);
    corr->resize(corr->size() * cutoff / 100);
    if(visualize)
        visu::showCorrespondences<PointT>(query, target, *corr, 10,  visu::LINES, "Ratio correspondences after cutoff");
    
    /*
     * Execute RANSAC
     */
    core::Detection d;
    {
        core::ScopedTimer t("RANSAC");
        detect::FitEvaluation<PointT>::Ptr fe(new detect::FitEvaluation<PointT>(target));
        fe->setOcclusionRemoval(!noOcclusionRemoval);
        
        detect::Ransac<PointT> ransac;
        ransac.setSource(query);
        ransac.setTarget(target);
        ransac.setCorrespondences(corr);
        ransac.setFitEvaluation(fe);
        
        ransac.setIterations(iterations);
        ransac.setInlierThreshold(inlierThreshold);
        ransac.setInlierFraction(inlierFraction);
        ransac.setReestimatePose(!noReestimate);
        ransac.setFullEvaluation(fullEvaluation);
        ransac.setPrerejection(prerejection);
        ransac.setPrerejectionSimilarity(prerejectionSimilarty);
        ransac.setVerbose(true);
        
        d = ransac.estimate();
        
        if(experimental) { // TODO: Experimental - remove!!!
            COVIS_MSG_WARN("ENTERING EXPERIMENTAL SECTION!");
            
            pcl::IterativeClosestPoint<PointT,PointT> icp[omp_get_max_threads()];
            for(int i = 0; i < omp_get_max_threads(); ++i) {
                icp[i].setInputSource(query);
                icp[i].setInputTarget(target);
                icp[i].setMaximumIterations(icpIterations);
                icp[i].setMaxCorrespondenceDistance(inlierThreshold);
            }
            
            core::Detection::Vec dall = ransac.getAllDetections();
            core::sort(dall);
            
            COVIS_MSG(dall.size() << " detections generated! Refining the top 50...");
            if(dall.size() > 50)
                dall.resize(50);

            core::Detection::Vec dicp(dall.size());
            core::ProgressDisplay pd(dall.size());
#pragma omp parallel for
            for(size_t i = 0; i < dall.size(); ++i) {
#pragma omp critical
                ++pd;
                
                pcl::PointCloud<PointT> tmp;
                icp[omp_get_thread_num()].align(tmp, dall[i].pose);
                dicp[i].pose = icp[omp_get_thread_num()].getFinalTransformation();
                dicp[i].rmse = dicp[i].penalty = icp[omp_get_thread_num()].getFitnessScore();
            }
            core::sort(dicp);
            
            COVIS_MSG("Done! Showing...");
            for(size_t i = 0; i < dicp.size(); ++i) {
                COVIS_MSG(dicp[i]);
                visu::showDetection<PointT>(querySurf, targetSurf, dicp[i].pose);
            }
        } // End experimental section
    }
    
    if(d) {
        if(refine) {
            core::ScopedTimer t("ICP");
            pcl::IterativeClosestPoint<PointT,PointT> icp;
            icp.setInputSource(query);
            icp.setInputTarget(target);
            icp.setMaximumIterations(icpIterations);
            icp.setMaxCorrespondenceDistance(inlierThreshold);
            pcl::PointCloud<PointT> tmp;
            icp.align(tmp, d.pose);
            if(icp.hasConverged()) {
                d.pose = icp.getFinalTransformation();
                d.rmse = icp.getFitnessScore();
            } else {
                COVIS_MSG_WARN("ICP failed!");
            }
        }
        
        // Print result and visualize
        COVIS_MSG(d);
        visu::showDetection<PointT>(querySurf, targetSurf, d.pose);
    } else {
        COVIS_MSG_WARN("RANSAC failed!");
    }
    
    return 0;
}

void ds(pcl::PointCloud<PointT>::Ptr& cloud, float resolution) {
    pcl::PointCloud<PointT> tmp;
    pcl::VoxelGrid<PointT> vg;
    vg.setLeafSize(resolution, resolution, resolution);
    vg.setInputCloud(cloud);
    vg.filter(tmp);
    *cloud = tmp;
}
