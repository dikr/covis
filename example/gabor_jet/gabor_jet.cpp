// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

#include <opencv2/opencv.hpp>

int main (int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("imgfile", "image file");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    // Load image
    const cv::Mat image = cv::imread(po.getValue("imgfile"));
    COVIS_ASSERT(!image.empty());

    // create jet space with kernel size 17, frequency 0.2, sigma 1, 8 rotations and five scales
    float k = 2;
    float p = 0.9;
    float sigma = (( k +1)/(k-1))* sqrt(-log(p));
    filter::GaborJet jet(17, 0.2, sigma, 8, 4);

    // Filter
    cv::Mat jetResult = jet.filter(image);
    
    // Prepare jet responses for visualization
    std::vector<cv::Mat> resultVector;
    cv::split(jetResult, resultVector);
    for(size_t i = 0; i < resultVector.size(); ++i)
        cv::normalize(resultVector[i], resultVector[i], 1, 0, CV_MINMAX);
    
    // Get filter banks for visualization
    cv::Mat realimag[2];
    realimag[0] = jet.getRealJet();
    realimag[1] = jet.getImaginaryJet();
    
    // Show
    cv::imshow("Real/imaginary jet kernels (x2)", visu::tile(realimag, 2, 1, 10, cv::Scalar::all(1), 5));
    cv::imshow("Input image", image);
    cv::imshow("Jet responses (x0.2)", visu::tile(resultVector, 4, 10, cv::Scalar::all(1), 0.2));
    cv::waitKey();

    return 0;
}
