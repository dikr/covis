// Copyright (c) 2014, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
//OpenCV
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
//PCL
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>

#include <dirent.h>
#include <boost/algorithm/string/split.hpp>

using namespace covis::core;
using namespace covis::calib;

int main(int argc, const char** argv) {
    // Setup program options
    ProgramOptions po;
    po.addPositional("pcdfiles", "pcds file");
    po.addOption("n", "rgb", "output folder name");
    po.addFlag('d', "directory", "read all files from directory");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    std::vector<std::string> objectsPath;

    const bool directory = po.getFlag("directory");

    if (directory) {
        const char* path = po.getValue("pcdfiles").c_str();

        DIR *dir;
        class dirent *ent;

        dir = opendir(path);
        std::string full_path;
        while ((ent = readdir(dir)) != NULL) {
            if(ent->d_type == DT_REG)
            {
                full_path = std::string(path);
                full_path.append(ent->d_name);
                objectsPath.push_back(full_path);
            }
        }
        std::cout << "amount of objects: " << objectsPath.size() << std::endl;
    }
    else  {
        objectsPath.push_back(po.getValue("pcdfiles"));
    }

    for (unsigned int i  = 0; i < objectsPath.size(); i++)
    {
        std::string pcd = objectsPath.at(i);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
        pcl::io::loadPCDFile (pcd, *cloud);
        std::cout << "loaded: " << pcd << std::endl;

        //get the rgb image
        ConvertInput<pcl::PointXYZRGBA> ci;
        cv::Mat rgb = ci.getRgbImageFromPointCloud(cloud);
        //
        cv::Mat_<ushort> depthm(cloud->height, cloud->width);
        for(int r = 0; r < depthm.rows; ++r)
        {
            for(int c = 0; c < depthm.cols; ++c)
            {
                depthm[r][c] = (ushort)((*cloud)(c,r).z * (-1000));
            }
        }

        //
        //        cv::Mat_<cv::Vec3b> rgb;
        //        cv::Mat_<ushort> depth;
        //        CameraCalibrationCV c = CameraCalibrationCV::KinectIdeal();
        //        ci.fromPointCloudToRGBDepth(cloud, c, rgb, depth);
        //
        //        cv::imwrite("rgb.png", rgb);
        //        cv::imwrite("depth.png", depth);
        std::stringstream path, pcdpath, pathDepth;
        path << po.getValue("n") << "/";
        mkdir(path.str().c_str(), 0777);
        path << "rgb/";
        pathDepth << po.getValue("n") << "/depth/";
        pcdpath << po.getValue("n") << "/pcds/";
        mkdir(path.str().c_str(), 0777);
        mkdir(pathDepth.str().c_str(), 0777);
        mkdir(pcdpath.str().c_str(), 0777);
        path << po.getValue("n") << "_"<< i << ".ppm";
        pathDepth << po.getValue("n") << "_"<< i << ".png";
        cv::imwrite(path.str(), rgb);
        cv::imwrite(pathDepth.str(), depthm);
        pcdpath << po.getValue("n") << "_"<< i << ".pcd";
        pcl::io::savePCDFile(pcdpath.str(), *cloud);


        std::cout << "saved in: " << path.str() << std::endl;
        std::cout << "saved in: " << pcdpath.str() << std::endl;
    }

    return 0;
}
