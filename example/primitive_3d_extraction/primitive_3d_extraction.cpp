// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>

//OpenCV
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//PCL
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/console/print.h>

using namespace covis;
using namespace covis::filter;
using namespace covis::feature;



int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("pcdfile", "pcd file");

    po.addFlag('l', "saveLsToPC", "save line segments 3d to point cloud");
    po.addFlag('t', "saveTexToPC", "save texlets 3d to point cloud");
    po.addFlag('v', "visualize", "enable visualization");

    po.addOption("id0", 0.0, "id0");
    po.addOption("id1", 0.6, "id1");
    po.addOption("id2", 0.3, "id2");
    po.addOption("lsFileName", "lineSegments.pcd", "line segments filename");
    po.addOption("texFileName", "texlets.pcd", "texlets filename");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    const float id0 = po.getValue<float>("id0");
    const float id1 = po.getValue<float>("id1");
    const float id2 = po.getValue<float>("id2");

    const bool saveLsToPC = po.getFlag("saveLsToPC");
    const bool saveTexToPC = po.getFlag("saveTexToPC");
    const bool visualize = po.getFlag("visualize");
    // load point cloud
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::io::loadPCDFile(po.getValue("pcdfile"), *cloud);
    COVIS_ASSERT(!cloud->empty());

    pcl::console::print_highlight("PointCloud loaded\n");

    //--------------------Texlets3D extraction
    if (saveTexToPC){
        Texlet3DExtraction<pcl::PointXYZRGBA> t3d = Texlet3DExtraction<pcl::PointXYZRGBA>();
        pcl::PointCloud<core::Texlet3D> t;
        {
            core::ScopedTimer ("Texlet3D extraction into PointCloud");
            t = t3d.extract3dTexletsToPointCloud(cloud, core::Ids(id0, id1, id2));
        }
        pcl::io::savePCDFile(po.getValue("texFileName"), t);

//        std::vector<core::Texlet3D> ll = t3d.extract3dTexlets(cloud, core::Ids(id0, id1, id2));
//        util::SaveECVPrimitives<core::Texlet2D, core::Texlet3D> se;
//        se.write3DPrimitivesToXMLFile(ll, "texlets2.xml");
    }
    //--------------------LineSegments3D extraction
    if (saveLsToPC){
        LineSegment3DExtraction<pcl::PointXYZRGBA> ls3d = LineSegment3DExtraction<pcl::PointXYZRGBA>();
        pcl::PointCloud<core::LineSegment3D> lsPointCloud;
        {
            core::ScopedTimer ("LineSegment3D extraction into PointCloud");
            lsPointCloud = ls3d.extract3dLineSegmentsToPointCloud(cloud, core::Ids(id0, id1, id2));
        }

        pcl::io::savePCDFile(po.getValue("lsFileName"), lsPointCloud);
    }

    //vizualization
    if (visualize){
        pcl::visualization::PCLVisualizer viewer ("3D Primitives");

        for (size_t idx = 0; idx < cloud->points.size (); idx++)
        {
            uint8_t gray = uint8_t ((cloud->points[idx].r + cloud->points[idx].g + cloud->points[idx].b) / 3);
            cloud->points[idx].r = cloud->points[idx].g = cloud->points[idx].b = gray;
        }
        viewer.addPointCloud (cloud, "original point cloud");

        if (saveLsToPC){
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr lsCloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
            pcl::io::loadPCDFile(po.getValue("lsFileName"), *lsCloud);
            viewer.addPointCloud(lsCloud, "lineSegments3D");
            viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "lineSegments3D");
            viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "lineSegments3D");
        }
        if (saveTexToPC){
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr texCloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
            pcl::io::loadPCDFile(po.getValue("texFileName"), *texCloud);
            viewer.addPointCloud(texCloud, "texlets3D");
        }
        while (!viewer.wasStopped ())
        {
            viewer.spinOnce ();
            boost::this_thread::sleep (boost::posix_time::microseconds (100));
        }
    }

    return 0;
}
