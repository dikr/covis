// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

#include <pcl/features/normal_3d_omp.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/point_picking_event.h>

// Desired histogram dimension
#define HIST_DIM 64

// Loaded point cloud and computed histogram features
pcl::PointCloud<pcl::PointNormal>::Ptr cloud;
pcl::PointCloud<pcl::Histogram<HIST_DIM> >::Ptr fhist;

// Point picker callback, shows a histogram of chosen feature
void callback(const pcl::visualization::PointPickingEvent& ppe, void*);

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("pcdfile", "point cloud file");
    po.addOption("radius-normal", 'n', 0.01, "normal estimation radius");
    po.addOption("radius-histogram", 'i', 0.035, "normal histogram estimation radius");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;
    
    const float nrad = po.getValue<float>("radius-normal");
    const float hrad = po.getValue<float>("radius-histogram");
    
    // Load point cloud
    cloud.reset(new pcl::PointCloud<pcl::PointNormal>);
    COVIS_ASSERT(pcl::io::loadPCDFile<pcl::PointNormal>(po.getValue("pcdfile"), *cloud) == 0);
    std::vector<int> dummy;
    pcl::removeNaNFromPointCloud(*cloud, *cloud, dummy);
    COVIS_ASSERT(!cloud->empty());
    
    {
        // Compute normals
        core::ScopedTimer t("Normals");
        pcl::NormalEstimationOMP<pcl::PointNormal, pcl::PointNormal> ne;
        ne.setInputCloud(cloud);
        ne.setRadiusSearch(nrad);
        ne.compute(*cloud);
    }
    
    {
        // Compute normal histograms
        core::ScopedTimer t("Normal histograms");
        feature::NormalHistogram<pcl::PointNormal, HIST_DIM> nh;
        nh.setRadius(hrad);
        fhist = nh.compute(cloud);
    }
    
    // Setup a point cloud visualizer
    visu::Visu3D visu;
    visu.addPointCloud<pcl::PointNormal>(cloud);
    visu.visu.registerPointPickingCallback(callback);
    visu.show();
    
    return 0;
}

void callback(const pcl::visualization::PointPickingEvent& ppe, void*) {
    const int idx = ppe.getPointIndex();
    if(idx >= 0 && size_t(idx) < fhist->size()) {
        COVIS_MSG("Picked point " << idx << ": " << cloud->points[idx]);
        const pcl::Histogram<HIST_DIM>& hidx = fhist->at(idx);
        const std::vector<float> hidxv(hidx.histogram, hidx.histogram + HIST_DIM);
        if(!core::zero<float>(hidxv)) {
            COVIS_MSG("Plotting the following histogram (press any key to close plot):");
            core::print(hidxv);
            visu::HistVisu<HIST_DIM> hv;
            hv.setHist(hidx.histogram);
            hv.show();
        }
    }
}
