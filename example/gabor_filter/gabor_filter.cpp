// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

#include <opencv2/opencv.hpp>

int main (int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("imgfile", "image file");
    po.addOption("kernel-size", 'k', 17, "kernel size");
    po.addOption("frequency", 'f', 0.2, "kernel frequency");
    po.addOption("sigma", 's', 2, "kernel standard deviation");
    po.addOption("theta", 't', 0, "kernel rotation");
    po.addOption("scale", 'a', 1, "kernel scale");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    // Load image
    const cv::Mat image = cv::imread(po.getValue("imgfile"));
    COVIS_ASSERT(!image.empty());

    // Create a Gabor filter with parameters
    filter::GaborFilter gaborFilter(
            po.getValue<int>("kernel-size"),
            po.getValue<float>("frequency"),
            po.getValue<float>("sigma"),
            po.getValue<float>("theta"),
            po.getValue<float>("scale"));

    // Filter image
    cv::Mat filteredImage = gaborFilter.filter(image);

    // Normalize image for viewing
    cv::normalize(filteredImage, filteredImage, 1, 0, CV_MINMAX);

    // Get kernels
    cv::Mat filters[] = {gaborFilter.getRealKernel(), gaborFilter.getImaginaryKernel()};
    cv::Mat images[] = {image, filteredImage};
    
    // Show kernels (x10) and input/output image
    cv::imshow("Real/imaginary kernels (x10)", visu::tile(filters, 2, 1, 10, cv::Scalar::all(1), 10));
    cv::imshow("Input/filtered image (x0.5)", visu::tile(images, 2, 1, 10, cv::Scalar::all(1), 0.5));
    cv::waitKey();
    
    return 0;
}
