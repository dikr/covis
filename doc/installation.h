/** \page installation Installation
 * \brief This page describes how to build and install CoViS
 *
 * \tableofcontents
 *
 *
 * \section dependencies Dependencies
 * In order to download and configure CoViS,
 * these are the <b>required applications</b>:
 *  - Git (http://git-scm.com)
 *  - CMake >= 2.8 (http://cmake.org)
 *
 * CoViS can then be compiled with a C++-compliant <b>compiler</b>. We have tested the
 * compilation on Ubuntu Linux with the following compilers:
 *  - GCC >= 4.7 (http://gcc.gnu.org)
 *  - clang >= 3 (http://clang.llvm.org)
 *
 * CoViS depends on a number of third-party libraries. The following
 * are <b>mandatory libraries</b>:
 *  - OpenCV >= 2.3 (http://opencv.org)
 *  - Eigen >= 3 (http://eigen.tuxfamily.org)
 *  - Boost >= 1.46 (http://www.boost.org)
 *  - PCL >= 1.7 (http://pointclouds.org)
 *  - VTK (http://www.vtk.org)
 *
 * The following are <b>optional libraries</b>:
 *  - CUDA (http://nvidida.com/cuda)
 *  - OpenMP (http://openmp.org)
 *  - OpenNI2 (http://www.openni.org)
 *  - NURBS++ (http://libnurbs.sourceforge.net/old)
 *
 * Additionally, the following <b>optional applications</b>
 * may come in handy (see \ref coding_guidelines):
 *  - Doxygen (http://www.doxygen.org)
 *  - Astyle (http://astyle.sourceforge.net)
 *
 * 
 * \section download Download
 * \covis has moved to Git. Use the following to clone \covis to your local file system:
\verbatim
git clone git@gitlab.com:caro-sdu/covis.git <directory>
\endverbatim
 * where <b>\<directory></b> will default to <b>covis</b> if left out.
 * Remember that this requires you to have a valid GitLab account (http://gitlab.com),
 * <b>and</b> be a member of the group <b>CARO-SDU</b>.
 * 
 * 
 * \section configuration_compilation Configuration and compilation
 * The CoViS build system is configured using CMake. The recommended approach is
 * to create an out or source build, e.g. by creating a subfolder called
 * \b build inside the CoViS source tree and run the configuration from there.
 * Assuming you have a terminal open in the CoViS source tree, run the
 * following:
\verbatim
mkdir build
cd build
cmake ..
make
\endverbatim
 *
 *
 * \cond
 * \subsection cpp11_build C++11 build
 * It is possible to compile \covis using the new C++11 standard
 * (default is the existing C++03 standard):
\verbatim
cmake -DCOVIS_CXX11=ON ..
\endverbatim
 * Note that this functionality is experimental
 * \endcond
 * 
 * 
 * \subsection build_flags Build flags
 * \covis automatically adds required flags to the build.
 * These are stored in the advanced CMake cache variable <b>COVIS_DEFINITIONS</b>.
 * If you want to add your own flags to the build, add them to the cache variable
 * <b>COVIS_DEFINITIONS_EXTRA</b>, e.g. from the command-line:
\verbatim
cmake -DCOVIS_DEFINITIONS_EXTRA="-ansi;-pedantic-errors" ..
\endverbatim
 *
 *
 * \subsection static_build Static build
 * You can do a static build by changing the CMake command as follows:
\verbatim
cmake -DCOVIS_BUILD_STATIC=ON ..
\endverbatim
 *
 *
 * \subsection generating_documentation Generating documentation
 * To generate the documentation (requires Doxygen, see \ref documentation), run
 * the following:
\verbatim
make doc
\endverbatim
 * The documentation will appear inside \b build/doc with the index file
 * \b build/doc/html/index.html.
 *
 *
 * \subsection source_code_styling Source code styling
 * To apply formatting to source files (requires Astyle, see
 * \ref automatic_styling), use:
\verbatim
make style
\endverbatim
 *
 *
 * \subsection library_installation Library installation
 * If you want to install the CoViS libraries, just run (as super user):
\verbatim
make install
\endverbatim
 * The root path of the installation can be changed in the CMake variable
 * \b CMAKE_INSTALL_PREFIX.
 * 
 * The \b install target installs all headers and libraries into the \b include
 * and \b lib (\b bin for DLLs) subdirectories of the \b CMAKE_INSTALL_PREFIX
 * path. Additionally, a find script \b FindCOVIS.cmake is installed into the
 * CMake module path, making it possible to use your CoViS installation from
 * another project (see \ref using_covis). Finally, if documentation was
 * generated, this will appear inside the subdirectory \b share/doc/covis of
 * \b CMAKE_INSTALL_PREFIX.
*/
