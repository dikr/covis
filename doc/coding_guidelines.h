/** \page coding_guidelines Coding guidelines
 * 
 * \brief This page describes all you need to know in order to put your
 * own code into @covis, both regarding file structures and coding styles.
 * 
 * Apart from file naming conventions (which in our case uses lowercase
 * names), we have tried to align with the RobWork coding guidelines:
 * http://www.robwork.dk/apidoc/nightly/rw/page_coding_guidelines.html.
 * 
 * \attention DO NOT contribute code to @covis before reading this document!
 * Pay special attention to the \ref good_practices section while coding.
 * We are aware that this page contains a lot of information. However, it is
 * based on several years of experience on programming,
 * so please spend the time required to read through this document.
 * 
 * \tableofcontents
 * 
 * \section source_files Source files
 * All functionality is encapsulated inside C++ classes. A class is
 * written into three types of source files: regular headers,
 * implementation headers and source files. These are then arranged in a certain
 * directory structure.
 * 
 * \subsection source_file_types Source file types
 * 
 * <b>Headers</b> have the extension *.h and contain only class/function declarations,
 * with the exception of inline functions which must be defined in the header.
 * You are also allowed to put constructor/destructor bodies in here.
 * Additionally, an implementation header is included at the bottom of the file,
 * below all terminating namespaces.
 * 
 * <b>Implementation headers</b> (*.hpp) are used for defining
 * non-specialized (general) function templates.
 * 
 * <b>Sources</b> (*.cpp) contain explicit template specializations
 * and definitions of non-templated functions. Static members and
 * utility functions in empty namespaces must be put here also.
 *
 * Every file must contain license information in the very top of the
 * file. The license can be copy-pasted from the file \b LICENSE.txt in the
 * root.
 * 
 * \subsection directory_structure Directory structure
 * The source code part of @covis is located inside the subfolder
 * \b src/covis. Each of these folders contains module-specific
 * functionality, so when contributing, you first need to identify to
 * which module the functionality of your code should belong. For example,
 * a \b core class \b ImageRGB would be placed as:
\verbatim
src/covis/core/image_rgb.h
src/covis/core/image_rgb_impl.hpp
src/covis/core/image_rgb.cpp
\endverbatim
 * Note that the above example also reveals the file naming convention,
 * which uses only lowercase words, separated by underscores. Note also that
 * implementation headers should always have the suffix \b _impl and the
 * extension <b>.hpp</b>.
 * To add your code to the build system, just add the source file name to the
 * CMakeLists.txt file in the module folder. Have a look at the existing
 * module folders for examples.
 * 
 * If you wish to create a new module, do not
 * forget to add a CMakeLists.txt file inside the new folder,
 * where you fill a CMake variable called SOURCES, which must be set in PARENT_SCOPE:
\code{.cmake}
set(SOURCES
    source_a.cpp
    source_b.cpp
    .
    .
    .
    source_z.cpp
    PARENT_SCOPE)
\endcode
 *
 * 
 * \subsubsection global_headers Global headers
 * Inside the \b src/covis directory, @covis also provides global include
 * headers which are put in the main include directory upon
 * installation. There is one header per module, e.g. \b %core.h. When
 * you create a new class, do not forget to add it to the module header,
 * e.g. <b>%src/covis/core.h</b>, in <b>alphabetical order</b>. Example:
\code
// License

#ifndef COVIS_CORE_H
#define COVIS_CORE_H

#include "core/covis_base.h"
#include "core/image_rgb.h"

#endif
\endcode
 *
 *
 * If a whole new module is created, you need to create the module
 * header yourself and also include that module header in the project header
 * \b %covis.h. Additionally, you should create a top-level module class that
 * inherits the @covis base class \b %core::CovisBase. Below is given an example
 * for a new module called \b recognition, with the base class header
 * \b src/covis/recognition/recognition_base.h:
\code
// License

#ifndef COVIS_RECOGNITION_RECOGNITION_BASE_H
#define COVIS_RECOGNITION_RECOGNITION_BASE_H

#include "../core/covis_base.h"

namespace covis {
    namespace recognition {
        // Class documentation
        class RecognitionBase : core::CovisBase {
            // Class body
        };
    }
}

#endif
\endcode
 * Every class inside the newly created \b recognition module should inherit from
 * this class.
 * 
 * In the \b src/covis directory, you now create a module header called
 * \b recognition.h (license and documentation not shown):
\code
// License

#ifndef COVIS_RECOGNITION_H
#define COVIS_RECOGNITION_H

// All module headers should be included here
#include "recognition/recognition_base.h"

// Module documentation

#endif
\endcode
 * 
 * \section Coding_conventions Coding conventions
 * This section describes the naming and formatting conventions used inside the
 * source files.
 * 
 * \subsection naming Naming
 * The following rules apply for source code:
 *  - \b Namespaces are in lowercase, separated by underscores, e.g. \b filter
 *  - \b Classes begin with capital letters and are camel cased,
 *    e.g. \b MonogenicSignal
 *  - <b>Class member functions</b> (verbs) begin with small letters and
 *    are camel cased, e.g. \b extractTexlets()
 *  - <b>Class member variables</b> are normally prefixed with an underscore,
 *    begin with small letters and are camel cased, e.g. \b _useMonogenic.\n
 *    <i>However</i>, we make the following exceptions:
 *     - <b>Static class member functions/variables</b> begin with capital
 *       letters and are camel cased, just like classes, e.g.
 *       <b>void DoSomethingStatically()</b>
 *     - <b>Public member variables</b> have no underscore, to make them easily
 *       accessible in user code, e.g.:
 *       \code{.cpp}
 *       struct Point {
 *           float x;
 *           float y;
 *           float z;
 *           static unsigned int InstanceCount;
 *       };
 *       \endcode
 *  - <b>Template declarations</b> (capital, camel-cased) are made on a separate line above the 
 *    class/function declarations, e.g.:
\code
template<typename VecT>
double norm2(const VecT& a);
\endcode
 * 
 * \subsection bracketing_indentation Bracketing and indentation
 * @covis uses a custom bracketing and indentation style. The
 * following rules apply:
 *  - <b>Begin brackets</b> are never broken, and <b>end brackets</b> are always broken. 
 *  - \b Indentation is 4 spaces
 *  - <b>Namespace, class and switch contents</b> are indented
 *  - <b>No spaces around parentheses</b>, both inside and outside
 *  - <b>Arithmetic operators</b> should, contrary to parentheses,
 *    always have spaces around them
 *  - <b>Line wrapping</b> should be done after 120 columns
 * 
 * \subsubsection automatic_styling Automatic styling
 * If you use \b Eclipse (http://www.eclipse.org), you can load the code formatter profile inside
 * <b>project/eclipse_formatter.xml</b> for automatic formatting while coding.
 *  
 * If you have \b Astyle installed (http://astyle.sourceforge.net), the
 * @covis build system will automatically create a make target called
 * \b style. When you make this target, the whole source tree inside
 * \b src is traversed, and every source file is formatted. The full command
 * executed for each source file is as follows:
\verbatim
astyle --brackets=attach --indent=spaces=4 --indent-namespaces --indent-classes --indent-switches --unpad-paren --pad-oper --max-code-length=120 --suffix=none --formatted <file>
\endverbatim
 * \note It is strongly recommended that you use the the <b>style</b> target before contributing any code to the @covis repository!
 * \note Automatic line wrapping and operator padding is only supported if you
 * have Astyle >= 2.0.3, otherwise you need to carefully verify
 * that your code conforms to these rules manually.
 * 
 * \subsection preprocessor_directives Preprocessor directives
 * After the license, headers and implementation headers should define a unique
 * name. Here we use uppercase words, separated by underscores. For the example
 * class \b %covis::feature_extraction::TexletExtraction, the header
 * \b texlet_extraction.h will have the following structure:
\code
// License

#ifndef COVIS_FEATURE_TEXLET_EXTRACTION_H
#define COVIS_FEATURE_TEXLET_EXTRACTION_H

// Includes

// Documentation and class body

// Include implementation header
#include "texlet_extraction_impl.hpp"

#endif
\endcode
 * Likewise, the implementation header will be named
\code
COVIS_FEATURE_TEXLET_EXTRACTION_IMPL_HPP
\endcode
 * 
 * 
 * \subsubsection macros Macros
 * Macros should be in uppercase, separated by underscores, and should be
 * defined after the includes:
\code
// License

#ifndef COVIS_FEATURE_TEXLET_EXTRACTION_H
#define COVIS_FEATURE_TEXLET_EXTRACTION_H

// Includes

// Return the minimum of two numbers
#define CMP_MIN(a,b) (a < b ? a : b)

// Documentation and class body

// Include implementation header
#include "texlet_extraction_impl.hpp"

#endif
\endcode
 * 
 * 
 * \subsection documentation Documentation
 * Code documentation is done using Doxygen (http://www.doxygen.org). If you
 * have Doxygen installed, the @covis build system will automatically create a
 * make target called \b covis_doc.
 *
 * The global module header defines the module using the command \b \@defgroup
 * and provides a general introduction to the module. Each class header then
 * adds to the group and gives both a brief and a full introduction to the
 * class, and finally provides author information.
 *
 * Functions should be documented by a \b \@brief description, optionally a full
 * description and a description of each parameter (\b \@param) and return value
 * (\b \@return), if any. Optionally, add a description of possible exception
 * conditions using \b \@exception.
 *
 * Data members and void functions with empty parameter lists
 * (e.g. empty constructors) can be documented by the single line /// syntax:
\verbatim
/// Empty constructor
void MonogenicSignal() {}
\endverbatim
 * 
 * 
 * \section good_practices Good practices
 * Below is a (non-exhaustive) list of coding rules to consider while coding:
 
 * - <i>Avoid reimplementations</i>\n
 * Take a careful look into the existing
 * @covis code base and the functionality of the dependencies before implementing
 * basic functionalities, e.g. image filtering operations. Many software
 * projects suffer from reimplementations of the same behaviors, which makes it
 * very hard to both use and understand the code.
 * \n\n
 * - <i>Use the following libraries as much as possible for representing data
 * types:</i>
 *   - OpenCV (http://opencv.org): images, 2D features etc.
 *   - PCL (http://pointclouds.org): 3D feature/point clouds etc.
 *   - Eigen (http://eigen.tuxfamily.org): matrices, vectors, quaternions etc.
 *   .
 * @covis provides many converter functions between these libraries inside
 * \ref core, and you are welcome to add to these.
 *\n\n
 * - <i>Do not create data storage classes unless absolutely necessary</i>\n
 * In @covis, we aim at providing easy-to-use algorithms, and this is best achieved
 * when these algorithms operate on known, well-tested data types. Therefore, do
 * not create e.g. your own vector or array classes if STL/Boost containers suffice
 * (which they almost always do). Another example is feature types, either for
 * images or for point clouds. @covis has both OpenCV and PCL as mandatory
 * dependencies, so you are expected to check whether the feature that your
 * algorithm produces fits into exising containers, e.g. <b>pcl::Histogram</b>.
 * Yet another example is matrix/vector representations, which are provided by
 * Eigen.
 * \n
 * If, however, you need to define a new feature, DO NOT put it in the \ref feature
 * module, which is made only for feature extraction functionality. Use instead
 * the \ref core module for this purpose.
 * \n\n
 * - <i>Use templates instead of inheritance, but only for simple interfaces</i>\n
 * Templates provide a nice way to produce generic code, but with the risk
 * of losing readability. If you've ever tried to read Boost code, you know
 * exactly what is meant by this. We encourage you to use templating only for simple
 * interfaces. For example, a function for calculating the 2-norm of a 3D array
 * could look like this:
\code
template<typename VecT>
inline double norm2(const VecT& a) {
    return std::sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
}
\endcode
 * The "interface" of <b>VecT</b> is only expected to provide the square bracket
 * operator in this case. A counter example is an image filtering class. It
 * could be tempting to template the class on the image container type, but this
 * would get messy, since the algorithms would most likely make use of many
 * functions of the image class (operators, constructors, allocations etc.).
 * We therefore advice that such a class is designed to simply take as input a
 * <b>cv::Mat</b> and produce another <b>cv::Mat</b> as the filtered output.
 * \n\n
 * - <i>Consider thread safety for your functions</i>\n
 * Since parallel processing is becoming more and more popular, using e.g. OpenMP,
 * you may wish to exploit these possibilities for your classes by making it possible
 * to call member functions from multiple threads. In by far the most cases,
 * thread safety of a member function can be easily achieved if you keep two rules in mind:
 *   1. Make sure your member function does not access class members or static variables.
 *      A good rule of thumb is that if you create a <b>const</b> function,
 *      it is thread safe. The method is not foolproof, though, since constness can be cast away.
 *   2. If your member function must access (read/write) member variables,
 *      use a mutex to protect the variable in the critical part of your code.
 *   .
 * The following code demonstrates both a safe and an unsafe construction:
\code
class ThreadWorker {
    public:
        // OK: Function has only access to local members
        // The outside application needs to make sure that multiple threads do not
        // call this function on the same counter without mutex locks
        void funcLocalSafe(int& someCounter) const {
            ++someCounter;
        }
        
        // ERROR: Multiple threads can try to write to _globalCounter at the same time
        void funcGlobalUnsafe() {
            ++_globalCounter;
        }
        
        // OK: Mutex protection makes sure only one thread writes to the member at a time
        void funcGlobalSafe() {
            boost::mutex::scoped_lock lock(_mutexGlobalCounter);
            ++_globalCounter;
        }
        
        // ERROR: If another thread is doing a write, _globalCounter will potentially contain invalid data
        int getGlobalCounterUnsafe() {
            return _globalCounter;
        }
        
        // OK: Reading is done only when member is available to avoid invalid data
        int getGlobalCounterSafe() {
            int tmp;
            {
                boost::mutex::scoped_lock lock(_mutexGlobalCounter);
                tmp = _globalCounter;
            }
            return tmp;
        }
        
    private:
        // Class variable and mutex
        int _globalCounter;
        boost::mutex _mutexGlobalCounter;
};
\endcode
 * \n\n
 * - <i>Avoid excessively long function names</i>\n
 * The idea that very long function names makes code more readable is illusory:
\code
applyFilterOnlyToIndexedPoints(cv::Mat& image, const std::vector<cv::Point>& points);
\endcode
* Instead, spend your efforts on documenting your code, and remember that 
* programmers should be able to easily derive the functionality of functions
* from a combination of the documentation and the
* class/function/parameter names. Thus, the following would suffice:
* \code
* // Function documentation
* MonogenicSignal::filter(cv::Mat& image, const std::vector<cv::Point>& points);
* \endcode
 * \n
 * - <i>Use the provided macros for logs, assertions and exceptions</i>\n
 * The header \ref core/macros.h defines a set of macros which should be used
 * for both crucial (release mode) and regular (debug mode) assertions (COVIS_ASSERT*)
 * exceptions (\ref COVIS_THROW), colored messages (COVIS_MSG*) and logs (COVIS_LOG*).
 * Use these macros instead of \b printf() or \b std::cout.
 *
 *   - <b>Messages (\ref COVIS_MSG):</b> These macros always print to standard output, and provide
 * nicely formatted messages. Avoid using these macros (unless outside
 * of a class scope), and prefer the log macros.
 *   - <b>Logs (\ref COVIS_LOG):</b> These macros behave the same way as the COVIS_MSG* macros, but
 * only print conditionally (depending on the log level), and can only be used
 * from within classes (since they reference \b this). The global variable
 * \ref CovisLogLevel is initialized to \ref COVIS_LOG_LEVEL_ERROR.
 * Try to avoid using \ref COVIS_LOG_ALL, which always prints
 * (like \ref COVIS_MSG), and reduce the use of \ref COVIS_LOG_INFO to very
 * important messages. Use \ref COVIS_LOG_WARN to indicate conditions which
 * create non-critical behavior, and use
 * \ref COVIS_LOG_ERROR/\ref COVIS_THROW/\ref COVIS_ASSERT/\ref COVIS_ASSERT_MSG
 * to abort execution due to critical conditions. For debugging purposes,
 * you can increase the log level using \ref COVIS_SET_LOG_LEVEL, or at build
 * time using the CMake variable <b>COVIS_LOG_LEVEL</b>.
 *   - <b>Exceptions (\ref COVIS_THROW):</b> Print an error message and throw an exception to indicate a
 * critical runtime condition.
 *   - <b>Assertions (\ref COVIS_ASSERT):</b> Check if an important condition is satisfied, and invoke \ref
 * COVIS_THROW if not. The macro also comes in an additional form where an error
 * message can be specified. Additionally, both release (\ref COVIS_ASSERT/\ref COVIS_ASSERT_MSG)
 * and debug (\ref COVIS_ASSERT_DBG/\ref COVIS_ASSERT_DBG_MSG) versions exist.
 * Use the debug versions as much as possible, and the release versions only
 * where your code cannot be expected to proceed any further.
 * \n\n
 * - <i>Add global convenience functions for your class, if possible</i>\n
 * For some classes, the functionality can be easily encapsulated into a single function call.
 * In such cases, you should provide a namespace function which instantiates the class, performs the computation
 * and returns the result in one go. Put the function below your class definition (inside the namespace),
 * and remember to document it properly, placing the documentation correctly using <b>\@ingroup</b>.
 * See the function \ref covis::feature::computeNormalHistograms() in the file \ref src/covis/feature/normal_histogram.h
 * for an example.
 * \n\n
 * - <i>Add a unit test for your class</i>\n
 * When you've completed your excellent contribution to \covis in the form of one or more new classes,
 * please also add a meaningful unit test for future maintenance. The directory <b>test</b> contains
 * a source file for each module, and you should add a test case for your class inside the appropriate
 * test file. All tests are run with <b>test/data</b> as working directory,
 * so any necessary test data should be placed here. We follow these test rules:
 * 
 *   - <b>Test library:</b> We use the Boost Test Library (http://www.boost.org/doc/libs/release/libs/test).
 *   Each \covis module has its own test module encapsulated in a single source file,
 *   e.g. <b>test_filter.cpp</b>.
 *   - <b>Structure:</b> Each class or source file has its own test suite, and ideally
 *   each public function has its own test case inside that test suite. The test suite is named as
 *   \<module\>_\<class\>, e.g. <b>filter_monogenic_signal</b>, and the test case is appended
 *   the function name, e.g. <b>filter_monogenic_signal_filter</b>. Have a look at <b>test/test_filter.cpp</b>
 *   for an example.
 *   - <b>Writing a test:</b> Try to make your unit tests as independent as possible (you don't want
 *   your test to fail each time you make changes to your code), and try to avoid adding
 *   to the data folder unless absolutely necessary. Simulated data will do in most cases.
 */