# Helper script for setting a 'covis_style' target
#
# The script reads the following variables:
#   COVIS_LIBRARY
#   COVIS_MODULE_DIRS
#


# Check that the module directories are available
if(NOT COVIS_LIBRARY)
  message(FATAL_ERROR "COVIS_LIBRARY not defined!")
endif(NOT COVIS_LIBRARY)
if(NOT COVIS_MODULE_DIRS)
  message(FATAL_ERROR "COVIS_MODULE_DIRS not defined!")
endif(NOT COVIS_MODULE_DIRS)

# Only add style target if Astyle is found
if(ASTYLE_FOUND)
  # Get the Astyle version
  execute_process(COMMAND ${ASTYLE_EXECUTABLE} --version
                  ERROR_VARIABLE ASTYLE_ERR_VAR)
  string(REGEX MATCHALL "[0-9.]" ASTYLE_VERSION "${ASTYLE_ERR_VAR}")
  string(REPLACE ";" "" ASTYLE_VERSION "${ASTYLE_VERSION}")

  # Print status
  message(STATUS ">> Astyle ${ASTYLE_VERSION} found")
  
  # Add the target
  add_custom_target(style)
  
  # Add the root source directory
  set(COVIS_MODULE_DIRS_ALL "." ${COVIS_MODULE_DIRS})
  
  # Associate astyle command to each source directory
  foreach(MODULE_DIR ${COVIS_MODULE_DIRS_ALL})
    # From Astyle 2.0.3 and on, we also have '--pad-oper' and '--max-code-length'
    if(ASTYLE_VERSION VERSION_GREATER 2.0.2)
      add_custom_command(TARGET style
                         COMMAND ${ASTYLE_EXECUTABLE} ARGS --brackets=attach --indent=spaces=4 --indent-namespaces --indent-classes --indent-switches --unpad-paren --pad-oper --max-code-length=120 --suffix=none --formatted --recursive \"${CMAKE_SOURCE_DIR}/src/covis/${MODULE_DIR}/*.h\" \"${CMAKE_SOURCE_DIR}/src/covis/${MODULE_DIR}/*.hpp\" \"${CMAKE_SOURCE_DIR}/src/covis/${MODULE_DIR}/*.cpp\")
    else(ASTYLE_VERSION VERSION_GREATER 2.0.2)
      add_custom_command(TARGET style
                         COMMAND ${ASTYLE_EXECUTABLE} ARGS --brackets=attach --indent=spaces=4 --indent-namespaces --indent-classes --indent-switches --unpad-paren --suffix=none --formatted --recursive \"${CMAKE_SOURCE_DIR}/src/covis/${MODULE_DIR}/*.h\" \"${CMAKE_SOURCE_DIR}/src/covis/${MODULE_DIR}/*.hpp\" \"${CMAKE_SOURCE_DIR}/src/covis/${MODULE_DIR}/*.cpp\")
    endif(ASTYLE_VERSION VERSION_GREATER 2.0.2)
  endforeach(MODULE_DIR)
endif(ASTYLE_FOUND)