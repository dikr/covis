# Helper script for finding all CoViS dependencies.
# The script adds information to the following variables:
#
#   COVIS_INCLUDE_DIRS
#   COVIS_LIBRARY_DIRS
#   COVIS_LIBRARIES
#   COVIS_DEFINITIONS
#
#
# These libraries are mandatory, and a fatal error will be issued if one
# of these is missing:
#
#   OpenCV
#   Eigen
#   Boost
#   PCL
#
#
# These are optional:
#
#   Astyle
#   Doxygen
#   VTK
#   CUDA
#   OpenMP
#   OpenNI2
#   NurbsPP
#


# Point CMake module path to here
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

##
# Set version requirements
# Remember to update doc/installation.h if you change these
##
set(OPENCV_REQUIRED_VERSION 2.3)
set(BOOST_REQUIRED_VERSION 1.46)
set(PCL_REQUIRED_VERSION 1.7)

##
# Find OpenCV (mandatory)
##
find_package(OpenCV ${OPENCV_REQUIRED_VERSION} QUIET)
if(OpenCV_FOUND)
  message(STATUS ">> OpenCV ${OpenCV_VERSION} found")
  if(OpenCV_VERSION VERSION_LESS 3)
    set(OPENCV_3 OFF)
  else(OpenCV_VERSION VERSION_LESS 3)
    set(OPENCV_3 ON)
  endif(OpenCV_VERSION VERSION_LESS 3)
else(OpenCV_FOUND)
  message(FATAL_ERROR ">> OpenCV 2.3 or higher not found!")
endif(OpenCV_FOUND)
list(APPEND COVIS_INCLUDE_DIRS ${OpenCV_INCLUDE_DIR})
list(APPEND COVIS_LIBRARIES ${OpenCV_LIBS})
mark_as_advanced(OpenCV_DIR)

##
# Find Eigen 3 (mandatory)
# TODO: Make silent
##
find_package(Eigen3 QUIET)
if(EIGEN_FOUND)
  message(STATUS ">> Eigen ${PC_EIGEN_VERSION} found")
  list(APPEND COVIS_INCLUDE_DIRS ${EIGEN_INCLUDE_DIRS})
  list(APPEND COVIS_DEFINITIONS ${EIGEN_DEFINITIONS})
else(EIGEN_FOUND)
  message(FATAL_ERROR ">> Eigen 3 or higher not found!")
endif(EIGEN_FOUND)

##
# Find Boost (mandatory)
##
find_package(Boost ${BOOST_REQUIRED_VERSION} QUIET COMPONENTS program_options unit_test_framework test_exec_monitor)
if(Boost_FOUND)
  message(STATUS ">> Boost ${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}.${Boost_SUBMINOR_VERSION} found")
  list(APPEND COVIS_INCLUDE_DIRS ${Boost_INCLUDE_DIRS})
  list(APPEND COVIS_LIBRARIES ${Boost_LIBRARIES})
  mark_as_advanced(BOOST_THREAD_LIBRARY)
else(Boost_FOUND)
  message(FATAL_ERROR ">> Boost not found!")
endif(Boost_FOUND)
mark_as_advanced(Boost_DIR)


##
# Find PCL (mandatory)
# TODO: Make 'flann' and 'openni-dev' silent
##
SET(PkgConfig_FIND_QUIETLY TRUE)
SET(eigen_FIND_QUIETLY TRUE)
SET(Flann_FIND_QUIETLY TRUE)
SET(libusb-1.0_FIND_QUIETLY TRUE)
SET(openni_FIND_QUIETLY TRUE)
SET(opennidev_FIND_QUIETLY TRUE)
SET(qhull_FIND_QUIETLY TRUE)
SET(PCL_FIND_QUIETLY TRUE)
SET(PCL_2D_FIND_QUIETLY TRUE)
SET(PCL_APPS_FIND_QUIETLY TRUE)
SET(PCL_COMMON_FIND_QUIETLY TRUE)
SET(PCL_FEATURES_FIND_QUIETLY TRUE)
SET(PCL_FILTERS_FIND_QUIETLY TRUE)
SET(PCL_GEOMETRY_FIND_QUIETLY TRUE)
SET(PCL_IO_FIND_QUIETLY TRUE)
SET(PCL_KDTREE_FIND_QUIETLY TRUE)
SET(PCL_KEYPOINTS_FIND_QUIETLY TRUE)
SET(PCL_ML_FIND_QUIETLY TRUE)
SET(PCL_OCTREE_FIND_QUIETLY TRUE)
SET(PCL_OUTOFCORE_FIND_QUIETLY TRUE)
SET(PCL_PEOPLE_FIND_QUIETLY TRUE)
SET(PCL_RECOGNITION_FIND_QUIETLY TRUE)
SET(PCL_REGISTRATION_FIND_QUIETLY TRUE)
SET(PCL_SAMPLE_CONSENSUS_FIND_QUIETLY TRUE)
SET(PCL_SEARCH_FIND_QUIETLY TRUE)
SET(PCL_SEGMENTATION_FIND_QUIETLY TRUE)
SET(PCL_STEREO_FIND_QUIETLY TRUE)
SET(PCL_SURFACE_FIND_QUIETLY TRUE)
SET(PCL_TRACKING_FIND_QUIETLY TRUE)
SET(PCL_VISUALIZATION_FIND_QUIETLY TRUE)
find_package(PCL ${PCL_REQUIRED_VERSION} QUIET)
if(PCL_FOUND)
  message(STATUS ">> PCL ${PCL_VERSION} found")
  list(APPEND COVIS_INCLUDE_DIRS ${PCL_INCLUDE_DIRS})
  list(APPEND COVIS_LIBRARY_DIRS ${PCL_LIBRARY_DIRS})
  list(APPEND COVIS_LIBRARIES ${PCL_LIBRARIES})
  list(APPEND COVIS_DEFINITIONS ${PCL_DEFINITIONS})
else(PCL_FOUND)
  message(FATAL_ERROR ">> PCL not found!")
endif(PCL_FOUND)
mark_as_advanced(PCL_2D_INCLUDE_DIR)
mark_as_advanced(PCL_APPS_INCLUDE_DIR)
mark_as_advanced(PCL_COMMON_INCLUDE_DIR)
mark_as_advanced(PCL_DIR)
mark_as_advanced(PCL_FEATURES_INCLUDE_DIR)
mark_as_advanced(PCL_FILTERS_INCLUDE_DIR)
mark_as_advanced(PCL_GEOMETRY_INCLUDE_DIR)
mark_as_advanced(PCL_IO_INCLUDE_DIR)
mark_as_advanced(PCL_KDTREE_INCLUDE_DIR)
mark_as_advanced(PCL_KEYPOINTS_INCLUDE_DIR)
mark_as_advanced(PCL_ML_INCLUDE_DIR)
mark_as_advanced(PCL_OCTREE_INCLUDE_DIR)
mark_as_advanced(PCL_OUTOFCORE_INCLUDE_DIR)
mark_as_advanced(PCL_PEOPLE_INCLUDE_DIR)
mark_as_advanced(PCL_RECOGNITION_INCLUDE_DIR)
mark_as_advanced(PCL_REGISTRATION_INCLUDE_DIR)
mark_as_advanced(PCL_SAMPLE_CONSENSUS_INCLUDE_DIR)
mark_as_advanced(PCL_SEARCH_INCLUDE_DIR)
mark_as_advanced(PCL_SEGMENTATION_INCLUDE_DIR)
mark_as_advanced(PCL_STEREO_INCLUDE_DIR)
mark_as_advanced(PCL_SURFACE_INCLUDE_DIR)
mark_as_advanced(PCL_TRACKING_INCLUDE_DIR)
mark_as_advanced(PCL_VISUALIZATION_INCLUDE_DIR)
mark_as_advanced(FLANN_INCLUDE_DIRS)
mark_as_advanced(FLANN_LIBRARY)
mark_as_advanced(FLANN_LIBRARY_DEBUG)
mark_as_advanced(LIBUSB_1_INCLUDE_DIR)
mark_as_advanced(LIBUSB_1_LIBRARY)
mark_as_advanced(OPENNI_INCLUDE_DIRS)
mark_as_advanced(OPENNI_LIBRARY)
mark_as_advanced(OPENNI2_LIBRARY)
mark_as_advanced(QHULL_INCLUDE_DIRS)
mark_as_advanced(QHULL_LIBRARY)
mark_as_advanced(QHULL_LIBRARY_DEBUG)

##
# FLANN version check
##
if(PC_FLANN_VERSION VERSION_LESS 1.8)
  set(FLANN_1_8 OFF)
else(PC_FLANN_VERSION VERSION_LESS 1.8)
  set(FLANN_1_8 ON)
endif(PC_FLANN_VERSION VERSION_LESS 1.8)

##
# Find Astyle
##
find_program(ASTYLE_EXECUTABLE astyle)
if(ASTYLE_EXECUTABLE)
  set(ASTYLE_FOUND TRUE CACHE STRING "Astyle find status" FORCE)
else(ASTYLE_EXECUTABLE)
  set(ASTYLE_FOUND FALSE CACHE STRING "Astyle find status" FORCE)
endif(ASTYLE_EXECUTABLE)
mark_as_advanced(ASTYLE_EXECUTABLE ASTYLE_FOUND)

##
# Find Doxygen
##
find_package(Doxygen QUIET)

##
# Find VTK
##
if(NOT VTK_FOUND)
  # PCL may have already found VTK
  find_package(VTK QUIET)
endif(NOT VTK_FOUND)
if(VTK_FOUND)
  message(STATUS ">> VTK ${VTK_VERSION} found")
  set(VTK_FOUND TRUE) # VTK uses 1/0 instead of TRUE/FALSE
  list(APPEND COVIS_INCLUDE_DIRS ${VTK_INCLUDE_DIRS})
  list(APPEND COVIS_LIBRARY_DIRS ${VTK_LIBRARY_DIRS})
  list(APPEND COVIS_LIBRARIES ${VTK_LIBRARIES})
else(VTK_FOUND)
  set(VTK_FOUND FALSE)
endif(VTK_FOUND)

##
# Find CUDA
##
find_package(CUDA QUIET)
if(CUDA_FOUND)
  message(STATUS ">> CUDA ${CUDA_VERSION} found")
endif(CUDA_FOUND)
mark_as_advanced(CUDA_BUILD_CUBIN)
mark_as_advanced(CUDA_BUILD_CUBIN)
mark_as_advanced(CUDA_BUILD_EMULATION)
mark_as_advanced(CUDA_HOST_COMPILER)
mark_as_advanced(CUDA_SDK_ROOT_DIR)
mark_as_advanced(CUDA_SEPARABLE_COMPILATION)
mark_as_advanced(CUDA_TOOLKIT_ROOT_DIR)
mark_as_advanced(CUDA_VERBOSE_BUILD)

##
# Find OpenMP
# TODO: Make silent
##
find_package(OpenMP QUIET)
if(OPENMP_FOUND)
  message(STATUS ">> OpenMP found")
  list(APPEND COVIS_DEFINITIONS ${OpenMP_CXX_FLAGS})
  # TODO: This should not be necessary!
  list(APPEND COVIS_LIBRARIES gomp)
endif(OPENMP_FOUND)

##
# Find OpenNI2
##
find_package(OpenNI2)
if(OPENNI2_FOUND)
  message(STATUS ">> OpenNI2 found")
  list(APPEND COVIS_INCLUDE_DIRS ${OPENNI2_INCLUDE_DIRS})
  list(APPEND COVIS_LIBRARIES ${OPENNI2_LIBRARIES})
endif(OPENNI2_FOUND)

##
# Find NURBS++
##
SET(NURBSPP_FIND_QUIETLY TRUE)
find_package(NurbsPP)
if(NURBSPP_FOUND)
  message(STATUS ">> NURBS++ found")
  list(APPEND COVIS_INCLUDE_DIRS ${NURBSPP_INCLUDE_DIRS})
  list(APPEND COVIS_LIBRARIES ${NURBSPP_LIBRARIES})
endif(NURBSPP_FOUND)
