# Helper script for setting up CoViS build system
#
# The following variables are set:
#   COVIS_COMIPLER
#   COVIS_CXX11
#   COVIS_BUILD_STATIC
#   COVIS_LOG_LEVEL
#   COVIS_INCLUDE_DIRS
#   COVIS_LIBRARY_DIRS
#   COVIS_LIBRARIES
#   COVIS_DEFINITIONS
#   COVIS_DEFINITIONS_EXTRA
#   INSTALL_DIR_INC
#   INSTALL_DIR_LIB
#   INSTALL_DIR_BIN
#   INSTALL_DIR_DOC
#   INSTALL_DIR_CMAKE
#

##
# Check for C++ compiler
##
if(NOT CMAKE_CXX_COMPILER)
  message(FATAL_ERROR ">> C++ compiler missing!")
endif(NOT CMAKE_CXX_COMPILER)

##
# Check for valid compiler type (GCC, CLANG)
# Stores upper-case compiler string in COVIS_COMPILER
# Taken from here: http://stackoverflow.com/questions/10046114
##
string(TOUPPER ${CMAKE_CXX_COMPILER_ID} COVIS_COMPILER)
if(COVIS_COMPILER STREQUAL "GNU") # GNU, set to GCC
  set(COVIS_COMPILER "GCC")
elseif(COVIS_COMPILER STREQUAL "CLANG") # Clang
  # Do nothing for now
elseif(COVIS_COMPILER STREQUAL "INTEL") # ICC
  message(FATAL_ERROR "Intel compiler not supported!")
elseif(COVIS_COMPILER STREQUAL "MSVC") # MSVC
  message(FATAL_ERROR "Visual Studio  compiler not supported!")
else(COVIS_COMPILER STREQUAL "GNU")
  message(FATAL_ERROR "Unknown compiler ID: ${CMAKE_CXX_COMPILER_ID}!")
endif(COVIS_COMPILER STREQUAL "GNU")

##
# In case of GCC, get version (for use below)
##
if(COVIS_COMPILER STREQUAL "GCC")
    execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion
                    OUTPUT_VARIABLE GCC_VERSION)
    string(REGEX MATCHALL "[0-9.]+" GCC_VERSION ${GCC_VERSION})
endif(COVIS_COMPILER STREQUAL "GCC")

##
# C++11 option
##
#option(COVIS_CXX11 "Set to ON to enable C++11 compilation" OFF)
#if(COVIS_CXX11)
  #if(COVIS_COMPILER STREQUAL "GCC")
    # GCC
    #if(GCC_VERSION VERSION_LESS 4.3)
      # GCC version too old
      #message(FATAL_ERROR "C++11 not supported for GCC < 4.3!")
    #elseif(GCC_VERSION VERSION_LESS 4.7)
      # GCC version < 4.7 uses c++0x
      #add_definitions("-std=c++0x")
    #else(GCC_VERSION VERSION_LESS 4.3)
      # GCC version >= 4.7 uses c++11
      #add_definitions("-std=c++11")
    #endif(GCC_VERSION VERSION_LESS 4.3)
  #elseif(COVIS_COMPILER STREQUAL "CLANG")
    # Clang
    #add_definitions("-std=c++11")
  #else(COVIS_COMPILER STREQUAL "GCC")
    # Unsupported
    #message(FATAL_ERROR "C++11 only supported for GCC/Clang!")
  #endif(COVIS_COMPILER STREQUAL "GCC")
#endif(COVIS_CXX11)

##
# Build type option, default to RelWithDebInfo
##
if(CMAKE_BUILD_TYPE)
  if(NOT (CMAKE_BUILD_TYPE STREQUAL "Release" OR
          CMAKE_BUILD_TYPE STREQUAL "Debug" OR
          CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo" OR
          CMAKE_BUILD_TYPE STREQUAL "MinSizeRel"))
    message(FATAL_ERROR "Invalid build type: \"${CMAKE_BUILD_TYPE}\"! Valid build types are: Release, Debug, RelWithDebInfo, MinSizeRel")
  endif()
else(CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Build type: Release, Debug, RelWithDebInfo, MinSizeRel" FORCE)
endif(CMAKE_BUILD_TYPE)

##
# Library type option
##
option(COVIS_BUILD_STATIC "Set to ON to build static libraries" OFF)

##
# Log level option
##
if(COVIS_LOG_LEVEL)
  if(NOT (COVIS_LOG_LEVEL STREQUAL "ERROR" OR
          COVIS_LOG_LEVEL STREQUAL "WARN" OR
          COVIS_LOG_LEVEL STREQUAL "INFO" OR
          COVIS_LOG_LEVEL STREQUAL "ALL")
    )
    message(FATAL_ERROR "Invalid log level: \"${COVIS_LOG_LEVEL}\"! Valid log levels are: ERROR, WARN, INFO, ALL")
  endif()
else(COVIS_LOG_LEVEL)
  set(COVIS_LOG_LEVEL "WARN" CACHE STRING "CoViS log level: ERROR, WARN, INFO, ALL" FORCE)
endif(COVIS_LOG_LEVEL)

##
# The main tuple of state variables necessary for using CoViS from outside
##
set(COVIS_INCLUDE_DIRS "" CACHE STRING "" FORCE)
set(COVIS_LIBRARY_DIRS "" CACHE STRING "" FORCE)
set(COVIS_LIBRARIES "" CACHE STRING "" FORCE)
set(COVIS_DEFINITIONS "" CACHE STRING "" FORCE)

##
# Standard compiler flags
##
if(COVIS_COMPILER STREQUAL "GCC" OR COVIS_COMPILER STREQUAL "CLANG")
  # TODO: I don't want the -Wno-* flags, but goddamn PCL+VTK are causing them!
  list(APPEND COVIS_DEFINITIONS -Wall -Wextra -Wno-invalid-offsetof -Wno-deprecated)
  if(CMAKE_BUILD_TYPE STREQUAL "Release" OR
     CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo" OR
     CMAKE_BUILD_TYPE STREQUAL "MinSizeRel")
    list(APPEND COVIS_DEFINITIONS -march=native)
  endif()
endif(COVIS_COMPILER STREQUAL "GCC" OR COVIS_COMPILER STREQUAL "CLANG")

##
# Hard-coded compiler flags
##
if(COVIS_COMPILER STREQUAL "GCC")
  # Remove nasty warning coming from PCL's point registration
  list(APPEND COVIS_DEFINITIONS -Wno-invalid-offsetof)
  # Pretty clang-like color diagnostics for GCC >= 4.9
  if(GCC_VERSION VERSION_GREATER 4.8)
    list(APPEND COVIS_DEFINITIONS -fdiagnostics-color=auto)
  endif(GCC_VERSION VERSION_GREATER 4.8)
endif(COVIS_COMPILER STREQUAL "GCC")

##
# Extra compiler flags only for this build
##
if(NOT COVIS_DEFINITIONS_EXTRA)
  set(COVIS_DEFINITIONS_EXTRA "" CACHE STRING "Extra CoViS definitions")
endif(NOT COVIS_DEFINITIONS_EXTRA)

##
# Setup installation variables - CMake installation prefixes these with
# CMAKE_INSTALL_PREFIX
##
set(INSTALL_DIR_INC include)
set(INSTALL_DIR_LIB lib)
set(INSTALL_DIR_BIN bin)
set(INSTALL_DIR_DOC share/doc)
set(INSTALL_DIR_CMAKE ${CMAKE_ROOT}/Modules)

##
# Make sure that libraries/binaries are built into convenient locations
##
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${INSTALL_DIR_BIN})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${INSTALL_DIR_LIB})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${INSTALL_DIR_LIB})
