# Find OpenNI 2
#
# The following variables are set:
#   OPENNI2_FOUND
#   OPENNI2_INCLUDE_DIRS
#   OPENNI2_LIBRARIES


find_path(OPENNI2_INCLUDE_DIRS OpenNI.h
          PATHS /usr/include/openni2
                /usr/local/include/openni2
                /usr/local/include/ni2
)

find_library(OPENNI2_LIBRARIES OpenNI2
             PATHS /usr/lib
                   /usr/local/lib
                   /usr/local/lib/ni2
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OPENNI2 DEFAULT_MSG OPENNI2_INCLUDE_DIRS OPENNI2_LIBRARIES)
mark_as_advanced(OPENNI2_INCLUDE_DIRS OPENNI2_LIBRARIES)
