# Helper script for setting up CoViS test
#


##
# Setup testing
##
#include(CMakeDependentOption)
#cmake_dependent_option(COVIS_BUILD_TESTS "Set to OFF to not build tests" 
                       #ON "Boost_TEST_EXEC_MONITOR_FOUND;Boost_UNIT_TEST_FRAMEWORK_FOUND" OFF)
set(COVIS_BUILD_TESTS ON)

if(COVIS_BUILD_TESTS)
  #include(CTest)
  #enable_testing()
  add_subdirectory(test)
endif(COVIS_BUILD_TESTS)
#CONFIGURE_FILE("${RW_ROOT}/CTestConfig.cmake.in" "${RW_ROOT}/CTestConfig.cmake")

##
# Setup coverage
##
if(ENABLE_COVERAGE)
  set(CMAKE_CXX_FLAGS "-g -O0 -Wall -W -Wshadow -Wunused-variable -Wunused-parameter -Wunused-function -Wunused -Wno-system-headers -Wno-deprecated -Woverloaded-virtual -Wwrite-strings -fPIC -fprofile-arcs -ftest-coverage")
  set(CMAKE_C_FLAGS "-g -O0 -Wall -W -fprofile-arcs -ftest-coverage")
  set(CMAKE_EXE_LINKER_FLAGS "-fprofile-arcs -ftest-coverage")
else(ENABLE_COVERAGE)
# TODO. What do do here?
endif(ENABLE_COVERAGE)