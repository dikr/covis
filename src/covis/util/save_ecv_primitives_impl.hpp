// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_UTIL_SAVE_ECV_PRIMITIVES_IMPL_HPP
#define COVIS_UTIL_SAVE_ECV_PRIMITIVES_IMPL_HPP

#include "save_ecv_primitives.h"
#include <fstream>

namespace covis {
    namespace util {

        template <class Prim2D, class Prim3D>
        void SaveECVPrimitives<Prim2D, Prim3D>::write3DPrimitivesToXMLFile(std::vector<Prim3D> &primitives, const char* fileName) {

                std::ofstream xmlOut;
                xmlOut.open(fileName, std::ofstream::out);
                xmlOut << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
                xmlOut << "<Texlet3D version=\"1.0\">\n";

                for (typename std::vector<Prim3D>::iterator it = primitives.begin() ; it != primitives.end(); ++it) {

                    xmlOut << "\t<Primitive3D type=\"t\" confidence= \"1.000000\" length=\"5.000000\">\n";
                    xmlOut << "\t\t<UID>-1</UID>\n";
                    xmlOut << "\t\t<EID>0</EID>\n";
                    xmlOut << "\t\t<FID>-1</FID>\n";
                    xmlOut << "\t\t<status>0</status>\n";

                    xmlOut << "\t\t<Location>\n";
                    xmlOut << "\t\t\t<Cartesian3D x=\"" << it->x*1000 <<"\" y=\""<< it->y*(1000) <<"\" z=\""<<it->z*(1000) << "\"/>\n";
                    xmlOut << "\t\t\t<Cartesian3DCovariance>\n";
                    xmlOut << "\t\t\t</Cartesian3DCovariance>\n";
                    xmlOut << "\t\t</Location>\n";

                    xmlOut << "\t\t<Orientation>\n";
                    xmlOut << "\t\t\t<DirGammaOrientation>\n";
                    xmlOut << "\t\t\t\t<GammaVector>\n";
                    xmlOut << "\t\t\t\t\t<Cartesian3D x=\"" << it->normal_x<<"\" y=\""<<it->normal_y <<"\" z=\""<<it->normal_z<< "\"/>\n";
                    xmlOut << "\t\t\t\t</GammaVector>\n";
                    xmlOut << "\t\t\t</DirGammaOrientation>\n";
                    xmlOut << "\t\t\t<Orientation3DCovariance></Orientation3DCovariance>\n";
                    xmlOut << "\t\t</Orientation>\n";

                    Prim2D* t = static_cast<Prim2D*>(it->getPrimitive2D());

                    xmlOut << "\t\t<IntrinsicDimensionality>\n";
                    xmlOut << "\t\t\t<Barycentric c0=\"" << t->getIds().id0<<"\" c1=\""<< t->getIds().id1 <<"\" c2=\""<< t->getIds().id2 <<"\"/>\n";
                    xmlOut << "\t\t</IntrinsicDimensionality>\n";

                    xmlOut << "\t\t<Color>\n";
                    xmlOut << "\t\t\t<RGB r=\"" << (double)(it->r)/255 << "\" g=\"" << (double)(it->g)/255 << "\" b=\""<< (double)(it->b)/255 << "\"/>\n";
                    xmlOut << "\t\t\t<Conf>1.000000</Conf>\n";
                    xmlOut << "\t\t\t<Histogram/>\n";
                    xmlOut << "\t\t</Color>\n";

                    xmlOut << "\t\t<Source2D>62</Source2D>\n";
                    xmlOut << "\t\t<BoundaryTypeVector size=\"0\">\n";
                    xmlOut << "\t\t\t<BoundaryValues></BoundaryValues>\n";
                    xmlOut << "\t\t</BoundaryTypeVector>\n";
                    xmlOut << "\t</Primitive3D>\n";

                }
                xmlOut << "</Texlet3D>";
                xmlOut.close();
                std::cout << "wrote to the xml file" << std::endl;
        }
    }
}

#endif /* COVIS_UTIL_SAVE_ECV_PRIMITIVES_IMPL_HPP */
