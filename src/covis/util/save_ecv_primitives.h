#ifndef COVIS_UTILS_SAVE_ECV_PRIMITIVES_H
#define COVIS_UTILS_SAVE_ECV_PRIMITIVES_H

#include <vector>
#include <iostream>
#include <covis/core/texlet_3d.h>
#include <covis/core/texlet_2d.h>
#include <covis/core/line_segment_3d.h>
#include <covis/core/line_segment_2d.h>


namespace covis {
    namespace util {
        template <class Prim2D, class Prim3D>
        class SaveECVPrimitives {
            public:
                SaveECVPrimitives() {}
                virtual ~SaveECVPrimitives() {}

                void write3DPrimitivesToXMLFile(std::vector<Prim3D> &primitives, const char* fileName);
        };

    }
}

#include "save_ecv_primitives_impl.hpp"
#endif
