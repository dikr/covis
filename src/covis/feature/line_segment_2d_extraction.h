// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_LINE_SEGMENT_2D_EXTRACTION_H
#define COVIS_FEATURE_LINE_SEGMENT_2D_EXTRACTION_H

#include "primitive_2d_extraction.h"

#include "../core/line_segment_2d.h"
#include "../filter/intrinsic_dimension.h"

namespace covis {
    namespace feature {
        /**
         * @class LineSegment2DExtraction
         * @ingroup feature
         * @brief LineSegment2DExtraction extraction class
         *
         * @author
         */
        class LineSegment2DExtraction : public Primitive2DExtraction {
            public:
                /// Empty constructor
                LineSegment2DExtraction() : _ids(0, 0.6, 0.3) {}
                /// Empty destructor
                virtual ~LineSegment2DExtraction() {};

                /**
                 * Parameter constructor
                 * @param ids doubles representing intrinsic dimensions (i0D, i1D and i2D)
                 */
                LineSegment2DExtraction(const core::Ids& ids) : _ids(ids.id0, ids.id1, ids.id2) { }

                /**
                 * Extract LineSegment2D
                 * @param image source image
                 * @return std::vector of 2D line segments
                 */
                std::vector<core::LineSegment2D> extract2DLineSegments(const cv::Mat &image){

                    filter::IntrinsicDimension id;
                    cv::Mat idResult = id.filter(image);
                    std::vector<core::LineSegment2D> ls = extract2DLineSegments(image, idResult);
                    return ls;
                }

                /**
                 * Extract LineSegment2D
                 * @param image source image
                 * @param idResult 3-channel double precision image representing intrinsic dimensions (i0D, i1D and i2D)
                 * @return std::vector of 2D line segments
                 */
                std::vector<core::LineSegment2D> extract2DLineSegments(const cv::Mat &image, const cv::Mat &idResult){

                    std::vector<core::LineSegment2D> ls;
                    ls = this->ExtractECVPrimitives(image, idResult, this->_ids).second;
                    return ls;
                }

                /**
                 * Set intrinsic dimensions
                 * @param ids intrinsic dimensions (i0D, i1D and i2D)
                 */
                inline void setIds(core::Ids ids) {
                    this->_ids = ids;
                }
                /**
                 * Get intrinsic dimensions
                 * @return ids intrinsic dimensions (i0D, i1D and i2D)
                 */
                inline core::Ids getIds() {
                    return this->_ids;
                }

            private:
                /// intrinsic dimensions
                core::Ids _ids;
        };
    }
}

#endif /* COVIS_FEATURE_LINE_SEGMENT_2D_EXTRACTION_H */
