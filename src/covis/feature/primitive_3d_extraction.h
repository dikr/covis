// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef PRIMITIVE_3D_EXTRACTION_H
#define PRIMITIVE_3D_EXTRACTION_H

#include "../core/texlet_2d.h"
#include "../core/texlet_3d.h"
#include "../core/line_segment_2d.h"
#include "../core/line_segment_3d.h"

//PCL
#define PCL_NO_PRECOMPILE
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

namespace covis {
    namespace feature {

        template<typename PointT, class Prim2D, typename Prim3D>
        class Primitive3DExtraction : public FeatureBase {
            public:
                /// Empty constructor
                Primitive3DExtraction() {}
                /// Empty destructor
                virtual ~Primitive3DExtraction() {}

                std::vector<Prim3D> extractECVPrimitives3D(std::vector<Prim2D> &primitives2d,
                        const typename pcl::PointCloud<PointT>::Ptr &cloud);

                pcl::PointCloud<Prim3D> extractECVPrimitives3DtoPointCloud
                (std::vector<Prim2D> &primitives2d, const typename pcl::PointCloud<PointT>::Ptr &cloud);


            private:
                pcl::PointCloud<pcl::Normal>::Ptr computeNormals(const typename pcl::PointCloud<PointT>::Ptr &cloud,
                        std::vector<int> &indices);

        };
    }
}

#include "primitive_3d_extraction_impl.hpp"

#endif /* PRIMITIVE_3D_EXTRACTION_H */
