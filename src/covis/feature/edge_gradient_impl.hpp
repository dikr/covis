// Copyright (c) 2014, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_EDGE_GRADIENT_IMPL_HPP
#define COVIS_FEATURE_EDGE_GRADIENT_IMPL_HPP

#include <pcl/point_types.h>
#include <opencv2/opencv.hpp>
#include <covislib/pcl_edge_detection/kernel.h>
#include <covis/calib/convert_input.h>

namespace covis {
    namespace feature {
        template <typename PointT, typename PointNT, typename PointLT>
        void EdgeGradient<PointT, PointNT, PointLT>::myCompute(
                pcl::PointCloud<PointLT>& labels,
                std::vector<pcl::PointIndices>& label_indices,
                pcl::PointCloud<core::CategorisedEdge>::Ptr &out){

            this->compute(labels, label_indices);

            cv::Mat_<cv::Vec3b> rgb = cv::Mat::zeros(this->input_->height, this->input_->width, CV_8UC3);
            for (size_t i = 0; i < rgb.rows; i++){
                for (size_t j = 0; j < rgb.cols; j++){
                    cv::Vec3b& dat = rgb(i, j);
                    PointT point = (*this->input_).at(j, i);
                    dat.val[0] = point.b;
                    dat.val[1] = point.g;
                    dat.val[2] = point.r;
                }
            }
//            cv::imshow("img",rgb);cv::waitKey();
            cv::Mat img_out, img_out2;
            cv::GaussianBlur(rgb, img_out, cv::Size(11,11), 0, 0); //31 31
            pcl::PointCloud<pcl::PointXYZI>::Ptr gray0 (new pcl::PointCloud<pcl::PointXYZI>);
            pcl::PointCloud<pcl::PointXYZI>::Ptr gray1 (new pcl::PointCloud<pcl::PointXYZI>);
            pcl::PointCloud<pcl::PointXYZI>::Ptr gray2 (new pcl::PointCloud<pcl::PointXYZI>);

            gray0->width = this->input_->width;
            gray0->height = this->input_->height;
            gray0->resize (this->input_->height*this->input_->width);

            gray1->width = this->input_->width;
            gray1->height = this->input_->height;
            gray1->resize (this->input_->height*this->input_->width);

            gray2->width = this->input_->width;
            gray2->height = this->input_->height;
            gray2->resize (this->input_->height*this->input_->width);

            for (size_t i = 0; i < rgb.rows; i++){
                for (size_t j = 0; j < rgb.cols; j++){
                    cv::Vec3b col = img_out.at<cv::Vec3b>(i, j);
                    (*gray0).at(j, i).intensity = col[2];
                    (*gray1).at(j, i).intensity = col[1];
                    (*gray2).at(j, i).intensity = col[0];
                }
            }
            //-------------------------------------------------------------------------------
            pcl::PointCloud<pcl::PointXYZIEdge> img_edge_rgb0, img_edge_rgb1, img_edge_rgb2;
            pcl::Edge<pcl::PointXYZI, pcl::PointXYZIEdge> edge0, edge1, edge2;
            edge0.setInputCloud (gray0);
            edge0.setHysteresisThresholdLow (this->th_rgb_canny_low_);
            edge0.setHysteresisThresholdHigh (this->th_rgb_canny_high_);
            edge0.detectEdgeSobel(img_edge_rgb0);

            edge1.setInputCloud (gray1);
            edge1.setHysteresisThresholdLow (this->th_rgb_canny_low_);
            edge1.setHysteresisThresholdHigh (this->th_rgb_canny_high_);
            edge1.detectEdgeSobel(img_edge_rgb1);

            edge2.setInputCloud (gray2);
            edge2.setHysteresisThresholdLow (this->th_rgb_canny_low_);
            edge2.setHysteresisThresholdHigh (this->th_rgb_canny_high_);
            edge2.detectEdgeSobel(img_edge_rgb2);

            for (uint32_t row=0; row<labels.height; row++){
                for (uint32_t col=0; col<labels.width; col++){
                    float mag0 = img_edge_rgb0(col, row).magnitude_x * img_edge_rgb0(col, row).magnitude_x + img_edge_rgb0(col, row).magnitude_y *img_edge_rgb0(col, row).magnitude_y;
                    float mag1 = img_edge_rgb1(col, row).magnitude_x * img_edge_rgb1(col, row).magnitude_x + img_edge_rgb1(col, row).magnitude_y *img_edge_rgb1(col, row).magnitude_y;
                    float mag2 = img_edge_rgb2(col, row).magnitude_x * img_edge_rgb2(col, row).magnitude_x + img_edge_rgb2(col, row).magnitude_y *img_edge_rgb2(col, row).magnitude_y;

                    if (mag0 > mag1 && mag0 > mag2){
                        (*out)(col, row).dx = img_edge_rgb0 (col, row).magnitude_x;
                        (*out)(col, row).dy = img_edge_rgb0 (col, row).magnitude_y;
                    } else if (mag1 > mag2 && mag1 > mag0){
                        (*out)(col, row).dx = img_edge_rgb1 (col, row).magnitude_x;
                        (*out)(col, row).dy = img_edge_rgb1 (col, row).magnitude_y;
                    } else if (mag2 > mag1 && mag2 > mag0){
                        (*out)(col, row).dx = img_edge_rgb2 (col, row).magnitude_x;
                        (*out)(col, row).dy = img_edge_rgb2 (col, row).magnitude_y;
                    }
                    (*out)(col, row).x_2D = col;
                    (*out)(col, row).y_2D = row;
                }
            }
        }
    }
}

#endif /* COVIS_FEATURE_EDGE_GRADIENT_IMPL_HPP */

