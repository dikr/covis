// Copyright (c) 2014, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_EDGE_GRADIENT_H
#define COVIS_FEATURE_EDGE_GRADIENT_H

#include <covislib/pcl_edge_detection/organized_edge_detection.h>
#include "../core/categorised_edge.h"

//PCL
#include <pcl/features/integral_image_normal.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/pcl_base.h>
#include <pcl/PointIndices.h>


namespace covis {
    namespace feature {
        /**
         * @class EdgeGradient
         * @ingroup feature
         * @brief Computes and extends pcl organized edge extraction by edge gradient and 2D positions
         *
         * @tparam PointT input point type, must contain XYZ data
         * @tparam PointNT input point type, must contain normals
         * @tparam PointLT output labels for different edge types
         * @author Lilita Kiforenko
         */
        template <typename PointT, typename PointNT, typename PointLT>

        class EdgeGradient : public pcl::OrganizedEdgeFromRGBNormals<PointT, PointNT, PointLT>
        {
            public:
                /**
                 * Compute pcl organized edges
                 * @param labels ouput edge types
                 * @param label_indices output idx for different edge types
                 * @param out output categorised edge point cloud
                 */
                void myCompute(pcl::PointCloud<PointLT>& labels, std::vector<pcl::PointIndices>& label_indices,
                        pcl::PointCloud<core::CategorisedEdge>::Ptr &out);
        };
    }
}

#include "edge_gradient_impl.hpp"

#endif /* COVIS_FEATURE_EDGE_GRADIENT_H */
