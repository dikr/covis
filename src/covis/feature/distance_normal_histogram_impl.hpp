// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_DISTANCE_NORMAL_HISTOGRAM_IMPL_HPP
#define COVIS_FEATURE_DISTANCE_NORMAL_HISTOGRAM_IMPL_HPP

// Own
#include "../core/stat.h"

// PCL
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>

// TODO
#include <opencv2/highgui/highgui.hpp>

namespace covis {
    namespace feature {
        template<typename PointNT, int NDist, int NNorm>
        typename pcl::PointCloud<typename DistanceNormalHistogram<PointNT, NDist, NNorm>::Histogram>::Ptr
        DistanceNormalHistogram<PointNT, NDist, NNorm>::compute(typename pcl::PointCloud<PointNT>::ConstPtr cloud) {
            // Check for valid inputs
            COVIS_ASSERT(cloud && !cloud->empty());
            COVIS_ASSERT_MSG(_radius > 0.0f, "Search radius not set!");
            
            // Create search
            typename pcl::search::Search<PointNT>::Ptr s;
            if(cloud->isOrganized() || (_surface && _surface->isOrganized()))
                s.reset(new pcl::search::OrganizedNeighbor<PointNT>);
            else
                s.reset(new pcl::search::KdTree<PointNT>);
            
            // Index input cloud or external surface
            if(_surface)
                s->setInputCloud(_surface);
            else
                s->setInputCloud(cloud);
            
            // Allocate results
            typename pcl::PointCloud<Histogram>::Ptr result(
                    new typename pcl::PointCloud<Histogram>(cloud->width, cloud->height));
            
            /*
             * Main loop over all input points
             */
            for(size_t i = 0; i < cloud->size(); ++i) {
                // Take current source point
                const PointNT& pi = cloud->points[i];
                
                // Take current output histogram
                Histogram& hi = (*result)[i];
                
                // Skip if source is non-finite in XYZ or normal
                if(!pcl::isFinite(pi) ||
                        !pcl_isfinite(pi.normal_x) || !pcl_isfinite(pi.normal_y) || !pcl_isfinite(pi.normal_z)) {
                    for(int j = 0; j < NDist * NNorm; ++j)
                        hi.histogram[j] = 0.0f;
                    continue;
                }
                
                // Find neighbors
                std::vector<int> idx;
                std::vector<float> distsq;
                s->radiusSearch(pi, double(_radius), idx, distsq);
                
                // Allocate relative distances/normal orientations
                std::vector<float> distances;
                distances.reserve(idx.size());
                std::vector<float> dots;
                dots.reserve(idx.size());
                    
                // Loop over neighbors and compute relative distances and dot products
                for(size_t j = 0; j < idx.size(); ++j) {
                    // Take neighbor
                    const PointNT& pj = (_surface ? _surface->points[idx[j]] : cloud->points[idx[j]]);
                    
                    // Skip neighbor if it is the source point
                    if(&pj == &pi)
                        continue;
                    
                    // Skip neighbor if it does not have a valid normal
                    if(!pcl_isfinite(pj.normal_x) || !pcl_isfinite(pj.normal_y) || !pcl_isfinite(pj.normal_z))
                        continue;
                    
                    // Take dot product
                    float dotij = pi.normal_x * pj.normal_x + pi.normal_y * pj.normal_y + pi.normal_z * pj.normal_z;
                    
                    // If negative, either skip or negate
                    if(dotij < 0.0f) {
                        if(_skipNegatives)
                            continue;
                        else
                            dotij = -dotij;
                    }
                    
                    // Store
                    dots.push_back(dotij);
                    distances.push_back(sqrtf(distsq[j]));
//                    distances.push_back(distsq[j]);
                } // End loop over neighbors (j)
                
                // Now compute histogram (will be set to zero if dots is empty)
                core::rhist2<float>(distances, dots, NDist, NNorm, hi.histogram, 0.0f, _radius, 0.0f, 1.0f);
//                core::rhist2<float>(distances, dots, NDist, NNorm, hi.histogram, 0.0f, _radius * _radius, 0.0f, 1.0f);
            } // End main loop over all input points (i)
            
            return result;
        }
    }
}

#endif
