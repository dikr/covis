// Copyright (c) 2014, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_CATEGORIZED_EDGE_DISTANCE_HISTOGRAM_IMPL_HPP
#define COVIS_FEATURE_CATEGORIZED_EDGE_DISTANCE_HISTOGRAM_IMPL_HPP

// Own
#include "../core/stat.h"

// PCL
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>


namespace covis {
    namespace feature {
        template<typename PointT, int N>
        typename pcl::PointCloud<pcl::Histogram<N> >::Ptr CategorizedEdgeDistanceHistogram<PointT, N>::compute(
                typename pcl::PointCloud<PointT>::ConstPtr cloud) {
            // Check for valid inputs
            COVIS_STATIC_ASSERT_XYZ(core::CategorisedEdge);
            COVIS_ASSERT(!cloud->empty());
            COVIS_ASSERT_MSG(_radius > 0.0f, "Search radius not set!");

            // Create search
            typename pcl::search::Search<PointT>::Ptr s;
            s.reset(new pcl::search::KdTree<PointT>);
            s->setInputCloud(cloud);

            typename pcl::PointCloud<pcl::Histogram<N> >::Ptr distanceHistogram(new typename pcl::PointCloud<pcl::Histogram<N> >);
            distanceHistogram->points.resize(this->edge_amount);

            int index = 0;
            for(size_t i = 0; i < cloud->size(); ++i) {

                // check if point is an edge point
                if ((this->edge_cloud[i].idx_edge_types[0] != -1 || this->edge_cloud[i].idx_edge_types[1] != -1 ||
                        this->edge_cloud[i].idx_edge_types[2] != -1 || this->edge_cloud[i].idx_edge_types[3] != -1 )&&
                                (pcl_isfinite(this->edge_cloud[i].x) && pcl_isfinite(this->edge_cloud[i].y) && pcl_isfinite(this->edge_cloud[i].z))
                ) {
                    this->edge_cloud[i].idx_original = index;
                    pcl::Histogram<N>& hi = (*distanceHistogram)[index];
                    std::vector<int> idx;
                    std::vector<float> distsq;
                    s->radiusSearch((*cloud)[i], double(_radius), idx, distsq);

                    std::vector<float> occluding_distance, occluded_distance, curvature_distance, canny_distance, non_edge_distance;

                    for(size_t j = 0; j < idx.size(); ++j) {
                        if(size_t(idx[j]) != i){
                            int _idx = idx[j];
                            if (this->edge_cloud[_idx].idx_edge_types[0] != -1) {
                                occluding_distance.push_back(distsq[j]);
                            }
                            if (this->edge_cloud[_idx].idx_edge_types[1] != -1){
                                occluded_distance.push_back(distsq[j]);
                            }
                            if (this->edge_cloud[_idx].idx_edge_types[2] != -1) {
                                curvature_distance.push_back(distsq[j]);
                            }
                            if (this->edge_cloud[_idx].idx_edge_types[3] != -1 ){
                                canny_distance.push_back(distsq[j]);
                            }
                            if (this->edge_cloud[_idx].idx_edge_types[0] == -1 && this->edge_cloud[_idx].idx_edge_types[1] == -1 &&
                                    this->edge_cloud[_idx].idx_edge_types[2] == -1 && this->edge_cloud[_idx].idx_edge_types[3] == -1) {
                                non_edge_distance.push_back(distsq[j]);
                            }
                        }
                    }

                    std::memset(hi.histogram, 0, N*sizeof(float));
                    //-------------------------
                    core::rhist<float>(occluding_distance, N/5, hi.histogram, 0.0f,  _radius*_radius);
                    cv::Mat_<float> m1(1, N/5, hi.histogram);
                    cv::GaussianBlur(m1, m1, cv::Size(1,3), 0, 0);

                    core::rhist<float>(occluded_distance, N/5, hi.histogram + N/5, 0.0f, _radius*_radius);
                    cv::Mat_<float> m3(1, N/5, hi.histogram + N/5);
                    cv::GaussianBlur(m3, m3, cv::Size(1,3), 0, 0);

                    core::rhist<float>(curvature_distance, N/5, hi.histogram + N/5 + N/5, 0.0f, _radius*_radius);
                    cv::Mat_<float> m5(1, N/5, hi.histogram + N/5 + N/5);
                    cv::GaussianBlur(m5, m5, cv::Size(1,3), 0, 0);

                    core::rhist<float>(canny_distance, N/5, hi.histogram + N/5 + N/5 + N/5, 0.0f, _radius*_radius);
                    cv::Mat_<float> m7(1, N/5, hi.histogram + N/5 + N/5 + N/5);
                    cv::GaussianBlur(m7, m7, cv::Size(1,3), 0, 0);

                    core::rhist<float>(non_edge_distance, N/5, hi.histogram + N/5 + N/5 + N/5 + N/5, 0.0f, _radius*_radius);
                    cv::Mat_<float> m9(1, N/5, hi.histogram + N/5 + N/5 + N/5 + N/5);
                    cv::GaussianBlur(m9, m9, cv::Size(1,3), 0, 0);
                    //------------------------------------------------------
                    index++;
                }
            }
            return distanceHistogram;
        }
    }
}

#endif /* COVIS_FEATURE_CATEGORIZED_EDGE_DISTANCE_HISTOGRAM_IMPL_HPP */
