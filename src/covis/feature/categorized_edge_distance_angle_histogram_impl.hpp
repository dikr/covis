// Copyright (c) 2014, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_CATEGORIZED_EDGE_DISTANCE_ANGLE_HISTOGRAM_IMPL_HPP
#define COVIS_FEATURE_CATEGORIZED_EDGE_DISTANCE_ANGLE_HISTOGRAM_IMPL_HPP

// Own
#include "../core/stat.h"

// PCL
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>


namespace covis {
    namespace feature {
        template<typename PointT, int N>
        typename pcl::PointCloud<pcl::Histogram<N> >::Ptr CategorizedEdgeDistanceAngleHistogram<PointT, N>::compute(
                typename pcl::PointCloud<PointT>::ConstPtr cloud) {
            // Check for valid inputs
            COVIS_STATIC_ASSERT_XYZ(PointT);
            COVIS_ASSERT(!cloud->empty());
            COVIS_ASSERT_MSG(_radius > 0.0f, "Search radius not set!");
            // Create search
            typename pcl::search::Search<PointT>::Ptr s;
            s.reset(new pcl::search::KdTree<PointT>);
            s->setInputCloud(cloud);
            int edgeSize = this->edge_cloud.size();
            std::cout << "edge_cloud.size() : " << this->edge_cloud.size() << std::endl;
            typename pcl::PointCloud<pcl::Histogram<N> >::Ptr distanceAngleHistogram(new typename pcl::PointCloud<pcl::Histogram<N> >);
            distanceAngleHistogram->points.resize(edgeSize);

            //            {
            //                covis::core::ScopedTimer t("CategorizedEdgeDistanceAngleHistogram");
            for(size_t i = 0; i < edgeSize; ++i) {
                pcl::Histogram<N>& hi = (*distanceAngleHistogram)[i];
                std::vector<int> idx;
                std::vector<float> distsq;
                const PointT pi = (this->edge_cloud)[i];
                int index_in_original = pi.idx_original;
                s->radiusSearch((*cloud)[index_in_original], double(_radius), idx, distsq);

                std::vector<float> occluding_angle, occluded_angle, curvature_angle, canny_angle, non_edge_angle;
                std::vector<float> occluding_distance, occluded_distance, curvature_distance, canny_distance, non_edge_distance;
                std::vector<float> occluding_color, curvature_color, canny_color, non_edge_color;

                //get the current point
                Eigen::Vector3f _point, _normal, g1;
                _point[0] = pi.dx;
                _point[1] = pi.dy;
                _point[2] = 0;

                _normal[0] =  pi.normal_x;
                _normal[1] =  pi.normal_y;
                _normal[2] =  pi.normal_z;

                g1 = _normal.cross(_point.cross(_normal));
                g1.normalize();

                for(size_t j = 0; j < idx.size(); ++j) {
                    if(size_t(idx[j]) != index_in_original){
                        PointT neigh = (*cloud)[idx[j]];
                        Eigen::Vector3f _pointg2, _normalg2, g2;
                        _pointg2[0] = neigh.dx;
                        _pointg2[1] = neigh.dy;
                        _pointg2[2] = 0;

                        _normalg2[0] =  neigh.normal_x;
                        _normalg2[1] =  neigh.normal_y;
                        _normalg2[2] =  neigh.normal_z;

                        g2 = _normalg2.cross(_pointg2.cross(_normalg2));
                        g2.normalize();

                        float a = g1.dot(g2);
                        if (a < 0) a = -a;

                        int _idx = idx[j];

                        if ((*cloud)[_idx].idx_edge_types[0] != -1) {
                            occluding_angle.push_back(a);
                            occluding_distance.push_back(distsq[j]);
                            //                                occluding_color.push_back(_color);
                        }
                        if ((*cloud)[_idx].idx_edge_types[1] != -1) {
                            occluded_angle.push_back(a);
                            occluded_distance.push_back(distsq[j]);
                            //                                occluding_color.push_back(_color);
                        }
                        if ((*cloud)[_idx].idx_edge_types[2] != -1) {
                            curvature_angle.push_back(a);
                            curvature_distance.push_back(distsq[j]);
                            //                                curvature_color.push_back(_color);
                        }
                        if ((*cloud)[_idx].idx_edge_types[3] != -1 ){
                            canny_angle.push_back(a);
                            canny_distance.push_back(distsq[j]);
                            //                                canny_color.push_back(_color);
                        }
                        if ((*cloud)[_idx].idx_edge_types[0] == -1 && (*cloud)[_idx].idx_edge_types[1] == -1 &&
                                (*cloud)[_idx].idx_edge_types[2] == -1 && (*cloud)[_idx].idx_edge_types[3] == -1) {
                            non_edge_angle.push_back(a);
                            non_edge_distance.push_back(distsq[j]);
                        }
                    }
                }
                std::memset(hi.histogram, 0, N*sizeof(float));
                //OLD STYLE!
                int his_amount = 10;
                core::rhist<float>(occluding_distance, N/his_amount, hi.histogram, 0.0f,  _radius*_radius);
                cv::Mat_<float> m1(1, N/his_amount, hi.histogram);
                cv::GaussianBlur(m1, m1, cv::Size(1,3), 0, 0);
                int num = 1;
                core::rhist<float>(occluded_distance, N/his_amount, hi.histogram + N/his_amount * num, 0.0f, _radius*_radius);
                cv::Mat_<float> m3(1, N/his_amount, hi.histogram + N/his_amount * num);
                cv::GaussianBlur(m3, m3, cv::Size(1,3), 0, 0);
                num++;
                core::rhist<float>(curvature_distance, N/his_amount, hi.histogram + N/his_amount * num, 0.0f, _radius*_radius);
                cv::Mat_<float> m5(1, N/his_amount, hi.histogram + N/his_amount * num);
                cv::GaussianBlur(m5, m5, cv::Size(1,3), 0, 0);
                num++;
                core::rhist<float>(canny_distance, N/his_amount, hi.histogram + N/his_amount * num, 0.0f, _radius*_radius);
                cv::Mat_<float> m7(1, N/his_amount, hi.histogram + N/his_amount * num);
                cv::GaussianBlur(m7, m7, cv::Size(1,3), 0, 0);
                num++;
                core::rhist<float>(non_edge_distance, N/his_amount, hi.histogram + N/his_amount * num, 0.0f, _radius*_radius);
                cv::Mat_<float> m9(1, N/his_amount, hi.histogram + N/his_amount + N/his_amount * num);
                cv::GaussianBlur(m9, m9, cv::Size(1,3), 0, 0);
                num++;
                //-------------------------------------------------------------------------------------
                core::rhist<float>(occluding_angle, N/his_amount, hi.histogram + N/his_amount * num, 0.0f, 1.0f);
                cv::Mat_<float> m2(1, N/his_amount, hi.histogram + N/his_amount);
                cv::GaussianBlur(m2, m2, cv::Size(1,3), 0, 0);
                num++;
                core::rhist<float>(occluded_angle, N/his_amount, hi.histogram + N/his_amount * num, 0.0f, 1.0f);
                cv::Mat_<float> m4(1, N/his_amount, hi.histogram + N/his_amount + N/his_amount * num);
                cv::GaussianBlur(m4, m4, cv::Size(1,3), 0, 0);
                num++;
                core::rhist<float>(curvature_angle, N/his_amount, hi.histogram + N/his_amount * num, 0.0f, 1.0f);
                cv::Mat_<float> m6(1, N/his_amount, hi.histogram + N/his_amount + N/his_amount * num);
                cv::GaussianBlur(m6, m6, cv::Size(1,3), 0, 0);
                //------------------------
                num++;
                core::rhist<float>(canny_angle, N/his_amount, hi.histogram + N/his_amount * num, 0.0f, 1.0f);
                cv::Mat_<float> m8(1, N/his_amount, hi.histogram + N/his_amount * num);
                cv::GaussianBlur(m8, m8, cv::Size(1,3), 0, 0);
                //-----------------------
                num++;
                core::rhist<float>(non_edge_angle, N/his_amount, hi.histogram + N/his_amount * num, 0.0f, 1.0f);
                cv::Mat_<float> m10(1, N/his_amount, hi.histogram + N/his_amount *num);
                cv::GaussianBlur(m10, m10, cv::Size(1,3), 0, 0);
                //------------------------------------------------------
                for(size_t n = 0; n < N; n++){
                    float& point = hi.histogram[n];
                    point = point/his_amount;
                }
                //                }
            }
            return distanceAngleHistogram;
        }
    }
}

#endif /* COVIS_FEATURE_CATEGORIZED_EDGE_DISTANCE_ANGLE_HISTOGRAM_IMPL_HPP */

