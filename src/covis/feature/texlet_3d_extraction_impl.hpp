// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_TEXLET_3D_EXTRACTION_IMPL_HPP
#define COVIS_FEATURE_TEXLET_3D_EXTRACTION_IMPL_HPP

#include "texlet_2d_extraction.h"

namespace covis {
    namespace feature {

        template<typename PointT>
        std::vector<covis::core::Texlet3D> Texlet3DExtraction<PointT>::extract3dTexlets(
                const typename pcl::PointCloud<PointT>::Ptr &cloud,  core::Ids ids){
            std::vector<core::Texlet3D> out;

            //get the RGB image from point cloud
            cv::Mat rgbImage = calib::ConvertInput<PointT>().getRgbImageFromPointCloud(cloud);

            //extract texlets 2d
            Texlet2DExtraction t2d = Texlet2DExtraction(ids);
            std::vector<covis::core::Texlet2D> tex = t2d.extract2DTexlets(rgbImage);

            out = extract3dTexlets(tex, cloud);

            return out;
        }

        template<typename PointT>
        std::vector<covis::core::Texlet3D> Texlet3DExtraction<PointT>::extract3dTexlets(
                std::vector<covis::core::Texlet2D> &tex, const typename pcl::PointCloud<PointT>::Ptr &cloud){

            std::vector<core::Texlet3D> tex3d =
                    Primitive3DExtraction<PointT, core::Texlet2D, core::Texlet3D>().extractECVPrimitives3D(tex, cloud);
            return tex3d;
        }

        template<typename PointT>
        pcl::PointCloud<covis::core::Texlet3D> Texlet3DExtraction<PointT>::extract3dTexletsToPointCloud(
                std::vector<covis::core::Texlet2D> &tex, const typename pcl::PointCloud<PointT>::Ptr & cloud){

            pcl::PointCloud<covis::core::Texlet3D> out =
                    Primitive3DExtraction<PointT, core::Texlet2D, covis::core::Texlet3D>().extractECVPrimitives3DtoPointCloud(tex, cloud);
            return out;
        }

        template<typename PointT>
        pcl::PointCloud<covis::core::Texlet3D> Texlet3DExtraction<PointT>::extract3dTexletsToPointCloud(
                const typename pcl::PointCloud<PointT>::Ptr &cloud,  core::Ids ids){

            //get the RGB image from point cloud
            cv::Mat rgbImage = calib::ConvertInput<PointT>().getRgbImageFromPointCloud(cloud);

            //extract texlets 2d
            Texlet2DExtraction t2d = Texlet2DExtraction(ids);
            std::vector<covis::core::Texlet2D> tex = t2d.extract2DTexlets(rgbImage);

            pcl::PointCloud<covis::core::Texlet3D> out = extract3dTexletsToPointCloud(tex, cloud);

            return out;
        }
    }
}


#endif /* COVIS_FEATURE_TEXLET_3D_EXTRACTION_IMPL_HPP */
