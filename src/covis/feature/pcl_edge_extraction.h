// Copyright (c) 2014, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_PCL_EDGE_EXTRACTION_H
#define COVIS_FEATURE_PCL_EDGE_EXTRACTION_H

#define PCL_NO_PRECOMPILE
#include "covislib/pcl_edge_detection/organized_edge_detection.h"


// PCL
#include <pcl/features/integral_image_normal.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/pcl_base.h>
#include <pcl/PointIndices.h>


namespace covis {
    namespace feature {
        /**
         * @class PCLEdgeExtraction
         * @ingroup feature
         * @brief PCL organised edge extraction top class
         *
         * This class computes normals, extracts edges and creates a categorised edge point cloud, where each point is a type of
         * CategorisedEdge
         *
         * @tparam PointT input point type, must contain XYZ data
         * @author Lilita Kiforenko
         */
        template<typename PointT>
        class PCLEdgeExtraction : public pcl::PCLBase<PointT> {
            public:
                // Empty constructor, sets edge extraction parameters to default and extracts all edges
                PCLEdgeExtraction(): _th_dd(0.02f), _max_search(50), _cannyLow(40), _cannyHigh(100),
                _edgeType(1|2|4|8|16), _it_is_template(false){}


                PCLEdgeExtraction(float th_dd, int max_search, int cannyLow, int cannyHigh, int edgeType, bool itIsTemplate) :
                    _th_dd(th_dd), _max_search(max_search), _cannyLow(cannyLow), _cannyHigh(cannyHigh),
                    _edgeType(edgeType), _it_is_template(itIsTemplate) {}

                // Empty destructor
                virtual ~PCLEdgeExtraction() {}

                /**
                 * Extract PCL edges
                 * @param cloud input PointCloud
                 * @param label_indices vector of indexes for each edge type
                 * @param out contains categorized edges
                 */
                int extractPCLEdges(const typename pcl::PointCloud<PointT>::Ptr &cloud,
                        std::vector<pcl::PointIndices> & label_indices, pcl::PointCloud<core::CategorisedEdge>::Ptr &out,
                        pcl::PointCloud<core::CategorisedEdge>::Ptr &only_edge, pcl::PointIndices::Ptr &edge_indices);

            private:
                /// Set the tolerance in meters for difference in depth values between neighboring points - PCL
                float _th_dd;
                /// Set the max search distance for deciding occluding and occluded edges - PCL
                int _max_search;
                /// The low threshold value for RGB Canny edge detection (default: 40.0) - PCL
                int _cannyLow;
                /// The high threshold value for RGB Canny edge detection (default: 100.0) - PCL
                int _cannyHigh;
                /// Set edge types
                int _edgeType;
                // Set if it is template from cut point cloud, so boundary edges are also occluding
                bool _it_is_template;

        };

    }
}

#include "pcl_edge_extraction_impl.hpp"

#endif /* COVIS_FEATURE_PCL_EDGES_EXTRACTION_H */
