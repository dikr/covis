// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_TEXLET_3D_EXTRACTION_H
#define COVIS_FEATURE_TEXLET_3D_EXTRACTION_H

#include "../core/texlet_3d.h"
#include "../core/texlet_2d.h"
#include "../calib/convert_input.h"
#include "primitive_3d_extraction.h"

//PCL
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/normal_3d.h>

namespace covis {
    namespace feature {
        /**
         * @class Texlet3DExtraction
         * @ingroup feature
         * @brief Texlet3D extraction class
         *
         * @tparam PointT input point type
         * @author
         */
        template<typename PointT>
        class Texlet3DExtraction : public Primitive3DExtraction<PointT, core::Texlet2D, core::Texlet3D> {
            public:
                /// Empty constructor
                Texlet3DExtraction() {}
                /// Empty destructor
                virtual ~Texlet3DExtraction() {}

                /**
                 * Extract Texlet3D
                 * Gets RGB image from point cloud and computes Texlets2D using provided intrinsic dimensions
                 * @param cloud input point cloud
                 * @param ids doubles representing intrinsic dimensions (i0D, i1D and i2D)
                 * @return vector of Texlet3D
                 */
                std::vector<covis::core::Texlet3D> extract3dTexlets(const typename pcl::PointCloud<PointT>::Ptr &cloud,
                        core::Ids ids);
                /**
                 * Extract Texlet3D
                 * @param tex vector containing Texlets2D
                 * @param cloud input point cloud
                 * @return vector of Texlet3D
                 */
                std::vector<covis::core::Texlet3D> extract3dTexlets(std::vector<covis::core::Texlet2D> &tex,
                        const typename pcl::PointCloud<PointT>::Ptr &cloud);
                /**
                 * Extract Texlet3D
                 * @param tex vector containing Texlets2D
                 * @param cloud input point cloud
                 * @return pointCloud of Texlet3D
                 */
                pcl::PointCloud<covis::core::Texlet3D> extract3dTexletsToPointCloud(
                        std::vector<covis::core::Texlet2D> &tex,
                        const typename pcl::PointCloud<PointT>::Ptr &cloud);
                /**
                 * Extract Texlet3D
                 * Gets RGB image from point cloud and computes Texlets2D using provided intrinsic dimensions
                 * @param cloud input point cloud
                 * @param ids doubles representing intrinsic dimensions (i0D, i1D and i2D)
                 * @return pointCloud of Texlet3D
                 */
                pcl::PointCloud<covis::core::Texlet3D> extract3dTexletsToPointCloud(
                        const typename pcl::PointCloud<PointT>::Ptr &cloud,  core::Ids ids);
        };

    }
}

#include "texlet_3d_extraction_impl.hpp"

#endif /* COVIS_FEATURE_TEXLET_3D_EXTRACTION_H */
