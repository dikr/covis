// Copyright (c) 2014, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_CATEGORIZED_EDGE_DISTANCE_ANGLE_HISTOGRAM_H
#define COVIS_FEATURE_CATEGORIZED_EDGE_DISTANCE_ANGLE_HISTOGRAM_H

// Own
#include "feature_3d.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_types.h>

namespace covis{
    namespace feature{
        /**
         * @class CategorizedEdgeDistanceAngleHistogram
         * @ingroup feature
         * @brief Feature based on point distance and angle to its neighbors

         * @param PointT input point type, must contain XYZ data
         * @tparam N output histogram dimension
         * @tparam NSmall output subhistogram dimension
         * @author Lilita Kiforenko
         */
        template<typename PointT, int N>
        class CategorizedEdgeDistanceAngleHistogram : public Feature3D<PointT,pcl::Histogram<N> > {
                using Feature3D<PointT,pcl::Histogram<N> >::_radius;
                using Feature3D<PointT,pcl::Histogram<N> >::_surface;

                COVIS_STATIC_ASSERT_XYZ(core::CategorisedEdge);
            public:
                /// Empty constructor
                CategorizedEdgeDistanceAngleHistogram(): edge_amount(0), edge_cloud() {}
                
                virtual ~CategorizedEdgeDistanceAngleHistogram() {}

                /**
                 * Compute distance histograms
                 * @param cloud input original point cloud, must contain XYZ
                 * @return histograms (zero for invalid points or points with no neighbors)
                 */
                typename pcl::PointCloud<pcl::Histogram<N> >::Ptr compute(
                        typename pcl::PointCloud<PointT>::ConstPtr cloud);


                /**
                 * Set search radius
                 * @param radius search radius
                 */
                inline void setRadius(float radius) {
                    _radius = radius;
                }

                inline void setEdgeAmount(int amount) {
                    edge_amount = amount;
                }

                inline void setEdgeCloud(typename pcl::PointCloud<core::CategorisedEdge> edgeCloud) {
                    edge_cloud = edgeCloud;
                }

            private:
                int edge_amount;
                typename pcl::PointCloud<core::CategorisedEdge> edge_cloud;

        };
    }
}

#include "categorized_edge_distance_angle_histogram_impl.hpp"
#endif /* COVIS_FEATURE_CATEGORIZED_EDGE_DISTANCE_ANGLE_HISTOGRAM_H */
