// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_LINE_SEGMENT_3D_EXTRACTION_IMPL_HPP
#define COVIS_FEATURE_LINE_SEGMENT_3D_EXTRACTION_IMPL_HPP

#include "line_segment_3d_extraction.h"


namespace covis {
    namespace feature {

        template<typename PointT>
        std::vector<covis::core::LineSegment3D> LineSegment3DExtraction<PointT>::extract3dLineSegments(
                const typename pcl::PointCloud<PointT>::Ptr &cloud,  core::Ids ids){
            std::vector<core::LineSegment3D> out;

            //get the RGB image from point cloud
            cv::Mat rgbImage = calib::ConvertInput<PointT>().getRgbImageFromPointCloud(cloud);

            //extract line segments 2d
            LineSegment2DExtraction ls2d = LineSegment2DExtraction(ids);
            std::vector<covis::core::LineSegment2D> ls = ls2d.extract2DLineSegments(rgbImage);

            out = extract3dLineSegments(ls, cloud);

            return out;
        }

        template<typename PointT>
        std::vector<covis::core::LineSegment3D> LineSegment3DExtraction<PointT>::extract3dLineSegments(
                std::vector<covis::core::LineSegment2D> &ls, const typename pcl::PointCloud<PointT>::Ptr &cloud){

            std::vector<core::LineSegment3D> ls3d =
                    Primitive3DExtraction<PointT, core::LineSegment2D, core::LineSegment3D>().extractECVPrimitives3D(ls, cloud);
            return ls3d;
        }

        template<typename PointT>
        pcl::PointCloud<covis::core::LineSegment3D> LineSegment3DExtraction<PointT>::extract3dLineSegmentsToPointCloud(
                std::vector<covis::core::LineSegment2D> &ls, const typename pcl::PointCloud<PointT>::Ptr & cloud){

            pcl::PointCloud<covis::core::LineSegment3D> out =
                    Primitive3DExtraction<PointT, core::LineSegment2D, covis::core::LineSegment3D>().extractECVPrimitives3DtoPointCloud(ls, cloud);
            return out;
        }

        template<typename PointT>
        pcl::PointCloud<covis::core::LineSegment3D> LineSegment3DExtraction<PointT>::extract3dLineSegmentsToPointCloud(
                const typename pcl::PointCloud<PointT>::Ptr &cloud,  core::Ids ids){

            //get the RGB image from point cloud
            cv::Mat rgbImage = calib::ConvertInput<PointT>().getRgbImageFromPointCloud(cloud);

            //extract line segments 2d
            LineSegment2DExtraction ls2d = LineSegment2DExtraction(ids);
            std::vector<covis::core::LineSegment2D> ls = ls2d.extract2DLineSegments(rgbImage);

            pcl::PointCloud<covis::core::LineSegment3D> out = extract3dLineSegmentsToPointCloud(ls, cloud);

            return out;
        }
    }
}


#endif /* COVIS_FEATURE_LINE_SEGMENT_3D_EXTRACTION_IMPL_HPP */
