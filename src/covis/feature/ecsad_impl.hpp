// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_ECSAD_IMPL_HPP
#define COVIS_FEATURE_ECSAD_IMPL_HPP

// STL
#include <cmath>

// PCL
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>
/// @cond
namespace covis {
    namespace feature {
        template<typename PointT>
        typename pcl::PointCloud<typename ECSAD<PointT>::Histogram>::Ptr ECSAD<PointT>::compute(
                typename pcl::PointCloud<PointT>::Ptr cloud) {
            // Check for valid inputs
            COVIS_ASSERT(cloud && !cloud->empty());
            COVIS_ASSERT_MSG(_radius > 0.0f, "Search radius not set!");

            // Create searches
            typename pcl::search::Search<PointT>::Ptr tree;
            if(cloud->isOrganized())
                tree.reset(new pcl::search::OrganizedNeighbor<PointT>(false));
            else
                tree.reset(new pcl::search::KdTree<PointT>(false));
            tree->setInputCloud(cloud);

            // AGB: Index input cloud or external surface automatically
            typename pcl::PointCloud<PointT>::ConstPtr surf;
            typename pcl::search::Search<PointT>::Ptr surfTree;
            if(_surface) {
                if(_surface->isOrganized())
                    surfTree.reset(new pcl::search::OrganizedNeighbor<PointT>(false));
                else
                    surfTree.reset(new pcl::search::KdTree<PointT>(false));
                
                surf = _surface;
                surfTree->setInputCloud(surf);
            } else {
                surf = cloud;
                surfTree = tree;
            }

            // AGFB: Allocate results
            std::vector<ECSADPatch> patches(cloud->size());
            pcl::PointCloud<Histogram>::Ptr result(new pcl::PointCloud<Histogram>(cloud->width, cloud->height));

            // Rotation separation factor
            float rotIndexer[ECSADPatch::Depth];
            float invRotIndexer[ECSADPatch::Depth];
//            float halfSlice[ECSADPatch::Depth]; // AGB: Unused?
            int depthIndex[ECSADPatch::Depth];
            int angDepthIndex[ECSADPatch::Depth];

            // Depth separations
            float depthSep[ECSADPatch::Depth];
            float sqrDepthSep[ECSADPatch::Depth];
            float partialRadius;
            for(int i = 0; i < ECSADPatch::Depth; ++i) {
                rotIndexer[i] = 0.5 * ECSADPatch::Slices * (i + 1) / M_PI;
                invRotIndexer[i] = 1.0 / rotIndexer[i];
//                halfSlice[i] = M_PI / (ECSADPatch::Slices * (i + 1)); // AGB: Unused?
                depthIndex[i] = ECSADPatch::Slices * (i + 1) * i / 2.0;
                angDepthIndex[i] = ECSADPatch::SlicesHalf * (i + 1) * i / 2.0;

                partialRadius = _radius * (i + 1) / ECSADPatch::Depth;
                depthSep[i] = partialRadius;
                sqrDepthSep[i] = partialRadius * partialRadius;
            }

            const float maxAreaValue = _radius * _radius * M_PI * ECSADPatch::SlicesHalf;

            for(size_t id = 0; id < cloud->size(); ++id) {
                // AGB: Moved all these parameters inside the loop
                // Determine surface info
                cv::Mat_<float> cov(3, 3);
                cv::Mat_<float> eigenvalues, eigenvectors(3, 3);
                float center[3];
                // Determine descriptor variables
                float x1, y1, z1, angle, vertAngle, dist;
                int angleId;
    //            float startAng; // AGB: Unused?
                float centerVec[3];
                // Rotate descriptor variables
                float avrArea;
                float newDesc[ECSADPatch::DescSize];
                float oldAx[3];
    //            float angleX, angleY; // AGB: Unused?
                float weight;
                cv::Mat_<float> covAng(2, 2);
                float xAngle, yAngle;
                float xCor, yCor;
                float angleZ;
                int index1, index2;
                float firstPart, secPart;
                
                float weights[ECSADPatch::DescSize];
                float* fastOpencvPtr1;
                float* fastOpencvPtr2;
                float* fastOpencvPtr3;

                // Determine nearest neighbors for surface patch
                std::vector<int> neighbors;
                std::vector<float> sqrDistances;
                //                Surf_tree_->radiusSearch(*cloud_, id, _radius, neighbors, sqrDistances);
                surfTree->radiusSearch(*cloud, id, _radius, neighbors, sqrDistances);

                // Handle too few neighbors
                // TODO: Add proper handling instead of hack
                if(neighbors.size() < 5) {
                    cloud->points[id].normal_x = 0;
                    cloud->points[id].normal_y = 0;
                    cloud->points[id].normal_z = 1;

                    center[0] = cloud->points[id].x;
                    center[1] = cloud->points[id].y;
                    center[2] = cloud->points[id].z;

                    for(int i = 0; i < 3; i++) {
                        patches[id].center[i] = center[i];
                        patches[id].orientation[i] = 0;
                        patches[id].xAx[i] = 0;
                        patches[id].yAx[i] = 0;
                    }

                    patches[id].xAx[0] = 1;
                    patches[id].yAx[1] = 1;
                    patches[id].orientation[2] = 1;
                    
                    for(int i = 0; i < ECSADPatch::DescSize; i++) {
                        patches[id].desc[i] = 0;
                        patches[id].border[i] = true;
                    }

                    for(int i = 0; i < ECSADPatch::AngleDescSize; i++)
                        patches[id].angleDesc[i] = 0;

                    patches[id].primeCurv = 0;
                    patches[id].secondaryCurv = 0;

                    continue;
                }

                center[0] = 0;
                center[1] = 0;
                center[2] = 0;
                for(std::vector<int>::const_iterator iIt = neighbors.begin(); iIt != neighbors.end(); ++iIt) {
                    center[0] += surf->points[*iIt].x;
                    center[1] += surf->points[*iIt].y;
                    center[2] += surf->points[*iIt].z;
                }
                center[0] = center[0] / neighbors.size();
                center[1] = center[1] / neighbors.size();
                center[2] = center[2] / neighbors.size();

                // Determine orientation of surface patch
                // Covariance matrix, only compute upper right part
                fastOpencvPtr1 = cov.ptr<float>(0);
                fastOpencvPtr2 = cov.ptr<float>(1);
                fastOpencvPtr3 = cov.ptr<float>(2);

                fastOpencvPtr1[0] = 0.0f;
                fastOpencvPtr1[1] = 0.0f;
                fastOpencvPtr1[2] = 0.0f;
                fastOpencvPtr2[1] = 0.0f;
                fastOpencvPtr2[2] = 0.0f;
                fastOpencvPtr3[2] = 0.0f;
                const float xmean = center[0];
                const float ymean = center[1];
                const float zmean = center[2];
                for(std::vector<int>::const_iterator iIt = neighbors.begin(); iIt != neighbors.end(); ++iIt) {
                    //            pointPtr = &(cloud_->points[*iIt]);
                    x1 = surf->points[*iIt].x - xmean;
                    y1 = surf->points[*iIt].y - ymean;
                    z1 = surf->points[*iIt].z - zmean;

                    fastOpencvPtr1[0] += x1 * x1;
                    fastOpencvPtr1[1] += x1 * y1;
                    fastOpencvPtr1[2] += x1 * z1;
                    fastOpencvPtr2[1] += y1 * y1;
                    fastOpencvPtr2[2] += y1 * z1;
                    fastOpencvPtr3[2] += z1 * z1;
                }
                fastOpencvPtr2[0] = fastOpencvPtr1[1];
                fastOpencvPtr3[0] = fastOpencvPtr1[2];
                fastOpencvPtr3[1] = fastOpencvPtr2[2];

                // Eigen decomposition
                cv::eigen(cov, eigenvalues, eigenvectors);

                //determine orientation
                patches[id].orientation[0] = eigenvectors[2][0];
                patches[id].orientation[1] = eigenvectors[2][1];
                patches[id].orientation[2] = eigenvectors[2][2];

                patches[id].xAx[0] = eigenvectors[0][0];
                patches[id].xAx[1] = eigenvectors[0][1];
                patches[id].xAx[2] = eigenvectors[0][2];
                
                // TODO (AGB): Changed the order here to produce a SO(3) frame
//                cross(patches[id].xAx, patches[id].orientation, patches[id].yAx);
                cross(patches[id].orientation, patches[id].xAx, patches[id].yAx);

                patches[id].center[0] = cloud->points[id].x;
                patches[id].center[1] = cloud->points[id].y;
                patches[id].center[2] = cloud->points[id].z;

                // Initialize the descriptor with 0's
                for(int i = 0; i < ECSADPatch::DescSize; i++) {
                    patches[id].desc[i] = 0;
                    weights[i] = 0;
                    patches[id].border[i] = true;
                }

                // Determine descriptor and first depth
                std::vector<float>::const_iterator sqrDistIt = sqrDistances.begin();
                for(std::vector<int>::const_iterator iIt = neighbors.begin(); iIt != neighbors.end(); ++iIt) {
                    if(*sqrDistIt < sqrDepthSep[0] * 0.01) {
                        ++sqrDistIt;
                        continue;
                    }

                    centerVec[0] = surf->points[*iIt].x - cloud->points[id].x;
                    centerVec[1] = surf->points[*iIt].y - cloud->points[id].y;
                    centerVec[2] = surf->points[*iIt].z - cloud->points[id].z;
                    
                    if((*sqrDistIt) <= sqrDepthSep[0]) {
                        x1 = dot(centerVec, patches[id].xAx);
                        y1 = dot(centerVec, patches[id].yAx);
                        z1 = dot(centerVec, patches[id].orientation);
                        angle = atan2(y1, x1) + M_PI;
                        angleId = floor(angle * rotIndexer[0]);

                        dist = sqrt(*sqrDistIt);
//                        vertAngle = asin(z1 / dist);
                        // TODO (AGB): The asin() above sometimes caused NaNs, using atan2 instead
                        vertAngle = atan2(z1, dist);

                        //index point
                        patches[id].desc[angleId] += vertAngle * dist;
                        weights[angleId] += dist;
                    } else {
                        for(int j = 1; j < ECSADPatch::Depth; j++) {
                            if((*sqrDistIt) <= sqrDepthSep[j]) {
                                x1 = dot(centerVec, patches[id].xAx);
                                y1 = dot(centerVec, patches[id].yAx);
                                z1 = dot(centerVec, patches[id].orientation);
                                angle = atan2(y1, x1) + M_PI;
                                angleId = angle * rotIndexer[j];

                                dist = sqrt(*sqrDistIt);
//                                vertAngle = asin(z1 / dist);// atan2(z1, sqrt(x1*x1 + y1*y1));
                                // TODO (AGB): The asin() above sometimes caused NaNs, using commented code
                                vertAngle = atan2(z1, dist);
                                dist -= depthSep[j - 1];

                                //index point
                                patches[id].desc[depthIndex[j] + angleId] += vertAngle * dist;
                                weights[depthIndex[j] + angleId] += dist;

                                break;
                            }
                        }
                    }
                    ++sqrDistIt;
                } // End loop over neighbors

                // Normalize descriptor
                for(int i = 0; i < ECSADPatch::Slices; i++) {
                    if(weights[i] == 0) {
                        patches[id].desc[i] = 0;
                    } else {
                        patches[id].border[i] = false;
                        patches[id].desc[i] = patches[id].desc[i] / weights[i];
                    }
                }

                // Determine remaining depths
                for(int j = 1; j < ECSADPatch::Depth; j++) {
                    // Normalize descriptor
                    for(int i = 0; i < ECSADPatch::Slices*(j+1); i++) {
                        if(weights[depthIndex[j] + i]  == 0) {
                            patches[id].desc[depthIndex[j] + i] = 0;//patches[id].desc[i][j-1];
                        } else {
                            patches[id].desc[depthIndex[j] + i] = patches[id].desc[depthIndex[j] + i] / weights[depthIndex[j] + i];
                            patches[id].border[depthIndex[j] + i] = false;
                        }
                    }
                }

                // Interpolate and extrapolate bad points:
                for(int j = 0; j < ECSADPatch::Depth; j++) {
                    for(int i = 0; i < ECSADPatch::Slices*(j+1); i++) {
                        if(patches[id].border[depthIndex[j] + i]) { // This point is unknown and should be interpolated or extrapolated
                            weight = 0;

                            if(j == 0) {
                                weight++;
                            } else { //if(!patches[id].border[depth_index[j - 1] + i*(j+1)/(j+2)])
                                patches[id].desc[depthIndex[j] + i] +=
                                        patches[id].desc[depthIndex[j - 1] + i * j / (j + 1)];
                                weight++;
                            }

                            if(j < ECSADPatch::Depth-1 &&
                                    !patches[id].border[depthIndex[j + 1] + i * (j + 2) / (j + 1)]) {
                                patches[id].desc[depthIndex[j] + i] +=
                                        patches[id].desc[depthIndex[j + 1] + i * (j + 2) / (j + 1)];
                                weight++;
                            }

                            if(j < ECSADPatch::Depth-1 &&
                                    !patches[id].border[depthIndex[j + 1] + i * (j + 2) / (j + 1) + 1]) {
                                patches[id].desc[depthIndex[j] + i] +=
                                        patches[id].desc[depthIndex[j + 1] + i * (j + 2) / (j + 1) + 1];
                                weight++;
                            }

                            for(int l = ECSADPatch::Slices*(j+1)-1; l <= ECSADPatch::Slices*(j+1)+1; l+= 2) {
                                if(!patches[id].border[depthIndex[j] + ((i + l) % (ECSADPatch::Slices * (j + 1)))]) {
                                    patches[id].desc[depthIndex[j] + i] +=
                                            patches[id].desc[depthIndex[j] +
                                                             ((i + l) % (ECSADPatch::Slices * (j + 1)))];
                                    weight++;
                                }
                            }

                            patches[id].desc[depthIndex[j] + i] /= weight;
                        }
                    }
                }

                // Determine smallest area slice of descriptor
                avrArea = 0;
                for(int i = 0; i < ECSADPatch::SlicesHalf; i++) {
                    // Determine angle
                    patches[id].angleDesc[i] =
                            M_PI + patches[id].desc[i] + patches[id].desc[i + ECSADPatch::SlicesHalf];
                    avrArea += patches[id].angleDesc[i] * sqrDepthSep[0];
                }

                for(int j = 1; j < ECSADPatch::Depth; j++) {
                    for(int i = 0; i < ECSADPatch::SlicesHalf * (j + 1); i++) {
                        // Determine angle
                        patches[id].angleDesc[angDepthIndex[j] + i] =
                                M_PI + patches[id].desc[depthIndex[j] + i] +
                                patches[id].desc[depthIndex[j] + i + ECSADPatch::SlicesHalf * (j + 1)];

                        // Add ring slice
                        avrArea += patches[id].angleDesc[angDepthIndex[j] + i] / (j + 1) *
                                (sqrDepthSep[j] - sqrDepthSep[j - 1]);
                    }
                }

                // If the area is too big the normal is flipped
                if(avrArea > maxAreaValue) {
                    // Reorient normal
                    patches[id].orientation[0] = -patches[id].orientation[0];
                    patches[id].orientation[1] = -patches[id].orientation[1];
                    patches[id].orientation[2] = -patches[id].orientation[2];

                    // Reorient descriptor:
                    for(int j = 0; j < ECSADPatch::Depth; j++)
                        for(int i = 0; i < ECSADPatch::SlicesHalf * (j + 1); i++)
                            patches[id].angleDesc[angDepthIndex[j] + i] =
                                    2 * M_PI - patches[id].angleDesc[angDepthIndex[j] + i];

                    for(int j = 0; j < ECSADPatch::Depth; j++)
                        for(int i = 0; i < ECSADPatch::Slices * (j + 1); i++)
                            patches[id].desc[depthIndex[j] + i] = -patches[id].desc[depthIndex[j] + i];

                    // Reorient y
                    patches[id].yAx[0] = -patches[id].yAx[0];
                    patches[id].yAx[1] = -patches[id].yAx[1];
                    patches[id].yAx[2] = -patches[id].yAx[2];

                    // Mirror descriptor
                    for(int j = 0; j < ECSADPatch::Depth; j++)
                        for(int i = 0; i < ECSADPatch::SlicesHalf * (j + 1); i++)
                            newDesc[angDepthIndex[j] + i] =
                                    patches[id].angleDesc[angDepthIndex[j] +
                                                           ECSADPatch::SlicesHalf * (j + 1) - 1 - i];

                    for(int j = 0; j < ECSADPatch::Depth; j++)
                        for(int i = 0; i < ECSADPatch::SlicesHalf * (j + 1); i++)
                            patches[id].angleDesc[angDepthIndex[j] + i] = newDesc[angDepthIndex[j] + i];

                    for(int j = 0; j < ECSADPatch::Depth; j++)
                        for(int i = 0; i < ECSADPatch::Slices * (j + 1); i++)
                            newDesc[depthIndex[j] + i] =
                                    patches[id].desc[depthIndex[j] + ECSADPatch::Slices * (j + 1) - 1 - i];

                    for(int j = 0; j < ECSADPatch::Depth; j++)
                        for(int i = 0; i < ECSADPatch::Slices * (j + 1); i++)
                            patches[id].desc[depthIndex[j] + i] = newDesc[depthIndex[j] + i];
                } // End flipping if area is too big

                //PCA full angles to determine x-y rotation
                //Determine orientation of surface patch
                //Covariance matrix, only compute upper right part
                covAng[0][0] = 0.0f;
                covAng[0][1] = 0.0f;
                covAng[1][1] = 0.0f;
                for(int j = 0; j < ECSADPatch::Depth; j++) {
                    for (int i = 0; i < ECSADPatch::SlicesHalf * (j + 1); ++i) {
                        xAngle = cos((i + 0.5) * invRotIndexer[j]);
                        yAngle = sin((i + 0.5) * invRotIndexer[j]);

                        weight = (M_PI - patches[id].angleDesc[angDepthIndex[j] + i]);
                        //if(weight < 0) weight = 0;
                        //if(weight > M_PI) weight = M_PI;
                        covAng[0][0] += xAngle * xAngle * weight * weight * (j + 1) * (j + 1);
                        covAng[0][1] += xAngle * yAngle * weight * weight * (j + 1) * (j + 1);
                        covAng[1][1] += yAngle * yAngle * weight * weight * (j + 1) * (j + 1);
                    }
                }

                // Set lower left part
                covAng[1][0] = covAng[0][1];

                cv::eigen(covAng, eigenvalues, eigenvectors);

                // Determine orientation
                xCor = eigenvectors[0][0];
                yCor = eigenvectors[0][1];
                angleZ = atan2(yCor, xCor) - 0.5*M_PI;

                // Rotate RF acording to z-x and z-y rotation
                oldAx[0] = patches[id].xAx[0];
                oldAx[1] = patches[id].xAx[1];
                oldAx[2] = patches[id].xAx[2];
                patches[id].xAx[0] = patches[id].xAx[0] * cos(angleZ) + patches[id].yAx[0] * sin(angleZ);
                patches[id].xAx[1] = patches[id].xAx[1] * cos(angleZ) + patches[id].yAx[1] * sin(angleZ);
                patches[id].xAx[2] = patches[id].xAx[2] * cos(angleZ) + patches[id].yAx[2] * sin(angleZ);
                patches[id].yAx[0] = patches[id].yAx[0] * cos(angleZ) - oldAx[0] * sin(angleZ);
                patches[id].yAx[1] = patches[id].yAx[1] * cos(angleZ) - oldAx[1] * sin(angleZ);
                patches[id].yAx[2] = patches[id].yAx[2] * cos(angleZ) - oldAx[2] * sin(angleZ);


                //curvatures:
                patches[id].primeCurv =
                        eigenvalues(0) / (2.0 * M_PI * ECSADPatch::AngleDescSize * (ECSADPatch::Depth-1));
                patches[id].secondaryCurv =
                        eigenvalues(1) / (2.0 * M_PI * ECSADPatch::AngleDescSize * (ECSADPatch::Depth-1));

                if(patches[id].primeCurv > 1)
                    patches[id].primeCurv = 1;

                if(patches[id].secondaryCurv > 1)
                    patches[id].secondaryCurv = 1;

                for(int j = 0; j < ECSADPatch::Depth; j++) {
                    for(int i = 0; i < ECSADPatch::SlicesHalf * (j + 1); i++) {
                        secPart = angleZ * rotIndexer[j] - floor(angleZ * rotIndexer[j]);
                        firstPart = 1.0 - secPart;
                        index1 = i + floor(angleZ * rotIndexer[j]);

                        if(index1 < 0)
                            index1 = index1 + ECSADPatch::SlicesHalf * (j + 1);
                        else
                            index1 = index1 % (ECSADPatch::SlicesHalf * (j + 1));

                        index2 = index1 + 1;
                        if(index2 >= ECSADPatch::SlicesHalf * (j + 1))
                            index2 = index2 - ECSADPatch::SlicesHalf * (j + 1);
                        
                        newDesc[angDepthIndex[j] + i] = patches[id].angleDesc[angDepthIndex[j] + index1] * firstPart +
                                patches[id].angleDesc[angDepthIndex[j] + index2] * secPart;
                    }
                }

                for(int j = 0; j < ECSADPatch::Depth; j++)
                    for(int i = 0; i < ECSADPatch::SlicesHalf * (j + 1); i++)
                        patches[id].angleDesc[angDepthIndex[j] + i] = newDesc[angDepthIndex[j] + i];

                // Rotate descriptors:
                for(int j = 0; j < ECSADPatch::Depth; j++) {
                    for(int i = 0; i < ECSADPatch::Slices * (j + 1); i++) {
                        secPart = angleZ * rotIndexer[j] - floor(angleZ * rotIndexer[j]);
                        firstPart = 1.0 - secPart;
                        index1 = i + floor(angleZ * rotIndexer[j]);

                        if(index1 < 0)
                            index1 = index1 + ECSADPatch::Slices * (j + 1);
                        else
                            index1 = index1 % (ECSADPatch::Slices * (j + 1));

                        index2 = index1 + 1;
                        if(index2 >= ECSADPatch::Slices * (j + 1))
                            index2 = index2 - ECSADPatch::Slices * (j + 1);

                        newDesc[depthIndex[j] + i] = patches[id].desc[depthIndex[j] + index1] * firstPart
                                + patches[id].desc[depthIndex[j] + index2] * secPart;
                    }
                }

                for(int j = 0; j < ECSADPatch::Depth; j++)
                    for(int i = 0; i < ECSADPatch::Slices * (j + 1); i++)
                        patches[id].desc[depthIndex[j] + i] = newDesc[depthIndex[j] + i];

                // Store normal
                cloud->points[id].normal_x = patches[id].orientation[0];
                cloud->points[id].normal_y = patches[id].orientation[1];
                cloud->points[id].normal_z = patches[id].orientation[2];
            } // End loop over point cloud
            
            // AGB: Write back the descriptor part of the patches to the output histograms
            for(size_t i = 0; i < patches.size(); ++i) {
                std::memcpy(result->points[i].histogram,
                        patches[i].angleDesc,
                        ECSADPatch::AngleDescSize * sizeof(float));
                // Normalize
                const float sum = std::accumulate(
                        result->points[i].histogram,
                        result->points[i].histogram + ECSADPatch::AngleDescSize,
                        0.0f);
                if(sum > 0.0f) {
                    for(size_t j = 0; j < ECSADPatch::AngleDescSize; ++j)
                        result->points[i].histogram[j] /= sum;
                } else {
                    for(size_t j = 0; j < ECSADPatch::AngleDescSize; ++j)
                        result->points[i].histogram[j] = 0.0f;
                }
            }
            
            return result;
        } // End compute()
    }
}
/// @endcond

#endif /* COVIS_FEATURE_DISTANCE_HISTOGRAM_IMPL_HPP */
