// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_LINE_SEGMENT_3D_EXTRACTION_H
#define COVIS_FEATURE_LINE_SEGMENT_3D_EXTRACTION_H

#include "../core/line_segment_2d.h"
#include "../core/line_segment_3d.h"
#include "../calib/convert_input.h"
#include "primitive_3d_extraction.h"

//PCL
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/normal_3d.h>

namespace covis {
    namespace feature {
        /**
         * @class LineSegment3DExtraction
         * @ingroup feature
         * @brief LineSegment3D extraction class
         *
         * @tparam PointT input point type
         * @author
         */
        template<typename PointT>
        class LineSegment3DExtraction : public Primitive3DExtraction<PointT, core::LineSegment2D, core::LineSegment3D> {
            public:
                /// Empty constructor
                LineSegment3DExtraction() {}
                /// Empty destructor
                virtual ~LineSegment3DExtraction() {}

                /**
                 * Extract LineSegments3D
                 * Gets RGB image from point cloud and computes LineSegments2D using provided intrinsic dimensions
                 * @param cloud input point cloud
                 * @param ids doubles representing intrinsic dimensions (i0D, i1D and i2D)
                 * @return vector of LineSegment3D
                 */
                std::vector<covis::core::LineSegment3D> extract3dLineSegments(
                        const typename pcl::PointCloud<PointT>::Ptr &cloud, core::Ids ids);
                /**
                 * Extract LineSegments3D
                 * @param ls vector containing LineSegmets2D
                 * @param cloud input point cloud
                 * @return vector of LineSegment3D
                 */
                std::vector<covis::core::LineSegment3D> extract3dLineSegments(
                        std::vector<covis::core::LineSegment2D> &ls,
                        const typename pcl::PointCloud<PointT>::Ptr &cloud);
                /**
                 * Extract LineSegments3D
                 * @param ls vector containing LineSegmets2D
                 * @param cloud input point cloud
                 * @return pointCloud of LineSegment3D
                 */
                pcl::PointCloud<covis::core::LineSegment3D> extract3dLineSegmentsToPointCloud(
                        std::vector<covis::core::LineSegment2D> &ls,
                        const typename pcl::PointCloud<PointT>::Ptr &cloud);
                /**
                 * Extract LineSegments3D
                 * Gets RGB image from point cloud and computes LineSegments2D using provided intrinsic dimensions
                 * @param cloud input point cloud
                 * @param ids doubles representing intrinsic dimensions (i0D, i1D and i2D)
                 * @return pointCloud of LineSegment3D
                 */
                pcl::PointCloud<covis::core::LineSegment3D> extract3dLineSegmentsToPointCloud(
                        const typename pcl::PointCloud<PointT>::Ptr &cloud,  core::Ids ids);
        };

    }
}

#include "line_segment_3d_extraction_impl.hpp"

#endif /* COVIS_FEATURE_LINE_SEGMENT_3D_EXTRACTION_H */
