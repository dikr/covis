// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "scoped_timer.h"

#include <limits>

using namespace covis::core;

volatile int ScopedTimer::_instances = 0;

ScopedTimer::ScopedTimer(const std::string& name, std::ostream& os) : _os(os) {
    _start = cv::getTickCount();
    _previous = _start;
    _name = name;
    _idx = _instances++;
    
    indent();
    
    if (_name.empty())
        os << "Starting timer" << std::endl;
    else
        os << "Starting timer (" << _name << ")" << std::endl;
}

ScopedTimer::~ScopedTimer() {
    double elapsed;
    std::string unit;
    convert(seconds(), elapsed, unit);
    
    indent();
    
    if (_name.empty())
        _os << "----> Total elapsed time: " << elapsed << " " << unit << std::endl;
    else
        _os << "----> Total elapsed time (" << _name << "): " << elapsed << " " << unit << std::endl;
    
    --_instances;
}

double ScopedTimer::intermediate(const std::string& desc) {
    const int64 ticks = cv::getTickCount();
    const double secondss = seconds(ticks);
    const double deltaa = delta(ticks);
    
    bool first = ( std::abs(_start - _previous) < std::numeric_limits<double>::epsilon() );

    _previous = ticks;
    
    double elapsed;
    std::string unit;
    convert(secondss, elapsed, unit);
    
    indent();
    
    if (_name.empty()) {
        if (desc.empty())
            _os << "--> Elapsed time: " << elapsed << " " << unit;
        else
            _os << "--> Elapsed time (" << desc << "): " << elapsed << " " << unit;
    } else {
        if (desc.empty())
            _os << "--> Elapsed time (" << _name << "): " << elapsed << " " << unit;
        else
            _os << "--> Elapsed time (" << _name << ", " << desc << "): " << elapsed << " " << unit;
    }
    
    if (!first) {
        double elapsedP;
        std::string unitP;
        convert(deltaa, elapsedP, unitP);
        
        _os << " (intermediate: " << elapsedP << " " << unitP << ")";
    }
    
    _os << std::endl;
    
    return secondss;
}

void ScopedTimer::convert(double s, double& elapsed, std::string& unit) {
    elapsed = s;
    unit = "s";
    if (elapsed < 1.0) {
        elapsed *= 1000.0;
        unit = "ms";
        if (elapsed < 1.0) {
            elapsed *= 1000.0;
            unit = "us";
        }
    } else
        if (elapsed > 60.0) {
            elapsed /= 60.0;
            unit = "m";
            if (elapsed > 60.0) {
                elapsed /= 60.0;
                unit = "h";
            }
        }
}

void ScopedTimer::indent(const std::string& istring) {
    if (_idx == 0)
        return;
    
    for (int i = 0; i < _idx; ++i)
        _os << istring;
    _os << " ";
}
