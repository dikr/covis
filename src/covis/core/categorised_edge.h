// Copyright (c) 2014, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_CATEGORISED_EDGE_H
#define COVIS_CORE_CATEGORISED_EDGE_H

namespace covis {
    namespace core {
        /**
         * @ingroup core
         * @struct CategorisedEdge
         * @brief Structure for representing a categorised 3d edge
         *
         * CategorisedEdge point consist of:
         *   xyz coordinate
         *   rgb color
         *   normal
         *   gradient (dx, dy)
         *   2D position (x_2D, y_2D)
         *   index in original point cloud (idx_original)
         *   index in each of the 4 edge types (idx_edge_types), -1 corresponds to none
         *
         * @author Lilita Kiforenko
         */
        struct CategorisedEdge
        {
                PCL_ADD_POINT4D;                    // preferred way of adding a XYZ+padding
                PCL_ADD_RGB;
                PCL_ADD_NORMAL4D;
                float dx;
                float dy;
                int x_2D;
                int y_2D;
                int idx_original;
                int idx_edge;
                int idx_edge_types[4];
                EIGEN_MAKE_ALIGNED_OPERATOR_NEW     // make sure our new allocators are aligned
        } EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment
    }
}

POINT_CLOUD_REGISTER_POINT_STRUCT (covis::core::CategorisedEdge,
        (float, x, x)
        (float, y, y)
        (float, z, z)
        (uint32_t, rgba, rgba)
        (float, normal[0], normal_x)
        (float, normal[1], normal_y)
        (float, normal[2], normal_z)
        (float, dx, dx)
        (float, dy, dy)
        (int, x_2D, x_2D)
        (int, y_2D, y_2D)
        (int, idx_original, idx_original)
        (int, idx_edge, idx_edge)
        (int[4], idx_edge_types, idx_edge_types)
)

#endif /* COVIS_CORE_CATEGORISED_EDGE_H */


