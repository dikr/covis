// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "visu_3d.h"

using namespace covis::visu;

void Visu3D::addPose(const Eigen::Matrix4f& T, float length, const std::string& label) {
    pcl::PointXYZ p(T(0,3), T(1,3), T(2,3));
    pcl::PointXYZ px = p;
    pcl::PointXYZ py = p;
    pcl::PointXYZ pz = p;
    
    px.getVector3fMap() += T.block<3,1>(0,0) * length;
    py.getVector3fMap() += T.block<3,1>(0,1) * length;
    pz.getVector3fMap() += T.block<3,1>(0,2) * length;
    
    visu.addLine(p, px, 1.0, 0.0, 0.0, label + "_x");
    visu.addLine(p, py, 0.0, 1.0, 0.0, label + "_y");
    visu.addLine(p, pz, 0.0, 0.0, 1.0, label + "_z");
}

void Visu3D::jet(double v, double& r, double& g, double& b) {
    COVIS_ASSERT(v >= 0.0 && v <= 1.0);
    r = g = b = 1.0;
    if (v < 0.25) {
       r = 0;
       g = 4 * v;
    } else if (v < 0.5) {
       r = 0;
       b = 1 + 4 * (0.25 - v);
    } else if (v < 0.75) {
       r = 4 * (v - 0.5);
       b = 0;
    } else {
       g = 1 + 4 * (0.75 - v);
       b = 0;
    }
}
