// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "montage_visu.h"
#include "../core/macros.h"
#include "../covis_config.h"

// OpenCV
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace covis::visu;

void MontageVisu::show() {
    const cv::Mat tiled = tile();
    cv::imshow(_title, tiled);
    cv::waitKey();
}

cv::Mat MontageVisu::tile() {
    COVIS_ASSERT_MSG(_images != NULL && _n > 0, "Images not set!");
    COVIS_ASSERT(_scale > 0.0);
    
    // Find max height/width and channels
    int maxHeight = 0;
    int maxWidth = 0;
    int maxChannels = 0;
    for(size_t i = 0; i < _n; ++i) {
        const int rowsScaled(_images[i].rows * _scale + 0.5);
        const int colsScaled(_images[i].cols * _scale + 0.5);
        if(rowsScaled > maxHeight)
            maxHeight = rowsScaled;
        if(colsScaled > maxWidth)
            maxWidth = colsScaled;
        if(_images[i].channels() > maxChannels)
            maxChannels = _images[i].channels();
    }
    COVIS_ASSERT_MSG(maxHeight > 0 && maxWidth > 0, "All images are empty!");
    COVIS_ASSERT_MSG(maxChannels > 0 && maxChannels != 2 && maxChannels <= 4,
            "Cannot deal with images with 0, 2 or >= 5 channels!");
    
    // Get number of rows/columns in the montage, and whether we use grayscale or RGB
    const int rowss = ( _rows > 0 ? _rows : int(sqrt(double(_n)) + 0.5) );
    const int cols = int(ceil(double(_n) / double(rowss)) + 0.5);
    const bool useGray = (maxChannels == 1);
    
    // Instantiate montage
    cv::Mat result(rowss * maxHeight + (rowss + 1) * _spacing,
            cols * maxWidth + (cols + 1) * _spacing,
            (useGray ? CV_32F : CV_32FC3),
            _bg);
    
    // Insert images into montage, with padding if necessary
    size_t i = 0;
    for(int r = 0; r < rowss; ++r) {
        // Vertical offset in montage
        const int y = _spacing + r * (maxHeight + _spacing);
        for(int c = 0; c < cols; ++c) {
            // Horizontal offset in montage
            const int x = _spacing + c * (maxWidth + _spacing);
            // Output montage ROI
            cv::Mat roi = result(cv::Rect(x, y, maxWidth, maxHeight));
            
            // Current image
            cv::Mat img = _images[i];
            
            // Scale if necessary
            if(_scale != 1.0)
                cv::resize(img, img, cv::Size(), _scale, _scale);
            
            // Ensure correct depth, and scale the elements to [0,1] if necessary
            if(img.depth() == CV_64F)
                img.convertTo(img, CV_32F);
            else if(img.depth() == CV_8U)
                img.convertTo(img, CV_32F, 1.0 / 255.0);
            else if(img.depth() == CV_16U)
                img.convertTo(img, CV_32F, 1.0 / 65535.0);
            COVIS_ASSERT_MSG(img.depth() == CV_32F, "Cannot handle signed image elements (" << img.depth() << ")!");
            
            // Ensure correct number of channels
            int bgr2gray, gray2bgr;
#ifdef OPENCV_3
            bgr2gray = cv::COLOR_BGR2GRAY;
            gray2bgr = cv::COLOR_GRAY2BGR;
#else
            bgr2gray = CV_BGR2GRAY;
            gray2bgr = CV_GRAY2BGR;
#endif
            
            if(useGray) { // Use gray, convert RGB(A) types
                if(img.channels() == 3)
                    cv::cvtColor(img, img, bgr2gray);
                else if(img.channels() == 4)
                    cv::cvtColor(img, img, bgr2gray);
            } else { // Use RGB, convert gray and RGBA types
                if(img.channels() == 1)
                    cv::cvtColor(img, img, gray2bgr);
                else if(img.channels() == 4)
                    cv::cvtColor(img, img, gray2bgr);
            }

            // Add border before insertion if necessary
            if(img.rows < maxHeight || img.cols < maxWidth) {
                const int dh = maxHeight - img.rows; // Vertical number padding pixels
                const int top = (_centering ? dh / 2 : 0); // Insert in the center or at the top
                const int bottom = maxHeight - (img.rows + top);
                const int dw = maxWidth - img.cols; // Horizontal number padding pixels
                const int left = (_centering ? dw / 2 : 0); // Insert in the center or to the left
                const int right = maxWidth - (img.cols + left);
                cv::copyMakeBorder(img, img, top, bottom, left, right, cv::BORDER_CONSTANT, _bg);
            }
            
            // Insert
            COVIS_ASSERT_DBG(img.rows == roi.rows && img.cols == roi.cols);
            img.copyTo(roi);

            // Break when done
            if(++i == _n)
                break;
        }
    }
    
    return result;
}
