// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_CORR_VISU_IMPL_HPP
#define COVIS_VISU_CORR_VISU_IMPL_HPP

// PCL
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>



namespace covis {
    namespace visu {
        template<typename PointT>
        void CorrVisu<PointT>::show() {
            // Sanity checks
            COVIS_ASSERT_MSG(_query && _target, "Query and target must be set!");
            COVIS_ASSERT_MSG(_corrpcl, "Correspondences not set!");

            // Apply separation, if enabled
            typename pcl::PointCloud<PointT>::Ptr query2(new pcl::PointCloud<PointT>);
            if(_separate) {
                // In case of no overlap, nothing actually happens here, as t will be zero
                pcl::transformPointCloud<PointT>(*_query, *query2, separate(_query, _target), Eigen::Quaternionf::Identity());
            }  else {
                *query2 = *_query;
            }
            
            // Add point clouds and correspondences
            switch(_mode) {
                case LINES:
                    if(_showPoints) {
                        addPointCloud<PointT>(query2, "query");
                        addPointCloud<PointT>(_target, "target");
                    }
                    visu.removeCorrespondences("correspondences");
                    visu.addCorrespondences<PointT>(query2, _target, *_corrpcl, std::max<size_t>(1, _level));
#if PCL_VERSION_COMPARE(>=,1,7,2)
                    static_cast<vtkLODActor*>((*visu.getShapeActorMap())["correspondences"].GetPointer())->GetProperty()->SetLineWidth(_lineWidth);
#endif
                    break;
                case COLORS: {
                    // Find minimal scalar value
                    float smin = FLT_MAX;
                    for(size_t i = 0; i < query2->size(); ++i)
                        if(query2->points[i].x < smin)
                            smin = query2->points[i].z;
                    smin *= (smin < 0.0f ? 1.25f : 0.75f); // Decrease to offset from the rest of the colors
                    // Initialize scalar fields for query/target
                    std::vector<float> fieldQuery(_query->size(), smin);
                    std::vector<float> fieldTarget(_target->size(), smin);
                    // Set scalar fields
                    for(size_t i = 0; i < _corrpcl->size(); ++i)
                        fieldQuery[(*_corrpcl)[i].index_query] =
                                fieldTarget[(*_corrpcl)[i].index_match] =
                                        query2->points[(*_corrpcl)[i].index_query].z;
                    // TODO: Add a dummy point to query in the case that _corrpcl are dense
                    query2->push_back(PointT());
                    fieldQuery.push_back(smin);
                    // Add to visu
                    addScalarField<PointT>(query2, fieldQuery, "query");
                    addScalarField<PointT>(_target, fieldTarget, "target");
                    break;
                } default:
                    COVIS_THROW("Unknown correspondence visualization method!");
            }
            
            // Start
            Visu3D::show();
        }
        
        template<typename PointT>
        Eigen::Vector3f CorrVisu<PointT>::separate(typename pcl::PointCloud<PointT>::ConstPtr query,
                typename pcl::PointCloud<PointT>::ConstPtr target,
                float multiplier) const {
            // Get extremal points
            PointT qmin, qmax, tmin, tmax;
            pcl::getMinMax3D<PointT>(*query, qmin, qmax);
            pcl::getMinMax3D<PointT>(*target, tmin, tmax);
            
            // If any dimension is overlapping, we move the query points away below
            Eigen::Vector3f t(0, 0, 0);
            if(qmax.x > tmin.x)
                t[0] = qmax.x - tmin.x;
            else if(qmax.y > tmin.y)
                t[1] = qmax.y - tmin.y;
            else if(qmax.z > tmin.z)
                t[2] = qmax.z - tmin.z;
            
            return multiplier * t;
        }
    }
}

#endif
