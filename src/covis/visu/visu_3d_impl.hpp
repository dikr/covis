// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_VISU_3D_IMPL_HPP
#define COVIS_VISU_VISU_3D_IMPL_HPP

// PCL
#include <pcl/visualization/common/actor_map.h>

namespace covis {
    namespace visu {
        /// @cond
        // Convert a point cloud to a blob
        template<typename PointT>
        inline pcl::PCLPointCloud2::Ptr toMsg(typename pcl::PointCloud<PointT>::ConstPtr cloud) {
            pcl::PCLPointCloud2::Ptr msg(new pcl::PCLPointCloud2);
            pcl::toPCLPointCloud2<PointT>(*cloud, *msg);
            return msg;
        }

        // Return true if the visualizer already has a specific color handler for a labeled point cloud
        inline bool hasColorHandler(Visu3D::VisuT& visu,
                const std::string& label,
                const pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::ConstPtr& h) {
            // If a point cloud with label exists
            pcl::visualization::CloudActorMapPtr map = visu.getCloudActorMap();
            pcl::visualization::CloudActorMap::iterator it = map->find(label);
            if(it != map->end()) {
                // Get all color handlers of the point cloud
                const std::vector<pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::ConstPtr>& handlers =
                        it->second.color_handlers;
                // Search for the labeled color handler
                for(size_t i = 0; i < handlers.size(); ++i)
                    if(handlers[i]->getName() == h->getName() && handlers[i]->getFieldName() == h->getFieldName())
                        return true;
            }
            
            return false;
        }
        
        // Make sure that h comes before other in the list of color handlers for the labeled cloud
        inline void promoteColorHandler(Visu3D::VisuT& visu,
                const std::string& label,
                const pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::ConstPtr& h,
                const pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::ConstPtr& other) {
            COVIS_ASSERT(h->getName() != other->getName());
            // If a point cloud with label exists
            pcl::visualization::CloudActorMapPtr map = visu.getCloudActorMap();
            pcl::visualization::CloudActorMap::iterator it = map->find(label);
            if(it != map->end()) {
                // Get all color handlers of the point cloud
                std::vector<pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::ConstPtr>& handlers =
                        it->second.color_handlers;
                int idxname = -1;
                int idxother = -1;
                for(int i = 0; i < int(handlers.size()); ++i) {
                    if(handlers[i]->getName() == h->getName())
                        idxname = i;
                    else if(handlers[i]->getName() == other->getName())
                        idxother = i;
                }
                
                if(idxname != -1 && idxother != -1)
                    std::swap(handlers[idxname], handlers[idxother]);
            }
        }
        
        // XYZ point clouds, add geometry-based color handlers
        template<typename PointT>
        struct Visu3D::PointCloudAdder<PointT,false> {
            static void add(VisuT& visu,
                    typename pcl::PointCloud<PointT>::ConstPtr cloud,
                    const std::string& label) {
                // Convert to message
                pcl::PCLPointCloud2::Ptr msg = toMsg<PointT>(cloud);
                
                // Identity transformation
                Eigen::Vector4f t(0, 0, 0, 1);
                Eigen::Quaternionf q = Eigen::Quaternionf::Identity();
                
                // Random color handler
                pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::Ptr crand(
                        new pcl::visualization::PointCloudColorHandlerRandom<pcl::PCLPointCloud2>(msg));
                if(!hasColorHandler(visu, label, crand))
                    visu.addPointCloud(msg, crand, t, q, label);
                
                // Depth color handler
                pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::Ptr cdepth(
                        new pcl::visualization::PointCloudColorHandlerGenericField<pcl::PCLPointCloud2>(msg, "z"));
                if(!hasColorHandler(visu, label, cdepth))
                    visu.addPointCloud(msg, cdepth, t, q, label);
            }
        };
        
        // RGB point clouds
        template<typename PointT>
        struct Visu3D::PointCloudAdder<PointT,true> {
            static void add(VisuT& visu,
                    typename pcl::PointCloud<PointT>::ConstPtr cloud,
                    const std::string& label) {
                // Convert to message
                pcl::PCLPointCloud2::Ptr msg = toMsg<PointT>(cloud);
                
                // Identity transformation
                Eigen::Vector4f t(0, 0, 0, 1);
                Eigen::Quaternionf q = Eigen::Quaternionf::Identity();
                
                // RGB color handler
                pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::Ptr c(
                        new pcl::visualization::PointCloudColorHandlerRGBField<pcl::PCLPointCloud2>(msg));
                if(!hasColorHandler(visu, label, c))
                    visu.addPointCloud(msg, c, t, q, label);
                
//                // Make sure the new color handler comes before any previous geometry-based handlers
//                pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::Ptr dummy(
//                        new pcl::visualization::PointCloudColorHandlerRandom<pcl::PCLPointCloud2>(msg));
//                promoteColorHandler(visu, label, c, dummy);

                // Geometry-based color handlers
                PointCloudAdder<PointT,false>::add(visu, cloud, label);
            }
        };
        /// @endcond
        
        template<typename PointT>
        void Visu3D::addPointCloud(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                const std::string& label) {
            // Add point cloud
            PointCloudAdder<PointT>::add(visu, cloud, label);
        }
        
        template<typename PointNT>
        void Visu3D::addNormals(typename pcl::PointCloud<PointNT>::ConstPtr cloud,
                size_t level,
                float scale,
                const std::string& label) {
            // Add the usual renders
            PointCloudAdder<PointNT>::add(visu, cloud, label);
            
            // Add the normals with a new label
            visu.addPointCloudNormals<PointNT>(cloud, std::max<int>(1, int(level)), scale, label + "_normals");
        }
        template<typename PointNT>
        void Visu3D::addGaussMap(typename pcl::PointCloud<PointNT>::ConstPtr normals,
                const pcl::RGB& color,
                size_t pointSize,
                const std::string& label) {
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr map(
                    new pcl::PointCloud<pcl::PointXYZRGBA>(normals->size(), 1));
            for(size_t i = 0; i < normals->size(); ++i) {
                std::memcpy(map->points[i].data, normals->points[i].data_n, 3 * sizeof(float));
                map->points[i].rgba = color.rgba;
            }
            addPointCloud<pcl::PointXYZRGBA>(map, label);
            visu.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,
                    std::max<double>(1, double(pointSize)), label);
        }
        
        template<typename PointNT>
        void Visu3D::addGaussMap(typename pcl::PointCloud<PointNT>::ConstPtr normals,
                size_t pointSize,
                const std::string& label) {
            pcl::RGB white;
            white.r = white.g = white.b = 255;
            addGaussMap(normals, white, pointSize, label);
        }
        
        template<typename PointT>
        void Visu3D::addColor(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                uchar r, uchar g, uchar b,
                const std::string& label) {
            // Convert to messages and copy field into cloud
            pcl::PCLPointCloud2::Ptr msgcloud = toMsg<PointT>(cloud);
            
            // Identity transformation
            Eigen::Vector4f t(0, 0, 0, 1);
            Eigen::Quaternionf q = Eigen::Quaternionf::Identity();
            
            // Constant color handler
            pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::Ptr c(
                    new pcl::visualization::PointCloudColorHandlerCustom<pcl::PCLPointCloud2>(
                            msgcloud, double(r), double(g), double(b)));
            if(!hasColorHandler(visu, label, c))
                visu.addPointCloud(msgcloud, c, t, q, label);
            
//            // Make sure the new color handler comes before any previous geometry-based color handlers
//            pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::Ptr dummy(
//                    new pcl::visualization::PointCloudColorHandlerRandom<pcl::PCLPointCloud2>(msgcloud));
//            promoteColorHandler(visu, label, c, dummy);
            
            // Add the usual renders after
            PointCloudAdder<PointT>::add(visu, cloud, label);
        }
        
        template<typename PointXYZT, typename PointFieldT>
        void Visu3D::addScalarField(typename pcl::PointCloud<PointXYZT>::ConstPtr cloud,
                typename pcl::PointCloud<PointFieldT>::ConstPtr field,
                const std::string& fname,
                const std::string& label) {
            COVIS_ASSERT(field->size() == cloud->size());
            
            // Convert to messages and copy field into cloud
            pcl::PCLPointCloud2::Ptr msgcloud = toMsg<PointXYZT>(cloud);
            pcl::PCLPointCloud2::Ptr msgfield = toMsg<PointFieldT>(field);
            
            // Identity transformation
            Eigen::Vector4f t(0, 0, 0, 1);
            Eigen::Quaternionf q = Eigen::Quaternionf::Identity();
            
            // Field color handler
            pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::Ptr c(
                    new pcl::visualization::PointCloudColorHandlerGenericField<pcl::PCLPointCloud2>(msgfield, fname));
            if(!hasColorHandler(visu, label, c))
                visu.addPointCloud(msgcloud, c, t, q, label);
            
//            // Make sure the new color handler comes before any previous geometry-based color handlers
//            pcl::visualization::PointCloudColorHandler<pcl::PCLPointCloud2>::Ptr dummy(
//                    new pcl::visualization::PointCloudColorHandlerRandom<pcl::PCLPointCloud2>(msgcloud));
//            promoteColorHandler(visu, label, c, dummy);
            
            // Add the usual renders after
            PointCloudAdder<PointXYZT>::add(visu, cloud, label);
        }

        template<typename PointT>
        void Visu3D::addScalarField(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                const std::string& fname,
                const std::string& label) {
            addScalarField<PointT, PointT>(cloud, cloud, fname, label);
        }
        
        template<typename PointT, typename T>
        void Visu3D::addScalarField(typename pcl::PointCloud<PointT>::ConstPtr cloud,
                const typename std::vector<T>& field,
                const std::string& label) {
            COVIS_ASSERT(field.size() == cloud->size());
            
            // Put XYZ and field values together
            pcl::PointCloud<pcl::PointXYZI>::Ptr cfield(
                    new pcl::PointCloud<pcl::PointXYZI>(cloud->width, cloud->height));
            for(size_t i = 0; i < field.size(); ++i) {
                (*cfield)[i].intensity = field[i];
                (*cfield)[i].x = (*cloud)[i].x;
                (*cfield)[i].y = (*cloud)[i].y;
                (*cfield)[i].z = (*cloud)[i].z;
            }
            
            addScalarField<pcl::PointXYZI>(cfield, "intensity", label);
        }
    }
}

#endif
