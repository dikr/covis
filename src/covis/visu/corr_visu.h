// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_CORR_VISU_H
#define COVIS_VISU_CORR_VISU_H

// Own
#include "visu_3d.h"
#include "../core/correspondence.h"

namespace covis {
    namespace visu {
        /**
         * @ingroup visu
         * Control the way correspondences are visualized
         */
        enum CORRESPONDENCE_VISU_MODE {
            LINES,//!< Show correspondences by lines between corresponding points
            COLORS //!< Show corresponding points by the same color - red means no match
        };
        
        /**
         * @class CorrVisu
         * @ingroup visu
         * @brief Point cloud correspondence visualizer
         * 
         * This class enables you to show two point clouds and correspondences between them.
         * If the two point clouds are overlapping, this class automatically translates the point clouds away from each
         * other for better visualization.
         * 
         * Use this class by first setting data using, @ref setQuery(), @ref setTarget() and @ref setCorr(), followed by
         * a call to @ref show(). Additionally, you can enable automatic separation of the two models using
         * @ref setSeparate(), which is convenient if the two models overlap.
         * 
         * @note If one to many correspondences are input, only the first (best) match for each correspondence is shown
         * @tparam PointT point type, must contain XYZ data
         * @author Anders Glent Buch
         */
        template<typename PointT>
        class CorrVisu : public Visu3D {
            public:
                /// Default constructor: set the level to 10 and disable automatic separation
                CorrVisu() : Visu3D("Correspondences"), _level(10), _mode(LINES), _separate(false), _showPoints(true),
                _lineWidth(1.0) {
                    setShowOrigo(false);
                }

                /// Destructor
                virtual ~CorrVisu() {}
                
                /// @copydoc VisuBase::show()
                void show();
                
                /**
                 * Get a suitable translation vector to apply to the query model for separating the two models - this
                 * function is convenient if the two models are overlapping, which leads to poor visualizations of
                 * correspondences
                 * @param query query model
                 * @param target target model
                 * @param multiplier if set to 1, the models are separated so that their bounding boxes exactly touch
                 * each other, and if higher the models are moved further apart
                 * @return translation to apply to the query model
                 */
                Eigen::Vector3f separate(typename pcl::PointCloud<PointT>::ConstPtr query,
                        typename pcl::PointCloud<PointT>::ConstPtr target,
                        float multiplier = -2.0f) const;
                
                /**
                 * Set query point set
                 * @param query query point set
                 */
                inline void setQuery(typename pcl::PointCloud<PointT>::ConstPtr query) {
                    _query = query;
                }
                
                /**
                 * Set target point set
                 * @param target target point set
                 */
                inline void setTarget(typename pcl::PointCloud<PointT>::ConstPtr target) {
                    _target = target;
                }
                
                /**
                 * Set correspondences query --> target
                 * 
                 * @param corrpcl correspondences
                 */
                inline void setCorr(pcl::CorrespondencesConstPtr corrpcl) {
                    _corrpcl = corrpcl;
                }
                
                /**
                 * Set correspondences query --> target
                 * 
                 * @param corr correspondences
                 */
                inline void setCorr(const core::Correspondence::Vec& corr) {
                    setCorr(core::convert(corr));
                }
                
                /**
                 * Set level
                 * @param level level
                 */
                inline void setLevel(size_t level) {
                    _level = level;
                }
                
                /**
                 * Set mode
                 * @param mode mode
                 */
                inline void setMode(CORRESPONDENCE_VISU_MODE mode) {
                    _mode = mode;
                }
                
                /**
                 * Set separation flag
                 * @param separate separation flag
                 */
                inline void setSeparate(bool separate) {
                    _separate = separate;
                }
                
                /**
                 * Set show points flag
                 * @param showPoints show points flag
                 */
                inline void setShowPoints(bool showPoints) {
                    _showPoints = showPoints;
                }
                
                /**
                 * Set the line width of the correspondence lines
                 * @param lineWidth line width of the correspondence lines
                 * @note this functionality is only supported for PCL 1.7.2 and above
                 */
                inline void setLineWidth(bool lineWidth) {
                    _lineWidth = lineWidth;
                }
                
            protected:
                /// Pointer to query point cloud
                typename pcl::PointCloud<PointT>::ConstPtr _query;
                
                /// Pointer to target point cloud
                typename pcl::PointCloud<PointT>::ConstPtr _target;
                
                /// Pointer to PCL correspondences
                pcl::CorrespondencesConstPtr _corrpcl;
                
                /// Level: show every level'th correspondence
                size_t _level;
                
                /// Visualization mode for correspondences
                CORRESPONDENCE_VISU_MODE _mode;
                
                /// If set to true, the models are separated before visualization
                bool _separate;
                
                /// If set to false, the query/target points are not added to the visualizer during line visualization
                bool _showPoints;
                
                /// Set the desired line width for the correspondences
                float _lineWidth;
        };
        
        /**
         * @ingroup visu
         * Show correspondences
         * @param query query point set
         * @param target target point set
         * @param corrpcl PCL correspondences
         * @param level show every level'th correspondence
         * @param mode choose which way correspondences are shown (see @ref CORRESPONDENCE_VISU_MODE)
         * @param title window title
         */
        template<typename PointT>
        inline void showCorrespondences(typename pcl::PointCloud<PointT>::ConstPtr query,
                typename pcl::PointCloud<PointT>::ConstPtr target,
                pcl::CorrespondencesConstPtr corrpcl,
                size_t level = 1,
                CORRESPONDENCE_VISU_MODE mode = LINES,
                const std::string title = "Correspondences") {
            CorrVisu<PointT> cv;
            cv.setQuery(query);
            cv.setTarget(target);
            cv.setCorr(corrpcl);
            cv.setLevel(level);
            cv.setMode(mode);
            cv.setTitle(title);
            cv.show();
        }
        
        /**
         * @ingroup visu
         * Show correspondences
         * @param query query point set
         * @param target target point set
         * @param corr correspondences
         * @param level show every level'th correspondence
         * @param mode choose which way correspondences are shown (see @ref CORRESPONDENCE_VISU_MODE)
         * @param title window title
         */
        template<typename PointT>
        void showCorrespondences(typename pcl::PointCloud<PointT>::ConstPtr query,
                typename pcl::PointCloud<PointT>::ConstPtr target,
                const core::Correspondence::Vec& corr,
                size_t level = 1,
                CORRESPONDENCE_VISU_MODE mode = LINES,
                const std::string title = "Correspondences") {
            CorrVisu<PointT> cv;
            cv.setQuery(query);
            cv.setTarget(target);
            cv.setCorr(corr);
            cv.setLevel(level);
            cv.setMode(mode);
            cv.setTitle(title);
            cv.show();
        }
    }
}

#include "corr_visu_impl.hpp"

#endif
