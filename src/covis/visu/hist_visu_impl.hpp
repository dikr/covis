// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_HIST_VISU_IMPL_HPP
#define COVIS_VISU_HIST_VISU_IMPL_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace covis {
    namespace visu {
        template<size_t N, typename T>
        void HistVisu<N,T>::show() {
            COVIS_ASSERT_MSG(_data != NULL, "Histogram data not set!");
            COVIS_ASSERT_MSG(_size.height > 0 && _size.width > 0, "Illegal window size!");
            
            // Set window size and horizontal increment
            const int rows = _size.height;
            const int cols = _size.width;
            const double hinc = double(cols) / double(N);
            
            // Find max
            T max = _data[0];
            for(size_t i = 1; i < N; ++i)
                if(_data[i] > max)
                    max = _data[i];
            
            // Initialize histogram RGB image (white background)
            cv::Mat_<cv::Vec3b> himg(rows, cols, cv::Vec3b(255, 255, 255));
            
            // Loop over histogram bins
            for(int i = 0; i < cols; ++i) {
                const T& value = _data[i];
                if(value > T(0)) {
                    // Bottom left
                    cv::Point pt1(int(double(i) * hinc + 0.5), rows - 1);
                    // Bottom right
                    cv::Point pt2(int(double(i) * hinc + hinc - 1 + 0.5), rows - 1);
                    // Top right
                    cv::Point pt3(int(double(i) * hinc + hinc - 1 + 0.5), rows - int(value * double(rows) / max + 0.5));
                    // Top left
                    cv::Point pt4(int(double(i) * hinc + 0.5), rows - int(value * double(rows) / max + 0.5));
                    
                    // Draw bar (azure)
                    cv::Point pts[] = {pt1, pt2, pt3, pt4, pt1};
                    cv::fillConvexPoly(himg, pts, 5, cv::Scalar(255, 127, 0));
                    
                    // Draw border around bar (black)
                    cv::rectangle(himg, pt1, pt3, cv::Scalar(0, 0, 0), 1);
                }
            }
            
            // Show histogram image
            cv::imshow(_title, himg);
            cv::waitKey();
        }
    }
}

#endif
