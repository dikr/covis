// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_VISU_DETECTION_VISU_IMPL_HPP
#define COVIS_VISU_DETECTION_VISU_IMPL_HPP

// PCL
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>

namespace covis {
    namespace visu {
        template<typename PointT>
        void DetectionVisu<PointT>::show() {
            // Sanity checks
            COVIS_ASSERT(!_queries.empty() || !_queryMeshes.empty());
            COVIS_ASSERT(!_detections.empty());
            
            if(_targetMesh)
                addMesh(_targetMesh, "target");
            else if(_target)
                addPointCloud<PointT>(_target, "target");
            
            if(!_queryMeshes.empty()) {
                std::vector<pcl::PolygonMesh::Ptr> qt(_detections.size());
                for(size_t i = 0; i < _detections.size(); ++i) {
                    // Get index of detected object
                    const size_t idx = _detections[i].idx;
                    // Convert to point cloud
                    pcl::PointCloud<PointT> cloudt;
                    pcl::fromPCLPointCloud2(_queryMeshes[idx]->cloud, cloudt);
                    // Transform point cloud
                    pcl::transformPointCloud(cloudt, cloudt, _detections[i].pose);
                    // Copy back
                    qt[i].reset(new pcl::PolygonMesh);
                    qt[i]->header = _queryMeshes[idx]->header;
                    qt[i]->polygons = _queryMeshes[idx]->polygons;
                    pcl::toPCLPointCloud2<PointT>(cloudt, qt[i]->cloud);
                    // Generate label
                    std::ostringstream oss;
                    oss << "detection_" << i;
                    // Generate color red --> blue
                    double r, g, b;
                    if(_queryMeshes.size() == 1)
                        jet(1.0, r, g, b);
                    else
                        jet(1.0 - double(idx) / double(_queryMeshes.size()), r, g, b);
                    // Add
                    addMesh(qt[i], oss.str());
                    getMeshActor(oss.str())->GetProperty()->SetColor(r, g, b);
                    getMeshActor(oss.str())->GetProperty()->SetOpacity(0.5);
                }
                
                Visu3D::show();
            } else {
                std::vector<typename pcl::PointCloud<PointT>::Ptr> qt(_detections.size());
                for(size_t i = 0; i < _detections.size(); ++i) {
                    // Get index of detected object
                    const size_t idx = _detections[i].idx;
                    // Transform point cloud
                    qt[i].reset(new pcl::PointCloud<PointT>);
                    pcl::transformPointCloud<PointT>(*_queries[idx], *qt[i], _detections[i].pose);
                    // Generate label
                    std::ostringstream oss;
                    oss << "detection_" << i;
                    // Generate color red --> blue
                    double r, g, b;
                    if(_queries.size() == 1)
                        jet(1.0, r, g, b);
                    else
                        jet(1.0 - double(idx) / double(_queries.size() - 1), r, g, b);
                    // Add
                    addColor<PointT>(qt[i], 255.0 * r, 255.0 * g, 255.0 * b, oss.str());
                    visu.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, oss.str());
                }
                
                Visu3D::show();
            }
        }
    }
}

#endif
