// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CONFIG_H
#define COVIS_CONFIG_H

// CoViS version in various formats
#define COVIS_VERSION_STRING "@COVIS_VERSION@"
#define COVIS_VERSION @COVIS_VERSION@
#define COVIS_VERSION_MAJOR @COVIS_VERSION_MAJOR@
#define COVIS_VERSION_MINOR @COVIS_VERSION_MINOR@
#define COVIS_VERSION_PATCH @COVIS_VERSION_PATCH@

// CoViS version suitable for numerical comparison: MAJOR*10000+MINOR*100+PATCH
#define COVIS_VERSION_CMP @COVIS_VERSION_CMP@

// CoViS revision (will be set to -1 if not detected)
#define COVIS_REVISION @COVIS_REVISION@

// CoViS build type
#define COVIS_BUILD_TYPE "@CMAKE_BUILD_TYPE@"

// Defined if CoViS is statically built
#cmakedefine COVIS_BUILD_STATIC 1

// Log level, possible values defined in core/macros.h
#cmakedefine COVIS_LOG_LEVEL COVIS_LOG_LEVEL_@COVIS_LOG_LEVEL@

// Defined if OpenCV version >= 3 was found
#cmakedefine OPENCV_3 1

// Defined if FLANN version >= 1.8 was found
#cmakedefine FLANN_1_8 1

// Defined if Visualization Toolkit was found
#cmakedefine VTK_FOUND 1

// Defined if CUDA was found
#cmakedefine CUDA_FOUND 1

// Defined if OpenMP was found
#cmakedefine OPENMP_FOUND 1

// Defined if OpenNI2 was found
#cmakedefine OPENNI2_FOUND 1

// Defined if NurbsPP was found
#cmakedefine NURBSPP_FOUND 1

#endif