// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "intrinsic_dimension.h"
#include "intensity_gradient.h"
#include "../core/macros.h"

#include <cmath>

#include <opencv2/imgproc/imgproc.hpp>

using namespace covis::filter;

cv::Mat IntrinsicDimension::filter(const cv::Mat& image) {
    IntensityGradient ms;
    cv::Mat result = ms.filter(image);

    cv::Mat_<double> mop[2];
    cv::split(result, mop);

    return filter(mop[0], mop[1]);
}

cv::Mat IntrinsicDimension::filter(const cv::Mat_<double> & m, const cv::Mat_<double> & o) {
    // Check inputs
    COVIS_ASSERT(!m.empty());
    COVIS_ASSERT(o.size() == m.size());

    // Image size
    const cv::Size size = m.size();

    // Instantiate result
    cv::Mat_<double> i0D(size);
    cv::Mat_<double> i1D(size);
    cv::Mat_<double> i2D(size);

    // Parameters
    const bool scale = true; // Scale line variance flag
    const double thres = 0.35; // The average of raw magnitude is mapped to this value
    const double alpha = atanh(thres) / cv::mean(m)[0]; // For normalization of magnitude
    const double beta = 5; // For normalization of y values

    // Maximum magnitude
    double mmax;
    if (_normalizeLinear)
        cv::minMaxLoc(m, 0, &mmax);

    // Compute normalized magnitude and real/imaginary images of the complex iD-cone
    cv::Mat_<double> mag(size);
    cv::Mat_<double> real(size);
    cv::Mat_<double> imag(size);

    for (int r = 0; r < size.height; ++r) {
        for (int c = 0; c < size.width; ++c) {
            const double magNorm = (_normalizeLinear ? m[r][c] / mmax : std::tanh(alpha * m[r][c]));
            COVIS_ASSERT_DBG(magNorm > 0.0 - 1e-5 && magNorm < 1.0 + 1e-5);

            // Double angle representation
            const double ori2 = 2.0 * o[r][c];
            const double realrc = magNorm * std::cos(ori2);

            const double imagrc = magNorm * std::sin(ori2);
            COVIS_ASSERT_DBG(realrc > -1.0 - 1e-5 && realrc < 1.0 + 1e-5);
            COVIS_ASSERT_DBG(imagrc > -1.0 - 1e-5 && imagrc < 1.0 + 1e-5);

            mag[r][c] = magNorm;
            real[r][c] = realrc;
            imag[r][c] = imagrc;
        }
    }

    // Smooth magnitude and real/imaginary images
    cv::Mat_<double> magSmooth, realSmooth, imagSmooth;
    cv::GaussianBlur(mag, magSmooth, cv::Size(0, 0), _sigma);
    cv::GaussianBlur(real, realSmooth, cv::Size(0, 0), _sigma);
    cv::GaussianBlur(imag, imagSmooth, cv::Size(0, 0), _sigma);

    // Compute ID using smoothed images
    for (int r = 0; r < size.height; r++) {
        for (int c = 0; c < size.width; c++) {
            const double xL = magSmooth[r][c];
            COVIS_ASSERT(xL > 0.0 - 1e-5 && xL < 1.0 + 1e-5);

            const double realrc = realSmooth[r][c];
            const double imagrc = imagSmooth[r][c];
            const double ynorm = std::sqrt(realrc * realrc + imagrc * imagrc);

            double yL;
            if (scale) {
                // Average line variance in square
                const double ynormSquare = (xL > DBL_EPSILON ? ynorm / xL : 0.5);
                yL = xL * std::pow(ynormSquare, beta);
            } else {
                yL = ynorm;
            }
            COVIS_ASSERT_DBG(yL > 0.0 - 1e-5 && yL < 1.0 + 1e-5);
            COVIS_ASSERT_DBG(xL > yL - 1e-5);

            // Barycentric coordinates
            i0D[r][c] = 1.0 - xL;
            i1D[r][c] = yL;
            i2D[r][c] = xL - yL;
        }
    }

    // Produce final result
    cv::Mat_<double> iD[3] = {i0D, i1D, i2D};
    cv::Mat result;
    cv::merge(iD, 3, result);
    
    return result;
}
