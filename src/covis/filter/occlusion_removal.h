// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_OCCLUSION_REMOVAL_H
#define COVIS_FILTER_OCCLUSION_REMOVAL_H

// Own
#include "point_cloud_filter_base.h"
#include "../core/macros.h"

// Boost
#include <boost/shared_ptr.hpp>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/octree/octree_pointcloud.h>

namespace covis {
    namespace filter {
        /**
         * @class OcclusionRemoval
         * @ingroup filter
         * @brief Remove occluded points from a point cloud
         * 
         * This class uses an octree for determining whether points in space are occluded by other points.
         * Start by inputting a target (scene) point cloud using @ref setTarget() (and optionally @ref setResolution()),
         * and then you can check whether points from an arbitrary input point cloud are occluded using @ref filter().
         * You can also input an already initialized octree, in which case the resolution parameter will be ignored.
         * 
         * Multiple queries are no problem, since the octree of the target is maintained until destruction.
         *
         * @tparam PointT input point type, must contain XYZ data
         * @author Anders Glent Buch
         * @example example/occlusion_removal/occlusion_removal.cpp
         */
        template<typename PointT>
        class OcclusionRemoval : public PointCloudFilterBase<PointT> {
            public:
                /// Pointer type
                typedef typename boost::shared_ptr<OcclusionRemoval<PointT> > Ptr;
                
                /**
                 * Default constructor: set resolution to automatic, resolution multiplier to 1.5 and viewpoint to zero
                 */
                OcclusionRemoval() : _resolution(-1.0), _multiplier(1.5) {
                    _viewpoint.x = 0.0f;
                    _viewpoint.y = 0.0f;
                    _viewpoint.z = 0.0f;
                }
                
                /// Destructor
                virtual ~OcclusionRemoval() {}
                
                /// @copydoc PointCloudFilterBase::filter()
                typename pcl::PointCloud<PointT>::Ptr filter(typename pcl::PointCloud<PointT>::ConstPtr cloud);
                
                /**
                 * Get occlusion mask - only valid after a call to @ref filter()
                 * @return occlusion mask
                 */
                inline const std::vector<bool>& getMask() const {
                    return _mask;
                }
                
                /**
                 * Set voxel grid resolution
                 * @param resolution voxel grid resolution
                 */
                inline void setResolution(float resolution) {
                    _resolution = resolution;
                }
                
                /**
                 * Get voxel grid resolution (<= 0 means automatic)
                 * @return voxel grid resolution
                 */
                inline float getResolution() const {
                    return _resolution;
                }
                
                /**
                 * Set viewpoint
                 * @param viewpoint viewpoint
                 */
                inline void setViewpoint(const PointT& viewpoint) {
                    _viewpoint = viewpoint;
                }
                
                /**
                 * Get viewpoint
                 * @return viewpoint
                 */
                inline const PointT& getViewpoint() const {
                    return _viewpoint;
                }
                
                /**
                 * Set target point cloud representing the scene
                 * @param target
                 */
                inline void setTarget(typename pcl::PointCloud<PointT>::ConstPtr target) {
                    _target = target;
                }
                
                /**
                 * Set octree - this function overrides @ref setTarget()!
                 * @param octree active octree, must be initialized with the input points!
                 */
                inline void setOctree(typename pcl::octree::OctreePointCloud<PointT>::Ptr octree) {
                    _octree = octree;
                }
                
                /**
                 * Get octree
                 * @return active octree
                 */
                inline typename pcl::octree::OctreePointCloud<PointT>::Ptr getOctree() {
                    return _octree;
                }
                
            protected:
                /// Voxel grid resolution (<= 0 means automatic)
                float _resolution;
                
                /**
                 * Voxel grid resolution, only used for automatic resolution estimation
                 * This parameter is needed because automatic point cloud resolution often underestimates it
                 */
                float _multiplier;
                
                /// Viewpoint
                PointT _viewpoint;
                
                /// Occlusion mask, each visible point will have a true
                std::vector<bool> _mask;
                
                /// Target point set
                typename pcl::PointCloud<PointT>::ConstPtr _target;
                
                /// Octree type
                typedef pcl::octree::OctreePointCloud<PointT> OctreeT;
                
                /// Octree used by this class
                typename OctreeT::Ptr _octree;
                
                /**
                 * Check if a single point is occluded by a point cloud represented by its octree
                 * @param octree octree
                 * @param p query point
                 * @param minx min bbox x coordinate
                 * @param miny min bbox y coordinate
                 * @param minz min bbox z coordinate
                 * @return true if point is visible, false if it is occluded
                 */
                inline bool isVisible(typename OctreeT::Ptr& octree,
                        const PointT& p,
                        double minx,
                        double miny,
                        double minz) const {
                    // Generate octree key
                    const int kx((p.x - minx) / _resolution);
                    const int ky((p.y - miny) / _resolution);
                    const int kz((p.z - minz) / _resolution);
                    
                    // Voxel centroid
                    Eigen::Vector3f c(minx + _resolution * (float(kx) + 0.5f),
                            miny + _resolution * (float(ky) + 0.5f),
                            minz + _resolution * (float(kz) + 0.5f));
                    
                    // Get direction/step vector from viewpoint to voxel centroid
                    Eigen::Vector3f r = c - _viewpoint.getVector3fMap();
                    const float norm = r.norm();
                    const Eigen::Vector3f rstep = r * _resolution / norm; // Normalized to length equal to resolution

                    // Number of steps
                    const size_t numStep(norm / _resolution);

                    // Decrement the vector until we reach viewpoint
                    if(numStep >= 2) {
                        // Move out of the voxel, and the immediate next voxel
                        r -= 2 * rstep;
                        
                        // Move towards viewpoint, without ending there
                        for(size_t j = 0; j < numStep - 2; ++j) {
                            if(octree->isVoxelOccupiedAtPoint(double(r[0]), double(r[1]), double(r[2]))) {
                                // Point occluded
                                return false;
                            }
                            r -= rstep;
                        }
                    }
                    
                    return true;
                }
        };
    }
}

#include "occlusion_removal_impl.hpp"

#endif