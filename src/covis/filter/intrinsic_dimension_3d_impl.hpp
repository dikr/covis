// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_INTRINSIC_DIMENSION_3D_IMPL_HPP
#define COVIS_FILTER_INTRINSIC_DIMENSION_3D_IMPL_HPP

// Own
#include "../core/macros.h"

// PCL
#include <pcl/common/centroid.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>

namespace covis {
    namespace filter {
        template<typename PointInT, typename PointOutT>
        typename pcl::PointCloud<PointOutT>::Ptr IntrinsicDimension3D<PointInT, PointOutT>::filter(
                typename pcl::PointCloud<PointInT>::ConstPtr cloud) {
            // Sanity checks
            COVIS_ASSERT(cloud && !cloud->empty());
            COVIS_ASSERT(_minScale > 0.0f);
            
            // Create search index
            typename pcl::search::Search<PointInT>::Ptr search;
            if(cloud->isOrganized())
                search.reset(new pcl::search::OrganizedNeighbor<PointInT>(true));
            else
                search.reset(new pcl::search::KdTree<PointInT>(true));
            search->setInputCloud(cloud);
            
            // TODO: Allocate result, default to ID0 at lowest scale
            typename pcl::PointCloud<PointOutT>::Ptr result(
                    new pcl::PointCloud<PointOutT>(
                            cloud->width, cloud->height, core::Ids(1, 0, 0, _minScale * _minScale)));
            
            // Scale increase
            const float factor = std::sqrt(2);
            const float factorsq = factor * factor;
            
            // Maximum possible scale
            const float maxScale = (_octaves == 0 ? _minScale : _minScale * std::pow(factor, float(_octaves)));
            
            // Start
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t i = 0; i < cloud->size(); ++i) {
                // Find neighbors at the maximum possible scale
                std::vector<int> indices;
                std::vector<float> distsq;
                search->radiusSearch(int(i), double(maxScale), indices, distsq);
                
                // Initialize loop parameters
                float emin = std::numeric_limits<float>::max(); // Entropy to be minimized
                size_t k = 0; // Number of neighbors at current scale
                
                // Loop over sampled scales, squared
                float scalesq = _minScale * _minScale;
                for(size_t o = 0; o <= _octaves; ++o, scalesq *= factorsq) {
                    // If previous iteration reached the final neighbor, break
                    if(k == indices.size())
                        break;
                    
                    // Find k
                    while(k < indices.size() && distsq[k] < scalesq)
                        ++k;
                    
                    // Get the k-NN indices
                    const std::vector<int> idxk(indices.begin(), indices.begin() + k);
                    
                    // Compute covariance matrix
                    Eigen::Matrix3f cov;
                    Eigen::Vector4f mean;
                    pcl::computeMeanAndCovarianceMatrix(*cloud, idxk, cov, mean);
                    
                    // Find eigenvalues
                    Eigen::Matrix3f evec;
                    Eigen::Vector3f eval;
                    pcl::eigen33(cov, evec, eval);
                    
                    float sigma1 = eval[2]; // Largest
                    float sigma2 = eval[1];
                    float sigma3 = eval[0]; // Smallest
                    
                    // TODO: It seems that pcl::eigen33() sometimes produces negative values
                    if(sigma1 < 0.0f)
                        sigma1 = -sigma1;
                    if(sigma2 < 0.0f)
                        sigma2 = -sigma2;
                    if(sigma3 < 0.0f)
                        sigma3 = -sigma3;
                    
                    COVIS_ASSERT(sigma1 >= sigma2 && sigma2 >= sigma3 && sigma3 >= 0.0f);
                    
                    const float eps = 1e-9;
                    
                    if(sigma1 < eps)
                        continue;
                    sigma3 = std::sqrt(sigma3);
                    sigma2 = std::sqrt(sigma2);
                    sigma1 = std::sqrt(sigma1);
                    
                    // Geometric features
                    const float a1D = (sigma1 - sigma2) / sigma1; // Point spread in one dimension
                    const float a2D = (sigma2 - sigma3) / sigma1; // Point spread in two dimensions
                    const float a3D = sigma3 / sigma1; // Point spread in three dimensions
                    
                    // Compute entropy
                    const float e = -a1D * std::log(a1D) - a2D * std::log(a2D) - a3D * std::log(a3D);
                    
                    // If minimized, update results
                    if(e < emin) {
                        emin = e;
                        // Set iD confidences - NOTE that 0 and 1 are switched, which is more meaningful for volumes
                        (*result)[i].id0 = a2D;
                        (*result)[i].id1 = a1D;
                        (*result)[i].id2 = a3D;
                        (*result)[i].scale = scalesq; // Will be sqrt'ed below
                    }
                } // End loop over octaves
                
                // Take Euclidean scale
                (*result)[i].scale = std::sqrt((*result)[i].scale);
            } // End loop over points
            
            return result;
        }
    }
}

#endif