// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_OCCLUSION_REMOVAL_IMPL_HPP
#define COVIS_FILTER_OCCLUSION_REMOVAL_IMPL_HPP

// Own
#include "../core/stat.h"
#include "../detect/point_search.h"

namespace covis {
    namespace filter {
        template<typename PointT>
        typename pcl::PointCloud<PointT>::Ptr OcclusionRemoval<PointT>::filter(
                typename pcl::PointCloud<PointT>::ConstPtr cloud) {
            // Sanity checks
            COVIS_ASSERT(_target);
            COVIS_ASSERT(cloud);
            COVIS_ASSERT(pcl_isfinite(_viewpoint.x) && pcl_isfinite(_viewpoint.y) && pcl_isfinite(_viewpoint.z));
            
            // Build an octree, if it does not exist already
            if(!_octree) {
                // If resolution not set, compute it
                if(_resolution <= 0.0) {
                    COVIS_ASSERT(_multiplier > 0.0);
                    _resolution = _multiplier * detect::computeResolution<PointT>(_target);
                }
                
                COVIS_ASSERT(_resolution > 0.0);
                _octree.reset(new OctreeT(_resolution));
                _octree->setInputCloud(_target);
                _octree->addPointsFromInputCloud();
            }
            
            // Get limits
            double minx, miny, minz, dummy;
            _octree->getBoundingBox(minx, miny, minz, dummy, dummy, dummy);
            
            // Non-occluded points
            typename pcl::PointCloud<PointT>::Ptr visible(new pcl::PointCloud<PointT>);
            visible->reserve(cloud->size());
            
            // Output mask
            _mask.resize(cloud->size());
            
            // Start
            for(size_t i = 0; i < cloud->size(); ++i) {
                // Current point
                const PointT& p = (*cloud)[i];
                
                _mask[i] = isVisible(_octree, p, minx, miny, minz);
                
                // If point not occluded, store it
                if(_mask[i])
                    visible->push_back(p);
            }
            
            return visible;
        }
    }
}

#endif