// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "intensity_gradient.h"
#include "../core/macros.h"

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>

using namespace covis::filter;

cv::Mat IntensityGradient::filter(const cv::Mat& image) {
    // Check input
    COVIS_ASSERT_MSG(!image.empty(), "Empty input image!");
    COVIS_ASSERT(image.channels() == 1 || image.channels() == 3 || image.channels() == 4);
    
    // Test whether we are doing smoothing
    const bool smooth = (_sigma > 0.0);
    
    // Check that kernel size is zero or odd
    if(smooth)
        COVIS_ASSERT_MSG((_ksize.width == 0 && _ksize.height == 0) || ((_ksize.width & 1) && (_ksize.height & 1)),
                "When doing presmoothing, kernel must be either 0 (automatic) or odd-sized!");
    
    // Convert input image to gray if necessary
    cv::Mat imgray = image;
    if(image.channels() == 3 || image.channels() == 4)
        cv::cvtColor(imgray, imgray, cv::COLOR_BGR2GRAY);
    
    // Convert to double
    if(imgray.type() != CV_64F)
        imgray = cv::Mat_<double>(imgray);
    
    // Smooth, if enabled
    cv::Mat imblur;
    if(smooth)
        cv::GaussianBlur(imgray, imblur, _ksize, _sigma);
    else
        imblur = imgray;
    
    // Compute derivatives using 1x3 and 3x1 derivative kernels
    cv::Mat dx, dy;
    cv::Sobel(imblur, dx, -1, 1, 0, 1);
    cv::Sobel(imblur, dy, -1, 0, 1, 1);
    
    // Compute polar representation
    cv::Mat polar[2];
    cv::cartToPolar(dx, dy, polar[0], polar[1]);
    
    // Produce final result
    cv::Mat result;
    cv::merge(polar, 2, result);
    
    return result;
}
