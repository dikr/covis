// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_POINT_CLOUD_FILTER_BASE_H
#define COVIS_FILTER_POINT_CLOUD_FILTER_BASE_H

// Own
#include "filter_base.h"

// PCL
#include <pcl/point_cloud.h>

namespace covis {
    namespace filter {
        /**
         * @class PointCloudFilterBase
         * @ingroup filter
         * @brief Point cloud filtering base class
         *
         * Base class for point cloud filters. For point cloud, a filter is understood in various ways:
         *   - Linear and non-linear neighborhood operations such as e.g. @ref IntrinsicDimension3D
         *   - More global operations, e.g. @ref OcclusionRemoval
         *
         * @tparam PointInT input point type
         * @tparam PointOutT output point type, defaults to the input point type
         * @author Anders Glent Buch
         */
        template<typename PointInT, typename PointOutT = PointInT>
        class PointCloudFilterBase : public FilterBase {
            public:
                /// Empty constructor
                PointCloudFilterBase() {}

                /// Destructor
                virtual ~PointCloudFilterBase() {}

                /**
                 * Apply the filter to an input point cloud
                 * @param cloud input point cloud
                 * @return filtered point cloud
                 */
                virtual typename pcl::PointCloud<PointOutT>::Ptr filter(
                        typename pcl::PointCloud<PointInT>::ConstPtr cloud) = 0;
        };
    }
}

#endif