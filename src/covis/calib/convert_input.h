
#ifndef COVIS_CALIB_CONVERT_INPUT_H
#define COVIS_CALIB_CONVERT_INPUT_H

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <opencv2/core/core.hpp>

namespace covis {
    namespace calib {

        template<typename PointRGB>
        class ConvertInput {
            public:
                ConvertInput() { }
                virtual ~ConvertInput() { }

                cv::Mat getRgbImageFromPointCloud(const typename pcl::PointCloud<PointRGB>::Ptr &cloud);

        };
    }
}

#include "convert_input_impl.hpp"

#endif /* COVIS_CONVERT_INPUT_H */
