#define BOOST_TEST_MODULE filter
#include <boost/test/included/unit_test.hpp>

#include <opencv2/opencv.hpp>

#include <covis/covis.h>
using namespace covis::core;
using namespace covis::filter;

BOOST_AUTO_TEST_SUITE(filter_monogenic_signal)

BOOST_AUTO_TEST_CASE(filter_monogenic_signal_filter) {
    // Load an image
    const cv::Mat image = cv::imread("lena.png");
    BOOST_REQUIRE(!image.empty());
    
    // Instantiate filter
    MonogenicSignal ms;

    // Test all three filters
    for(int i = 0; i < 3; ++i) {
        // Setup filter
        if(i == 0)
            ms.setFilterType(MonogenicSignal::MONOGENIC_FREQ0028);
        else if(i == 1)
            ms.setFilterType(MonogenicSignal::MONOGENIC_FREQ0055);
        else
            ms.setFilterType(MonogenicSignal::MONOGENIC_FREQ0110);
        
        // Run real filter
        ms.setUseComplex(false);
        const cv::Mat outputReal = ms.filter(image);
        BOOST_REQUIRE(outputReal.type() == CV_64FC3);
        
        // Run complex filter
        ms.setUseComplex(true);
        const cv::Mat outputComplex = ms.filter(image);
        BOOST_REQUIRE(outputComplex.type() == CV_64FC3);
        
        // Compute mean absolute errors between outputs
        const cv::Scalar mae = cv::mean(cv::abs(outputReal - outputComplex));
        
        // Check
        const double tol = 1e-6;
        BOOST_REQUIRE_SMALL(mae[0], tol);
        BOOST_REQUIRE_SMALL(mae[1], tol);
        BOOST_REQUIRE_SMALL(mae[2], tol);
    }
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(filter_intrinsic_dimension)

BOOST_AUTO_TEST_CASE(filter_intrinsic_dimension_filter) {
    // Load an image
    const cv::Mat image = cv::imread("lena.png");
    BOOST_REQUIRE(!image.empty());
    
    // Run filter
    IntrinsicDimension ms;
    const cv::Mat output = ms.filter(image);
    BOOST_REQUIRE(output.type() == CV_64FC3);
    
    // Split
    cv::Mat id012[3];
    cv::split(output, id012);
    
    // Check
    double min, max;
    for(int c = 0; c < 3; ++c) {
        cv::minMaxLoc(id012[c], &min, &max);
        BOOST_REQUIRE(min >= 0.0 && min <= 1.0);
        BOOST_REQUIRE(max >= 0.0 && max <= 1.0);
        BOOST_REQUIRE(min <= max);
    }
}

BOOST_AUTO_TEST_SUITE_END()
