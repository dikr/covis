#define BOOST_TEST_MODULE core
#include <boost/test/included/unit_test.hpp>

#include <covis/covis.h>
using namespace covis::core;

BOOST_AUTO_TEST_SUITE(core_converters)

BOOST_AUTO_TEST_CASE(core_converters_mapxyz) {
    // Create a random point cloud
    pcl::PointCloud<pcl::PointXYZ> cloud(100, 1);
    for(size_t i = 0; i < cloud.size(); ++i) {
        cloud[i].x = randn<float>();
        cloud[i].y = randn<float>();
        cloud[i].z = randn<float>();
    }
    
    // Convert to an Eigen matrix
    const Eigen::MatrixX3f m = mapxyz<pcl::PointXYZ>(cloud);
    BOOST_REQUIRE_EQUAL(m.rows(), cloud.size());
    
    // Convert back to a point cloud
    pcl::PointCloud<pcl::PointXYZ> cloud2 = mapxyz<pcl::PointXYZ>(m);
    BOOST_REQUIRE_EQUAL(cloud2.size(), cloud.size());
    
    // Check for equality
    const float tol = 1e-5f;
    for(size_t i = 0; i < cloud2.size(); ++i) {
        BOOST_REQUIRE_SMALL(cloud[i].x - cloud2[i].x, tol);
        BOOST_REQUIRE_SMALL(cloud[i].y - cloud2[i].y, tol);
        BOOST_REQUIRE_SMALL(cloud[i].z - cloud2[i].z, tol);
    }
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(core_random)

BOOST_AUTO_TEST_CASE(core_random_mean_stddev_rand) {
    typedef double RealT;
    
    // Create a large sample of random uniform numbers
    const RealT a(1);
    const RealT b(2);
    const std::vector<RealT> v = rand<RealT>(1000000, a, b);
    
    // Compute sample statistics
    const RealT m = mean<RealT>(v);
    const RealT s = stddev<RealT>(v);
    
    // Check
    const RealT tol(0.01);
    BOOST_REQUIRE_SMALL(m - 0.5 * (a + b), tol);
    BOOST_REQUIRE_SMALL(s - RealT(sqrtf((b-a) * (b-a) / 12.0)), tol);
}

BOOST_AUTO_TEST_CASE(core_random_mean_stddev_randn) {
    typedef double RealT;
    
    // Create a large sample of random Gaussian numbers
    const RealT meangt(1);
    const RealT stdgt(2);
    const std::vector<RealT> v = randn<RealT>(1000000, meangt, stdgt);
    
    // Compute sample statistics
    const RealT m = mean<RealT>(v);
    const RealT s = stddev<RealT>(v);
    
    // Check
    const RealT tol(0.01);
    BOOST_REQUIRE_SMALL(m - meangt, tol);
    BOOST_REQUIRE_SMALL(s - stdgt, tol);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(core_range)

BOOST_AUTO_TEST_CASE(core_range) {
    typedef int IntT;
    
    const IntT a = 10;
    const IntT b = 100;
    const std::vector<IntT> r = range<IntT>(a, b);
    
    for(IntT i = a; i < b; ++i)
        BOOST_REQUIRE_EQUAL(r[i-a], i);
}

BOOST_AUTO_TEST_SUITE_END()
