CoViS
=====

The Cognitive Vision Software (CoViS) is the implementational basis for the Early Cognitive Vision (ECV) system which has been developed by the Cognitive Vision Group, University of Southern Denmark ([http://covil.sdu.dk](http://covil.sdu.dk)).

CoViS started out in 2002 as a rather complete computer vision software library, providing functionality for linear algebra, low-level image processing, image feature extraction, pose estimation etc. Over time, the library had a lot of contributions by many people, but during this time the following happened:

  * The supplied code by contributors was leading to a great variety in code quality and standards.
  * Other parts of the computer vision community evolved, providing specialized libraries for image and point cloud processing, rendering much of the functionality in CoViS redundant.

We realized that we needed to rework CoViS, since it was becoming increasingly difficult to use. We decided on trying to avoid reimplementations as much as possible by relying on third-party libraries for low-level representations and functionality. The goal of CoViS is to provide *algorithms* which can be applied to e.g. features at different levels of granularity, but also to provide a *visual hierarchy* of features for the representation of 2D and 3D entities.

Installation
============

The following has only been tested on Ubuntu Linux. Contributions and experiences for other OSes are welcome!

Install system tools and dependencies from the Ubuntu repositories:
```sh
sudo apt-get install cmake doxygen g++ git libboost-all-dev libeigen3-dev libopencv-dev libvtk5-dev
```

Install PCL (as per [http://pointclouds.org/downloads/linux.html](http://pointclouds.org/downloads/linux.html)):
```sh
sudo add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl
sudo apt-get update
sudo apt-get install libpcl-all
```

Get and compile CoViS, relative to some root folder of your choice:
```sh
git clone https://gitlab.com/caro-sdu/covis.git covis
mkdir -p covis/build
cd covis/build
cmake ..
make
```

Also build the documentation (inside covis/build):
```sh
make doc
```
You will now find API doc and additional information, e.g. on how to use CoViS in your own CMake project, in covis/build/doc/html/index.html.

Further information
===================
Further information on the background of CoViS can be found here: [http://covis.sdu.dk](http://covis.sdu.dk).

