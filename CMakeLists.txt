##
# Require CMake
##
cmake_minimum_required(VERSION 2.8)

##
# Project and main library name
# TODO: Make compiler info silent
##
project(CoViS C CXX)
set(COVIS_LIBRARY covis CACHE STRING "Main CoViS library")
mark_as_advanced(COVIS_LIBRARY)

##
# Setup build system, standard compiler flags, log level and installation prefixes
##
include(cmake/covis_system.cmake)

##
# Setup version info
#   COVIS_VERSION
#   COVIS_REVISION
##
include(cmake/covis_version.cmake)

##
# Find all mandatory and optional dependencies
# This script adds to the state variables
##
include(cmake/covis_find_deps.cmake)

##
# Add the main library headers/sources
#   COVIS_MODULE_DIRS
#   COVIS_HEADERS
#   COVIS_SOURCES
##
add_subdirectory(src)

##
# Add target doc if Doxygen is available
##
add_subdirectory(doc)

##
# Add target style if astyle is available
# Needs COVIS_MODULE_DIRS
##
include(cmake/covis_style.cmake)

##
# Deploy for build and installation
##
include(cmake/covis_deploy.cmake)

##
# Compile examples
# Needs COVIS_LIBRARY and COVIS_LIBRARIES_BUILD
##
add_subdirectory(example)

##
# Setup testing and coverage
##
include(cmake/covis_testing.cmake)

##
# Print summary
##
message(STATUS "")
if(COVIS_REVISION EQUAL -1)
  message(STATUS ">>>>>>>>>>>>>>>>>>>>>>>>> CoViS ${COVIS_VERSION} <<<<<<<<<<<<<<<<<<<<<<<<<")
else(COVIS_REVISION EQUAL -1)
  message(STATUS ">>>>>>>>>>>>>>>>>>>>>>>>> CoViS ${COVIS_VERSION} (r${COVIS_REVISION}) <<<<<<<<<<<<<<<<<<<<<<<<<")
endif(COVIS_REVISION EQUAL -1)
message(STATUS "")

message(STATUS "")
message(STATUS ">> BUILD SYSTEM\t\t\t${CMAKE_SYSTEM_NAME}/${COVIS_COMPILER}")
#if(COVIS_CXX11)
  #message(STATUS ">> BUILD SYSTEM\t\t\t${CMAKE_SYSTEM_NAME}/${COVIS_COMPILER}/C++11")
#else(COVIS_CXX11)
  #message(STATUS ">> BUILD SYSTEM\t\t\t${CMAKE_SYSTEM_NAME}/${COVIS_COMPILER}/C++03")
#endif(COVIS_CXX11)

message(STATUS "")
message(STATUS ">> BUILD CONFIGURATION\t\t${CMAKE_BUILD_TYPE}")

message(STATUS "")
message(STATUS ">> COVIS_DEFINTIONS\t\t\t${COVIS_DEFINITIONS}")

if(COVIS_DEFINITIONS_EXTRA)
  message(STATUS "")
  message(STATUS ">> COVIS_DEFINITIONS_EXTRA\t\t${COVIS_DEFINITIONS_EXTRA}")
endif(COVIS_DEFINITIONS_EXTRA)

message(STATUS "")
message(STATUS ">> STATIC BUILD\t\t\t${COVIS_BUILD_STATIC}")

message(STATUS "")
message(STATUS ">> LOG LEVEL\t\t\t\t${COVIS_LOG_LEVEL}")

if(COVIS_MODULE_DIRS)
  message(STATUS "")
  set(COVIS_MODULE_DIRS_COPY ${COVIS_MODULE_DIRS})
  list(GET COVIS_MODULE_DIRS_COPY 0 MODULE_DIR_FIRST)
  list(REMOVE_AT COVIS_MODULE_DIRS_COPY 0)
  message(STATUS ">> MODULES IN BUILD\t\t\t${MODULE_DIR_FIRST}")
  foreach(MODULE_DIR ${COVIS_MODULE_DIRS_COPY})
    message(STATUS "\t\t\t\t\t${MODULE_DIR}")
  endforeach(MODULE_DIR)
endif(COVIS_MODULE_DIRS)

message(STATUS "")
message(STATUS ">> BUILDING TESTCASES\t\t${COVIS_BUILD_TESTS}")

message(STATUS "")
message(STATUS ">> OPTIONAL LIBRARIES FOUND")
if(VTK_FOUND)
  message(STATUS ">>>> VTK")
endif(VTK_FOUND)
if(CUDA_FOUND)
  message(STATUS ">>>> CUDA")
endif(CUDA_FOUND)
if(OPENMP_FOUND)
  message(STATUS ">>>> OpenMP")
endif(OPENMP_FOUND)
if(OPENNI2_FOUND)
  message(STATUS ">>>> OpenNI2")
endif(OPENNI2_FOUND)
if(NURBSPP_FOUND)
  message(STATUS ">>>> NURBS++")
endif(NURBSPP_FOUND)

message(STATUS "")
message(STATUS ">> OPTIONAL PROGRAMS FOUND")
if(DOXYGEN_FOUND)
  message(STATUS ">>>> Doxygen")
endif(DOXYGEN_FOUND)
if(ASTYLE_FOUND)
  message(STATUS ">>>> Astyle")
endif(ASTYLE_FOUND)

message(STATUS "")
message(STATUS ">> INSTALLATION PATHS")
message(STATUS ">>>> Headers\t\t\t\t${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_INC}")
message(STATUS ">>>> Libraries\t\t\t${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_LIB}")
message(STATUS ">>>> Binaries\t\t\t${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_BIN}")
message(STATUS ">>>> Documentation\t\t\t${CMAKE_INSTALL_PREFIX}/${INSTALL_DIR_DOC}")
message(STATUS ">>>> CMake find script\t\t${INSTALL_DIR_CMAKE}")
message(STATUS ">>>> CMake config script\t\t${CMAKE_BINARY_DIR}")

message(STATUS "")
message(STATUS ">>>>>>>>>>>>>>>>>>>> CoViS configuration done <<<<<<<<<<<<<<<<<<<<")
message(STATUS "")
